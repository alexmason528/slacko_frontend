'use strict';

angular
    .module('app.routes', ['ngRoute'])
    .config(config);

function config ($routeProvider, $locationProvider) {
    $routeProvider.
        when('/', {
            templateUrl: 'views/index/index.tpl.html',
            controller: 'IndexController'
        })
        .when('/home', {
            templateUrl: 'views/home/home.tpl.html',
            controller: 'HomeController'
        })
        .when('/companies', {
            templateUrl: 'views/companies/index.tpl.html',
            controller: 'CompanyController'
        })
        .when('/conversations', {
            templateUrl: 'views/conversation/index.tpl.html',
            controller: 'ConvController'
        })
        .when('/manage-conversation/:id', {
            templateUrl: 'views/conversation/conv-mng.tpl.html',
            controller: 'ConvMngController'
        })
        .when('/company-profile/:id', {
            templateUrl: 'views/companies/company-profile.tpl.html',
            controller: 'CompanyProfileController'
        })
        .when('/settings', {
            templateUrl: 'views/settings/index.tpl.html',
            controller: 'SettingsController'
        })
        .when('/confirm/auth/magic_login/:key', {
            templateUrl: 'views/index/index.tpl.html',
            controller: 'AuthMagicLoginController'
        })
        .when("/confirm/auth/verify/:key", {
            templateUrl: 'views/index/index.tpl.html',
            controller: 'AuthVerifyController',
        })
        .when("/confirm/auth/password_reset/:key", {
            templateUrl: 'views/index/index.tpl.html',
            controller: 'AuthPasswordResetController'
        })
        .when("/confirm/auth/invite/:key", {
            templateUrl: 'views/index/index.tpl.html',
            controller: 'AithInviteController'
        })
        .when("/confirm/company/invite/:key", {
            templateUrl: 'views/index/index.tpl.html',
            controller: 'CompanyInviteController'
        })
        .when("/confirm/conversation/invite/:key", {
            templateUrl: 'views/index/index.tpl.html',
            controller: 'ConversationInviteController'
        })
        .when("/confirm/company/affiliated_invite/:key", {
            templateUrl: 'views/index/index.tpl.html',
            controller: 'CompanyAffiliatedInviteController'
        })
        .when("/confirm/chat/delete/:key", {
            templateUrl: 'views/index/index.tpl.html',
            controller: 'ChatDeleteController'
        })
        .otherwise({
            redirectTo: '/'
        });

    // use the HTML5 History API
    // $locationProvider.html5Mode(true);
}

'use strict';

//This will prevent Dropzone to instantiate on it's own unless you are using dropzone class for styling
Dropzone.autoDiscover = false;

angular
    .module('app.config', [])
    .factory('httpInterceptor', ['$q','$location', '$rootScope', 'localStorageService', httpInterceptor])
    .config(configs)
    .run(runs);

function httpInterceptor($q, $location, $rootScope, localStorageService){
    var responseError = function (rejection) {
        if (rejection.status === 401) {
            console.log('You are unauthorised to access the requested resource (401)');
        } else if (rejection.status === 404) {
            console.log('The requested resource could not be found (404)');
        } else if (rejection.status === 500) {
            console.log('Internal server error (500)');
            // $location.path('/');
        } else if (rejection.status === 403) {
            if ('Signature has expired.' == rejection.data.detail) {
                console.log('Signature has expired. (403)');
                $rootScope.isAuthorized = false;
                $rootScope.isOwner = false;
                $rootScope.globals = {};
                localStorageService.remove('globals');
                // $http.defaults.headers.common.Authorization = 'Basic';
                $location.path('/');
            } else {
                console.log('Got unexpected 403:', rejection);
            }
        }
        return $q.reject(rejection);
    };

    return {
        responseError: responseError
    };
}

function configs($httpProvider, $resourceProvider) {
    //$httpProvider.defaults.withCredentials = true;
    $httpProvider.interceptors.push('httpInterceptor');
    $resourceProvider.defaults.stripTrailingSlashes = false;
}

function runs($rootScope, $location, $http, localStorageService, config) {
    // keep user logged in after page refresh
    $rootScope.globals = localStorageService.get('globals') || {};
    $rootScope.isOwner = false;
    $rootScope.isAuthorized = false;
    $rootScope.hideFooter = false;
    if ($rootScope.globals.currentUser) {
        $http.defaults.headers.common['Authorization'] = 'JWT ' + $rootScope.globals.currentUser.token; // jshint ignore:line
        $rootScope.isAuthorized = true;
        if ($rootScope.globals.companyOwner) {
            $rootScope.isOwner = $rootScope.globals.companyOwner.id == $rootScope.globals.currentUser.profile.id;
        }
    }

    $rootScope.$on('$locationChangeStart', function (event, next, current) {
        // redirect to login page if not logged in and trying to access a restricted page
        var restrictedPage = $.inArray($location.path(), ['/', ]) === -1;
        var loggedIn = $rootScope.globals.currentUser;
        if (restrictedPage && !loggedIn && !$location.path().startsWith('/confirm/')) {
            $location.path('/');
        } else {
            if ($rootScope.globals.currentUser) {
                $http.get(config.baseURL + '/api/v1/notifications/?user=' + $rootScope.globals.currentUser.profile.user_id).then(function(response) {
                    console.log('notifications:', response.data);
                    $rootScope.globals.notifications = response.data.results;

                    $rootScope.globals.notifications_new = 0;
                    angular.forEach(response.data.results, function(el) {
                        if (false == el.is_viewed) {
                            $rootScope.globals.notifications_new += 1;
                        }
                    });
                });
            }
        }
    });
}

'use strict';

angular
    .module('app.constants', [])
    .constant('config', {
    	'baseURL': 'http://api.bliff.io',
    	'chatURL': 'api.bliff.io',
    	'socials': ['facebook', 'twitter', 'instagram', 'google_plus', 'linkedin', 'youtube', 'github', 'stackoverflow']
	});

'use strict';

angular.module('app', [
	'ngRoute',
    'ngMessages',
    'ngDialog',
	'ngAnimate',
	'ngTouch',
	'ngResource',
    'ngDraggable',
	'ui.bootstrap',
	'angular-nicescroll',
	'LocalStorageModule',
	'angular-loading-bar',
	'angular-click-outside',
    'thatisuday.dropzone',
	'app.routes',
	'app.constants',
	'app.config'
]);

angular.module('app').run(['$templateCache', function($templateCache) {
  'use strict';

  $templateCache.put('views/companies/company-profile.tpl.html',
    "<script type=\"text/ng-template\" id=\"convCreatedPopup\"><div class=\"ngdialog-message\">\n" +
    "        <h3>Success</h3>\n" +
    "        <hr>\n" +
    "        <div>\n" +
    "            Conversation request sent!\n" +
    "        </div>\n" +
    "    </div>\n" +
    "    <div class=\"ngdialog-buttons mt\">\n" +
    "        <button type=\"button\" class=\"ngdialog-button ngdialog-button-primary\" ng-click=\"closeThisDialog()\">Close</button>\n" +
    "    </div></script> <div class=\"company-profile-page\"> <section class=\"top-container\" back-img=\"{{companyProfile.cover}}\"> <div class=\"fade-bg fade40\"></div> <div class=\"container\"> <img ng-src=\"{{companyProfile.logo || 'assets/images/no-avatar.png'}}\" class=\"company-logo\"> <h1 class=\"company-name\">{{companyProfile.name}}</h1> <ul class=\"social-list\"> <li ng-show=\"companyProfile.facebook && companyProfile.facebook!=''\"> <a ng-href=\"companyProfile.facebook\"><i class=\"fa fa-facebook\"></i></a> </li> <li ng-show=\"companyProfile.twitter && companyProfile.twitter!=''\"> <a ng-href=\"companyProfile.twitter\"><i class=\"fa fa-twitter\"></i></a> </li> <li ng-show=\"companyProfile.instagram && companyProfile.instagram!=''\"> <a ng-href=\"companyProfile.instagram\"><i class=\"fa fa-instagram\"></i></a> </li> <li ng-show=\"companyProfile.google_plus && companyProfile.google_plus!=''\"> <a ng-href=\"companyProfile.google_plus\"><i class=\"fa fa-google-plus\"></i></a> </li> <li ng-show=\"companyProfile.linkedin && companyProfile.linkedin!=''\"> <a ng-href=\"companyProfile.linkedin\"><i class=\"fa fa-linkedin\"></i></a> </li> <li ng-show=\"companyProfile.youtube && companyProfile.youtube!=''\"> <a ng-href=\"companyProfile.youtube\"><i class=\"fa fa-youtube-play\"></i></a> </li> <li ng-show=\"companyProfile.github && companyProfile.github!=''\"> <a ng-href=\"companyProfile.github\"><i class=\"fa fa-github-alt\"></i></a> </li> <li ng-show=\"companyProfile.stackoverflow && companyProfile.stackoverflow!=''\"> <a ng-href=\"companyProfile.stackoverflow\"><i class=\"fa fa-stack-overflow\"></i></a> </li> </ul> <div class=\"butotn-area\"> <button ng-hide=\"isFriend()\" class=\"btn btn-info\" ng-click=\"sendFriendRequest()\">Send a Friend Request</button> <button ng-show=\"isFriendReq()\" disabled class=\"btn btn-info send-request\">Friend Request Sent</button> <button class=\"btn btn-success\" ng-click=\"createConv()\">Create Conversation</button> </div> </div> </section> <section class=\"main-container\"> <div class=\"container\"> <h5 class=\"page-title\">{{companyProfile.name}} Profile</h5> <div class=\"row\"> <div class=\"col-lg-3 col-md-3 col-sm-6 col-xs-6\"> <div class=\"basic-info\"> <h3>Website</h3> <a ng-href=\"companyProfile.website\">{{companyProfile.website}}</a> </div> <div class=\"basic-info\"> <h3>Headquarters</h3> <p>{{companyProfile.city}}, {{companyProfile.state}} {{companyProfile.country}}</p> </div> </div> <div class=\"col-lg-3 col-md-3 col-sm-6 col-xs-6\"> <div class=\"basic-info\"> <h3>Industry</h3> <p>{{companyProfile.industry}}</p> </div> <div class=\"basic-info\"> <h3>Company Size</h3> <p>{{companyProfile.company_size}}</p> </div> </div> <div class=\"col-lg-3 col-md-3 col-sm-6 col-xs-6\"> <div class=\"basic-info\"> <h3>Type</h3> <p>{{companyProfile.company_type}}</p> </div> <div class=\"basic-info\"> <h3>Founded</h3> <p>{{companyProfile.founded}}</p> </div> </div> <div class=\"col-lg-3 col-md-3 col-sm-6 col-xs-6\"> <div class=\"basic-info\"> <h3>Owner</h3> <div class=\"user-info\"> <img class=\"user-avatar\" alt=\"{{companyProfile.owner.username}}\" ng-src=\"{{companyProfile.owner.avatar || 'assets/images/no-avatar.png'}}\"> <div class=\"user-data\"> <h3 class=\"user-name\" ng-show=\"companyProfile.owner.first_name != ''\">{{companyProfile.owner.first_name}} {{companyProfile.owner.last_name}}</h3> <h3 class=\"user-name\" ng-show=\"companyProfile.owner.first_name == ''\">{{companyProfile.owner.email}}</h3> <span class=\"user-level\">{{companyProfile.owner.role}}</span> </div> </div> </div> </div> </div> <div class=\"row basic-info-wrapper\"> <div class=\"col-lg-6 col-md-6 col-sm-12 col-xs-12\"> <div class=\"info\"> <div class=\"info-title\"> <h5>Description</h5> </div> <div class=\"divider\"></div> <div class=\"info-body\" ng-nicescroll nice-option=\"{zindex: 999, autohidemode: true, cursorcolor: '#d4d4d4', cursorwidth: '7px', cursorborder: 'none', cursorborderradius: '7px'}\" ng-class=\"{'has-data': companyProfile.description}\"> <p class=\"comment\">This company has not written a description</p> <div class=\"company-desc\"> <p>{{companyProfile.description}}</p> </div> </div> </div> </div> <div class=\"col-lg-6 col-md-6 col-sm-12 col-xs-12\"> <div class=\"info\"> <div class=\"info-title\"> <h5>{{companyProfile.name}} FRIENDS</h5> </div> <div class=\"divider\"></div> <div class=\"info-body\" ng-nicescroll nice-option=\"{zindex: 999, autohidemode: true, cursorcolor: '#d4d4d4', cursorwidth: '7px', cursorborder: 'none', cursorborderradius: '7px'}\" ng-class=\"{'has-data': companyFriends.length!=0}\"> <p class=\"comment\">{{companyProfile.name}} doesn't have any friends yet</p> <ul class=\"friend-list\"> <li class=\"company-info clearfix\" ng-repeat=\"company in companyFriends track by $index\" ng-if=\"company.id!=companyProfile.id\"> <a href=\"/#/company-profile/{{company.id}}\"> <img ng-src=\"{{company.logo || 'assets/images/no-avatar.png'}}\" class=\"company-logo\"> <div class=\"company-data\"> <h3 class=\"company-name\">{{company.name}}</h3> <span class=\"company-role\">{{company.industry}}</span> </div> </a> </li> </ul> </div> </div> </div> <div class=\"col-lg-6 col-md-6 col-sm-12 col-xs-12\"> <div class=\"info\"> <div class=\"info-title\"> <h5>PEOPLE WHO WORK IN {{companyProfile.name}}</h5> </div> <div class=\"divider\"></div> <div class=\"info-body\" ng-class=\"{'has-data': companyMembers.length!=0}\" ng-nicescroll nice-option=\"{zindex: 999, autohidemode: true, cursorcolor: '#d4d4d4', cursorwidth: '7px', cursorborder: 'none', cursorborderradius: '7px'}\"> <p class=\"comment\">Invite people who work in <br>{{companyProfile.name}}</p> <ul class=\"fb-user-list\"> <li class=\"user-info clearfix\" ng-repeat=\"member in companyMembers track by $index\"> <img ng-src=\"{{member.avatar || 'assets/images/no-avatar.png'}}\" class=\"user-avatar\"> <div class=\"user-data\"> <h3 class=\"user-name\" ng-show=\"member.first_name!=null && member.first_name!=''\">{{member.first_name}} {{member.last_name}}</h3> <h3 class=\"user-name\" ng-hide=\"member.first_name!=null && member.first_name!=''\">{{member.email}}</h3> <span class=\"user-level\">{{member.role}}</span> </div> </li> </ul> </div> </div> </div> <div class=\"col-lg-6 col-md-6 col-sm-12 col-xs-12\"> <div class=\"info\"> <div class=\"info-title\"> <h5>{{companyProfile.name}} HAS AFFILIATED COMPANIES</h5> </div> <div class=\"divider\"></div> <div class=\"info-body\" ng-class=\"{'has-data': companyProfile.affiliated_companies.length!=0}\" ng-nicescroll nice-option=\"{zindex: 999, autohidemode: true, cursorcolor: '#d4d4d4', cursorwidth: '7px', cursorborder: 'none', cursorborderradius: '7px'}\"> <p class=\"comment\">{{companyProfile.name}} has no affiliated companies.</p> <ul class=\"affli-company-list\" ng-repeat=\"company in affiCompanies track by $index\"> <li class=\"company-info clearfix\"> <a ng-href=\"/#/company-profile/{{company.id}}\"> <img ng-src=\"{{company.logo || 'assets/images/no-avatar.png'}}\" class=\"company-logo\"> <div class=\"company-data\"> <h3 class=\"company-name\">{{company.name}}</h3> <span class=\"company-role\">{{company.industry}}</span> </div> </a> </li> </ul> </div> </div> </div> </div> </div> </section> </div>"
  );


  $templateCache.put('views/companies/index.tpl.html',
    "<div class=\"company-page\"> <section class=\"top-container\"> <div class=\"container\"> <div class=\"row\"> <div class=\"col-md-6\"> <div class=\"form-group\"> <input type=\"text\" class=\"form-control\" placeholder=\"Start typing a company's name, a website or an email\" ng-model=\"searchKey\" ng-change=\"filter()\"> </div> <div class=\"industry-tokens-content\"> <ul class=\"industry-tokens\"> <!--\n" +
    "\t\t\t\t\t\t\t<li ng-show=\"allFiltered\">\n" +
    "\t\t\t\t\t\t\t\t<span class=\"content\">All</span>\n" +
    "\t\t\t\t\t\t\t\t<span ng-click=\"deleteAllFilters()\" class=\"close-icon thin-white cursor-pointer\"></span>\n" +
    "\t\t\t\t\t\t\t</li>\n" +
    "\t\t\t\t\t\t\t--> <li ng-repeat=\"item in filters\"> <span class=\"content\">{{item.name}}</span> <span ng-click=\"deleteFilter(item.name)\" class=\"close-icon thin-white cursor-pointer\"></span> </li> </ul> </div> </div> <div class=\"col-md-6\"> <div class=\"dropdown-wrapper\"> <div class=\"dropdown-custom searchByIndustry\" ng-class=\"{'open': isSearchDropdownOpened}\"> <div class=\"title\" ng-click=\"toggleSearchDropdown()\">Search by industry</div> <div class=\"content\"> <div class=\"industry-list\" ng-nicescroll nice-option=\"{zindex: 999, autohidemode: true, cursorcolor: '#595959', cursorwidth: '7px', cursorborder: 'none', cursorborderradius: '7px'}\"> <div class=\"item\" ng-click=\"selectAllItem()\" ng-class=\"{'filtered': allFiltered}\">All</div> <div class=\"item\" ng-repeat=\"item in industryList\" ng-click=\"selectItem(item)\" ng-class=\"{'filtered': item.filtered || allFiltered}\">{{item.name}}</div> </div> <div class=\"button-area clearfix\"> <a ng-click=\"selectAllItem()\">Reset Filter</a> <a ng-click=\"applyFilter()\">Apply Filter</a> </div> </div> </div> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-12\"> <div class=\"map-wrapper\"> <a class=\"on-map\" ng-click=\"toggleOnMap()\">On Map<span class=\"drop-down\"></span></a> <a class=\"reset-all-filter\" ng-click=\"resetAllFilters()\">Reset all filters</a> </div> </div> </div> </div> </section> <section class=\"map-container\" ng-show=\"isOnMap\"> <div id=\"map\" class=\"map-content\"> </div> </section> <section class=\"main-container\"> <div class=\"container\"> <div class=\"row\"> <div class=\"col-md-12\"> <p class=\"search-count\">{{filteredCompanyList.length}} Companies found</p> </div> </div> <div class=\"row companies-list\"> <div class=\"col-md-6\" ng-repeat=\"company in filteredCompanyList track by $index\"> <div class=\"company-item\" back-img=\"{{company.cover}}\" ng-click=\"visitCompany(company.id)\"> <div class=\"fade-bg fade40\"></div> <div class=\"title clearfix\"> <div class=\"pull-left\"> <img class=\"company-avatar\" ng-src=\"{{company.logo || 'assets/images/no-avatar.png'}}\"> <h3 class=\"company-name\">{{company.name}}</h3> </div> <div class=\"pull-right\"> <button ng-hide=\"isFriend(company.id)\" class=\"btn btn-info send-request\" ng-click=\"inviteFriend(company.id, $event)\">Send a Friend Request</button> <button ng-show=\"isFriendReq(company.id)\" disabled class=\"btn btn-info send-request\">Friend Request Sent</button> </div> </div> <div class=\"content\"> <ul class=\"info-list clearfix\"> <li> <p class=\"info-title\">Website</p> <span class=\"info-desc\"><a ng-href=\"{{company.website}}\" title=\"{{company.website}}\">{{getLinkPreview(company.website) || '&nbsp;'}}</a></span> </li> <li> <p class=\"info-title\">Industry</p> <span class=\"info-desc\">{{company.industry || '&nbsp;'}}</span> </li> <li> <p class=\"info-title\">Type</p> <span class=\"info-desc\">{{company.company_type || '&nbsp;'}}</span> </li> <li> <p class=\"info-title\">Company Size</p> <span class=\"info-desc\">{{company.company_size}} employees</span> </li> <li> <p class=\"info-title\">Headquarters</p> <span class=\"info-desc\">{{company.city}} {{company.state}} {{company.country}}</span> </li> <li> <p class=\"info-title\">Founded</p> <span class=\"info-desc\">{{company.founded || '&nbsp;'}}</span> </li> </ul> </div> </div> </div> </div> </div> </section> </div>"
  );


  $templateCache.put('views/conversation/choose-company-list.tpl.html',
    "<div class=\"company-list-wrapper\"> <ul class=\"company-list\"> <li ng-click=\"filterSource()\" ng-class=\"{'selected': !filter_source}\"> <div class=\"company-info\"> <img class=\"company-logo\" ng-src=\"{{ currentRoom.source.logo || 'assets/images/no-avatar.png'}}\"> <img class=\"selected-mark\" src=\"/assets/images/check-icon.png\"> <div class=\"company-data\"> <h3 class=\"company-name\">{{ currentRoom.source.name }}</h3> <span class=\"company-role\">{{ currentRoom.source.industry }}</span> </div> </div> </li> <li ng-click=\"filterTarget()\" ng-class=\"{'selected': !filter_target}\"> <div class=\"company-info\"> <img class=\"company-logo\" ng-src=\"{{ currentRoom.target.logo || 'assets/images/no-avatar.png'}}\"> <img class=\"selected-mark\" src=\"/assets/images/check-icon.png\"> <div class=\"company-data\"> <h3 class=\"company-name\">{{ currentRoom.target.name }}</h3> <span class=\"company-role\">{{ currentRoom.target.industry }}</span> </div> </div> </li> </ul> </div>"
  );


  $templateCache.put('views/conversation/conv-company-list.tpl.html',
    "<ul class=\"conv-company-list\"> <li class=\"wrapper\" ng-click=\"enterRoom(room)\" ng-repeat=\"room in roomsFiltered\"> <div class=\"company-info\"> <div class=\"company-logo company-left\"> <div class=\"company-left\"> <img ng-src=\"{{ room.source.logo || 'assets/images/no-avatar.png'}}\"> </div> <div class=\"company-right\"> <img ng-src=\"{{ room.target.logo || 'assets/images/no-avatar.png'}}\"> </div> </div> <div class=\"company-data\"> <h3 class=\"company-name\">{{room.source.name}} & {{room.target.name}}</h3> <span class=\"company-desc\">{{room.name}}</span> <span class=\"company-desc\">{{room.members.length}} people are invited</span> </div> </div> </li> </ul>"
  );


  $templateCache.put('views/conversation/conv-detail.tpl.html',
    "<!-- <tr ng-repeat=\"message in currentRoom.messages\" ng-switch on=\"message.type\">\n" +
    "        <td>{{ getFormattedTime(message.timestamp) }}</td>\n" +
    "        <td>{{ message.handle.first_name }} {{ message.handle.last_name }}</td>\n" +
    "        <td ng-switch-when=\"file\">\n" +
    "            <a href=\"{{message.message}}\" target=\"_blank\">{{getFileName(message.message)}}</a>\n" +
    "        </td>\n" +
    "        <td ng-switch-when=\"text\">\n" +
    "            {{ message.message }}\n" +
    "        </td>\n" +
    "    </tr> --> <ul id=\"chat\" class=\"conv-list chat\"> <li class=\"wrapper\" ng-repeat=\"message in currentRoom.messages\" ng-switch on=\"message.type\"> <img class=\"user-avatar\" ng-src=\"{{ message.handle.avatar || 'assets/images/no-avatar.png'}}\"> <div class=\"conv-wrapper\"> <div class=\"chat-content\"> <div class=\"chat-header\"> <span class=\"user-name\">{{message.handle.first_name}} {{message.handle.last_name}}</span> <span class=\"chat-time\">{{getFormattedTime(message.timestamp)}}</span> </div> <p ng-switch-when=\"file\" class=\"chat-data\"> <a href=\"{{message.message}}\" target=\"_blank\">{{getFileName(message.message)}}</a> </p> <p ng-switch-when=\"text\" class=\"chat-data\"> {{ message.message }} </p> <span ng-repeat=\"file in message.files\"> <a ng-show=\"is_image(file.filename)\" target=\"_blank\" href=\"{{file.file}}\"><img class=\"chat-preview\" ng-src=\"{{file.file}}\" title=\"{{file.filename}}\" alt=\"{{file.filename}}\"></a> </span> <br> <span ng-repeat=\"file in message.files\"> <a ng-hide=\"is_image(file.filename)\" target=\"_blank\" href=\"{{file.file}}\">{{file.filename}}</a> </span> </div> </div> </li> </ul> <form id=\"chatform\"> <div class=\"send-message\"> <div ng-class=\"{'show': show_spin}\" class=\"spin\"> <img src=\"/assets/images/spin.gif\"> </div> <div class=\"attachment\"> <div ng-show=\"opt.showSelect\" class=\"btn-group-vertical attachment-select\"> <button ng-click=\"uploadFile()\" type=\"button\" class=\"btn\"><img src=\"/assets/images/attachments-icon-small.png\" class=\"\">File</button> <button ng-click=\"uploadImage()\" type=\"button\" class=\"btn\"><img src=\"/assets/images/photo-icon-gray.png\" class=\"\">Image</button> </div> <img ng-click=\"opt.showSelect = !opt.showSelect\" src=\"/assets/images/attachments-icon-large.png\" class=\"\"> </div> <textarea placeholder=\"Type your text here...\" ng-model=\"msg_text\" id=\"message\" class=\"new-message\" auto-grow></textarea> <div class=\"attachments-list\"> <span ng-repeat=\"file in files\"> <a ng-show=\"file.preview\"><img class=\"chat-preview\" ng-src=\"{{file.preview}}\" title=\"{{file.name}}\" alt=\"{{file.name}}\"></a> <a ng-hide=\"file.preview\">{{file.name}}</a> </span> </div> <button type=\"submit\" ng-disabled=\"''==msg_text || show_spin\" class=\"btn btn-info btn-sm send\" id=\"go\" ng-click=\"send()\">Send</button> <input type=\"file\" accept=\"image/*\" file-upload style=\"display: none\" id=\"uploadimage\" profile-attr=\"uploadimage\"> <input type=\"file\" file-upload style=\"display: none\" id=\"uploadfile\" profile-attr=\"uploadfile\"> </div> <!--<td>Say something:</td>--> <!--<td>--> <!--<input id=\"file\" type=\"file\" onchange=\"angular.element(this).scope().checkFile()\">--> <!--<button type=\"submit\" id=\"go\" ng-click=\"send()\">Say it</button>--> <!--</td>--> </form>"
  );


  $templateCache.put('views/conversation/conv-list.tpl.html',
    "<ul class=\"conv-list\"> <li class=\"wrapper confirmed_{{room.confirmed}}\" ng-repeat=\"room in roomsFiltered\"> <div class=\"wrapper gray-bg mng-conv-wrapper\" ng-hide=\"room.confirmed\"> <h3>This conversation is wating for approval. {{ room.target.name }} hasn't accepted your conversation request. <a ng-hide=\"room.started.id !== globals.currentUser.profile.user_id\" href=\"/#/manage-conversation/{{ room.id }}\">Manage this conversation</a></h3> </div> <div class=\"conv-wrapper gray-bg\" ng-if=\"room.confirmed\" ng-click=\"enterRoom(room)\"> <img class=\"user-avatar\" ng-src=\"{{ room.messages[room.messages.length-1].handle.avatar || 'assets/images/no-avatar.png'}}\"> <div class=\"chat-content\"> <h3 class=\"user-name\">{{room.messages[room.messages.length-1].handle.first_name}} {{room.messages[room.messages.length-1].handle.last_name}}</h3> <p class=\"chat-data\">{{getMessagePreview(room.messages[room.messages.length-1].message)}}</p> <!-- <div ng-switch on=\"room.last_message.message.type\">\n" +
    "                    <div ng-switch-when=\"file\">\n" +
    "                        <a href=\"{{room.last_message.message.message}}\" target=\"_blank\">{{getFileName(room.last_message.message.message)}}</a>\n" +
    "                        file here\n" +
    "                    </div>\n" +
    "                    <div ng-switch-when=\"text\">\n" +
    "                        {{ message.message }}\n" +
    "                    </div>\n" +
    "                </div> --> <div class=\"attachments-list\"> <span ng-repeat=\"file in room.messages[room.messages.length-1].files\"> <a target=\"_blank\" href=\"{{file.file}}\">{{file.filename}}</a> </span> </div> <p class=\"chat-time\">{{getFormattedTime(room.last_message.timestamp)}}</p> </div> </div> </li> </ul>"
  );


  $templateCache.put('views/conversation/conv-mng.tpl.html',
    "<div class=\"conv-mng-page\"> <div class=\"conv-select-container\"> <div class=\"container\"> <div class=\"row\"> <div class=\"col-md-12\"> <button ng-click=\"closeManager()\" class=\"btn btn-close btn-round\"><span class=\"close-icon bold-white icon\"></span></button> <div class=\"user-info conv-user mng-section\"> <img class=\"user-avatar\" ng-src=\"{{ currentRoom.started.avatar || 'assets/images/no-avatar.png'}}\"> <div class=\"user-data\"> <h3 class=\"user-name\">{{ currentRoom.started.first_name }} {{ currentRoom.started.last_name}} started a conversation</h3> <p class=\"user-level\">{{ currentRoom.started.role }} in <a href=\"/#/company-profile/{{ currentRoom.source.id }}\" class=\"company\">{{ currentRoom.source.name }}</a></p> </div> </div> </div> <div class=\"col-md-12\"> <div class=\"mng-section\"> <h3 class=\"title\">CHOOSE COMPANY</h3> <div ng-include=\"'views/conversation/choose-company-list.tpl.html'\"></div> </div> </div> <div class=\"col-md-12\"> <div class=\"mng-section\"> <h3 class=\"title\">CLICK OR DRAG DEPARTMENT TO ADD IT TO THE CONVERSATION</h3> <div ng-include=\"'views/conversation/depart-list.tpl.html'\"></div> </div> </div> <div class=\"col-md-12\"> <div class=\"mng-section\"> <h3 class=\"title\">CLICK OR DRAG PEOPLE ADD THEM TO THE CONVERSATION</h3> <div ng-include=\"'views/conversation/conv-people-list.tpl.html'\"></div> </div> </div> </div> </div> </div> <div class=\"conv-members-container\"> <div class=\"container\"> <div class=\"row\"> <div class=\"col-md-12\"> <h2 class=\"conv-members-title\">Invited members</h2> <div class=\"pull-right actions\"> <button class=\"btn btn-success btn-round\" ng-click=\"saveSettings()\">Save Settings<span class=\"check white-icon icon\"></span></button> <button ng-click=\"deleteConversation()\" class=\"btn btn-round\">Delete Conversation<span class=\"close-icon bold-white icon\"></span></button> </div> </div> <div class=\"devider\"></div> <div class=\"col-md-12\"> <div class=\"company-info\"> <img class=\"company-logo\" ng-src=\"{{ currentRoom.source.logo || 'assets/images/no-avatar.png'}}\"> <div class=\"company-data\"> <h3 class=\"company-name\">{{ currentRoom.source.name }}</h3> <span class=\"company-role\">{{ currentRoom.source.industry }}</span> </div> </div> <div class=\"depart-list-wrapper\" ng-nicescroll nice-option=\"{zindex: 999, autohidemode: true, cursorcolor: '#595959', cursorwidth: '7px', cursorborder: 'none', cursorborderradius: '7px'}\"> <ul class=\"depart-list\"> <li ng-repeat=\"depart in chatSourceMembers\" ng-click=\"deleteDepartmentFromChat(true, depart.name)\"> {{ depart.name }}<span class=\"close-icon bold-white icon\"></span> <div class=\"dep-members\"> <div ng-repeat=\"m in depart.list\" ng-click=\"toggleSelect(m.id)\"> {{ getMemberName(m) }} <span class=\"close-icon bold-white icon\"></span> </div> <div class=\"arrow\"></div> </div> </li> </ul> </div> </div> <div class=\"col-md-12\"> <hr> </div> <div class=\"col-md-12\"> <div class=\"company-info\"> <img class=\"company-logo\" ng-src=\"{{ currentRoom.target.logo || 'assets/images/no-avatar.png'}}\"> <div class=\"company-data\"> <h3 class=\"company-name\">{{ currentRoom.target.name }}</h3> <span class=\"company-role\">{{ currentRoom.target.industry }}</span> </div> </div> <div class=\"depart-list-wrapper\" ng-nicescroll nice-option=\"{zindex: 999, autohidemode: true, cursorcolor: '#595959', cursorwidth: '7px', cursorborder: 'none', cursorborderradius: '7px'}\"> <ul class=\"depart-list\"> <li ng-repeat=\"depart in chatTargetMembers\" ng-click=\"deleteDepartmentFromChat(false, depart.name)\"> {{ depart.name }}<span class=\"close-icon bold-white icon\"></span> <div class=\"dep-members\"> <div ng-repeat=\"m in depart.list\" ng-click=\"toggleSelect(m.id)\"> {{ getMemberName(m) }} <span class=\"close-icon bold-white icon\"></span> </div> <div class=\"arrow\"></div> </div> </li> </ul> </div> </div> </div> </div> <div ng-class=\"{'dragging': dragging}\" class=\"droppable\" ng-drop=\"true\" ng-drop-success=\"onDropComplete($data,$event)\"> <div class=\"droptext\">DROP HERE</div> </div> </div> </div>"
  );


  $templateCache.put('views/conversation/conv-people-list.tpl.html',
    "<div class=\"people-list-wrapper\" ng-nicescroll> <ul class=\"conv-people-list\"> <li ng-class=\"{selected: member.selected}\" ng-drag-start=\"dragStart($data,$event)\" ng-drag-stop=\"dragStop($data,$event)\" ng-drag=\"true\" ng-drag-data=\"{'type':'member', 'obj':member}\" ng-click=\"toggleSelect(member.id)\" ng-repeat=\"member in members_filtered\"> <div class=\"user-info\"> <img class=\"user-avatar\" ng-src=\"{{ member.avatar || 'assets/images/no-avatar.png'}}\"> <img class=\"selected-mark\" src=\"/assets/images/check-icon.png\"> <div class=\"user-data\"> <h3 class=\"user-name\">{{getMemberName(member)}}</h3> <span class=\"user-level\">{{member.role}} in {{ member.company_name }}</span> </div> </div> </li> </ul> </div>"
  );


  $templateCache.put('views/conversation/conv-room-member-list.tpl.html',
    "<div class=\"people-list-wrapper\"> <ul class=\"conv-people-list header\"> <li class=\"member\" ng-repeat=\"member in currentRoom.members\"> <img class=\"user-delete\" ng-click=\"deleteMember(currentRoom.id, member.id)\" src=\"/assets/images/close-icon-thin-gray.png\"> <img class=\"user-avatar\" ng-src=\"{{ member.avatar || 'assets/images/no-avatar.png'}}\"> <label class=\"user-name\">{{getMemberName(member)}}</label> </li> </ul> <a class=\"mng-button\" ng-click=\"gotoManage()\">MANAGE<img src=\"/assets/images/plus-icon.png\"></a> </div>"
  );


  $templateCache.put('views/conversation/depart-list.tpl.html',
    "<div class=\"depart-list-wrapper\" ng-nicescroll> <ul class=\"depart-list\"> <li ng-class=\"{selected: allFiltered}\" ng-click=\"selectDepartment('all')\" ng-drag=\"true\" ng-drag-start=\"dragStart($data,$event)\" ng-drag-stop=\"dragStop($data,$event)\" ng-drag-data=\"{'type':'depart', 'obj': 'all'}\"> All </li> <li ng-class=\"{selected: depart.filtered}\" ng-click=\"selectDepartment(depart.name)\" ng-drag=\"true\" ng-drag-start=\"dragStart($data,$event)\" ng-drag-stop=\"dragStop($data,$event)\" ng-drag-data=\"{'type':'depart', 'obj':depart.name}\" ng-repeat=\"depart in departList\"> {{ depart.name }} </li> </ul> </div>"
  );


  $templateCache.put('views/conversation/index.tpl.html',
    "<div class=\"conv-page\"> <div class=\"container-fluid\"> <div class=\"top-container\"> <div class=\"row\"> <div class=\"col-md-4\"> <div class=\"wrapper gray-bg\"> <h3>You are in {{roomsFiltered.length}} conversations</h3> </div> </div> <div class=\"col-md-8\"> <div class=\"wrapper\"> <div ng-switch on=\"selection\"> <div ng-switch-when=\"list\"> <div class=\"row\"> <div class=\"col-md-4\"> <ul class=\"view-mode\"> <li ng-class=\"{'selected': filter_type =='see_all'}\"><a ng-click=\"filterChat('see_all')\">SEE ALL</a></li> <li ng-class=\"{'selected': filter_type =='confirmed'}\"><a ng-click=\"filterChat('confirmed')\">CONFIRMED</a></li> <li ng-class=\"{'selected': filter_type =='unconfirmed'}\"><a ng-click=\"filterChat('unconfirmed')\">UNCONFIRMED</a></li> </ul> </div> <div class=\"col-md-8\"> <input type=\"text\" class=\"form-control typing-text\" ng-model=\"search.text\" ng-change=\"filter()\" placeholder=\"Start typyng a conversation's name or the word\"> </div> </div> </div> <div ng-switch-when=\"detail\"> <div ng-include=\"'views/conversation/conv-room-member-list.tpl.html'\"></div> </div> </div> </div> </div> </div> </div> <div class=\"main-container\"> <div class=\"row\"> <div class=\"col-md-4 gray-bg col-conv-list\"> <div ng-include=\"'views/conversation/conv-company-list.tpl.html'\"></div> <div class=\"footer gray-bg\"> <ul> <li> <a href=\"/#/#about\">ABOUT</a> </li> <li> <a href=\"/#/#team\">TEAM</a> </li> <li> <a href=\"/#/#review\">CONTACT</a> </li> <li> <a href=\"/discover-companies\">DISCOVER COMPANIES</a> </li> <li> <a href=\"/#/#get_started\">HOW IT WORKS</a> </li> </ul> </div> </div> <div class=\"col-md-8\"> <div ng-switch on=\"selection\"> <div ng-switch-when=\"list\"> <div ng-include=\"'views/conversation/conv-list.tpl.html'\"></div> </div> <div ng-switch-when=\"detail\"> <div ng-include=\"'views/conversation/conv-detail.tpl.html'\"></div> </div> </div> </div> </div> </div> </div> </div>"
  );


  $templateCache.put('views/footer/footer.tpl.html',
    "<div ng-hide=\"hideFooter\" id=\"footer\"> <div class=\"container\"> <div class=\"row\"> <div class=\"col-md-3\"> <a href=\"/\" class=\"logo\"> <img class=\"bliff-logo\" src=\"assets/images/bliff_logo.png\"> </a> </div> <div class=\"col-md-6\"> <ul class=\"bottom-menu\"> <li ng-hide=\"isAuthorized\"> <a href=\"#\" class=\"page-scroll btn-signin\" ng-click=\"signin()\">SIGN IN</a> </li> <li> <a href=\"#\" class=\"page-scroll btn-signin\" ng-click=\"signin()\">SIGNUP</a> </li> <li> <a href=\"/discover-companies\">DISCOVER COMPANIES</a> </li> <li> <a href=\"/#/#about\">HOW IT WORKS</a> </li> </ul> </div> <div class=\"col-md-3\"> <div class=\"social-links\"> <a><i class=\"fa fa-facebook\"></i></a> <a><i class=\"fa fa-twitter\"></i></a> </div> </div> </div> <div class=\"row clearfix copyright\"> <div class=\"col-lg-6\"> <p>Founded in 2016, Bliff - business networking site, enables companies to build strategic connections, form solid business networks with companies from anywhere in the world, and freely communicate with one another. Through exploring and sharing our views, we discover new ways of doing things. We inspire excellence, and we facilitate growth and sustainability throughout our business environment.</p> </div> <div class=\"col-lg-6\"> <p class=\"company-reserved\">Bliff all Right Reserved, 2016</p> </div> </div> </div> </div>"
  );


  $templateCache.put('views/header/header.tpl.html',
    "<nav class=\"navbar navbar-custom\" ng-class=\"{'gray-bg': hasMenuBG}\"> <div class=\"container\"> <div class=\"navbar-header\"> <button type=\"button\" class=\"navbar-toggle\" ng-init=\"navCollapsed=true\" ng-click=\"navCollapsed=!navCollapsed\"> <i class=\"fa fa-bars\"></i> </button> <a class=\"navbar-brand page-scroll\" ng-href=\"/#/\" ng-hide=\"isAuthorized\"> <img class=\"bliff-logo\" src=\"assets/images/bliff_logo.png\"> </a> <a class=\"navbar-brand page-scroll\" ng-href=\"/#/home/\" ng-show=\"isAuthorized\"> <img class=\"bliff-logo\" src=\"assets/images/bliff_logo.png\"> </a> </div> <div class=\"collapse navbar-collapse navbar-right navbar-main-collapse\" uib-collapse=\"navCollapsed\"> <ul class=\"nav navbar-nav\"> <li> <a ng-show=\"isAuthorized\" class=\"page-scroll\" ng-href=\"/#/companies\">Discover Companies</a> <a ng-hide=\"isAuthorized\" class=\"page-scroll\" ng-click=\"signin()\">Discover Companies</a> </li> <li ng-hide=\"isAuthorized\"> <a class=\"page-scroll btn-signin\" ng-click=\"signin()\">Sign In</a> </li> <li ng-show=\"isAuthorized\"> <a href=\"/#/conversations\"> <img src=\"assets/images/comment-icon.png\"> </a> </li> <li ng-show=\"isAuthorized\" uib-dropdown on-toggle=\"toggled(open)\"> <a uib-dropdown-toggle> <img src=\"assets/images/ring-icon.png\"> </a> <span ng-show=\"globals.notifications_new\" class=\"notification-unread-cnt\">{{ globals.notifications_new }}</span> <ul class=\"dropdown-menu notificatoin-menu\" ng-click=\"$event.stopPropagation()\" uib-dropdown-menu=\"\" aria-labelledby=\"simple-dropdown\"> <li role=\"menuitem\">NOTIFICATIONS</li> <li role=\"menuitem\"> <ul class=\"notificatoin-menu messages\" ng-nicescroll nice-option=\"{autohidemode: false, cursorcolor: '#d4d4d4', cursorwidth: '7px', cursorborder: 'none', cursorborderradius: '7px'}\"> <li role=\"menuitem\" ng-click=\"markAsRead(n)\" ng-repeat=\"n in globals.notifications track by $index\"> <div class=\"row\"> <div class=\"col-xs-2\"> <img ng-src=\"{{n.source.logo || 'assets/images/no-avatar.png'}}\" class=\"company-logo\"> </div> <div class=\"col-xs-10\"> <p>{{ n.description }}<br>{{ getFormattedTime(n.dt_created) }}</p> <div class=\"buttons\" ng-show=\"n.link\"> <a ng-click=\"link_accept(n)\" class=\"btn btn-success btn-sm\">Accept</a> <a ng-click=\"link_deny(n)\" class=\"btn btn-error btn-sm\">Deny</a> </div> </div> <span ng-hide=\"n.is_viewed\" class=\"notification-unread\"></span> </div> </li> </ul> </li> </ul> </li> <li ng-show=\"isAuthorized\" uib-dropdown on-toggle=\"toggled(open)\"> <div class=\"user-info\" uib-dropdown-toggle> <div class=\"user-data\"> <h3 class=\"user-name\">{{globals.currentUser.profile.first_name}} {{globals.currentUser.profile.last_name}}</h3> <span class=\"user-level\"><span class=\"user-role\">{{globals.currentUser.profile.role}}</span> in {{globals.currentUser.company}}</span> </div> <img class=\"user-avatar\" ng-src=\"{{ globals.currentUser.profile.avatar || 'assets/images/no-avatar.png' }}\"> </div> <ul class=\"dropdown-menu dropdown-custom-menu\" uib-dropdown-menu aria-labelledby=\"simple-dropdown\"> <li ng-show=\"isOwner\" role=\"menuitem\"><a ng-href=\"/#/home\">{{globals.currentUser.company}}</a></li> <li ng-hide=\"isOwner\" role=\"menuitem\"><a ng-href=\"/#/company-profile/{{globals.currentUser.profile.company}}\">{{globals.currentUser.company}}</a></li> <li role=\"menuitem\"><a ng-href=\"/#/settings\">Settings</a></li> <li role=\"menuitem\"><a ng-click=\"signout()\">Logout</a></li> </ul> </li> </ul> </div> <!-- /.navbar-collapse --> </div> <!-- /.container --> </nav> <script type=\"text/ng-template\" id=\"notificationPopup\"><div class=\"ngdialog-message\">\n" +
    "        <h3>{{ctx.status}}</h3>\n" +
    "        <hr>\n" +
    "        <div>\n" +
    "            {{ctx.message}}\n" +
    "        </div>\n" +
    "    </div>\n" +
    "    <div class=\"ngdialog-buttons mt\">\n" +
    "        <button type=\"button\" class=\"ngdialog-button ngdialog-button-primary\" ng-click=\"closeThisDialog()\">Close</button>\n" +
    "    </div></script> <script type=\"text/ng-template\" id=\"notificationPopupFriendship\"><div class=\"ngdialog-message\">\n" +
    "        <h3>Congratulation</h3>\n" +
    "        <hr>\n" +
    "        <div>\n" +
    "            You are now friends\n" +
    "        </div>\n" +
    "    </div>\n" +
    "    <div class=\"ngdialog-buttons mt\">\n" +
    "        <button type=\"button\" class=\"ngdialog-button ngdialog-button-primary\" ng-click=\"closeThisDialog()\">Close</button>\n" +
    "    </div></script> <script type=\"text/ng-template\" id=\"notificationPopupAffiliated\"><div class=\"ngdialog-message\">\n" +
    "        <h3>Congratulation</h3>\n" +
    "        <hr>\n" +
    "        <div>\n" +
    "            You are now affiliated companies\n" +
    "        </div>\n" +
    "    </div>\n" +
    "    <div class=\"ngdialog-buttons mt\">\n" +
    "        <button type=\"button\" class=\"ngdialog-button ngdialog-button-primary\" ng-click=\"closeThisDialog()\">Close</button>\n" +
    "    </div></script>"
  );


  $templateCache.put('views/home/home.tpl.html',
    "<div class=\"home-page\"> <section class=\"top-container\" back-img=\"{{companyProfile.cover}}\"> <div class=\"fade-bg fade40\"></div> <div class=\"container\"> <div class=\"company-logo\"> <img ng-src=\"{{companyProfile.logo || 'assets/images/no-avatar.png'}}\" ng-class=\"{'no-src': !companyProfile.logo}\"> <input type=\"file\" accept=\"image/*\" file-upload style=\"display: none\" id=\"logoFile\" profile-attr=\"logo\"> <a class=\"edit-icon white\" ng-click=\"changeLogo()\"></a> </div> <h1 class=\"company-name\">{{companyProfile.name}}<a ng-href=\"/#/settings?main=company&sub=basic-info\" class=\"margin-left-15 edit-icon white\"></a></h1> <ul class=\"social-list no-margin\"> <li ng-show=\"companyProfile.facebook && companyProfile.facebook!=''\"> <a target=\"_blank\" href=\"/#/settings?main=company&sub=socials\"><i class=\"fa fa-facebook\"></i></a> </li> <li ng-show=\"companyProfile.twitter && companyProfile.twitter!=''\"> <a target=\"_blank\" href=\"/#/settings?main=company&sub=socials\"><i class=\"fa fa-twitter\"></i></a> </li> <li ng-show=\"companyProfile.instagram && companyProfile.instagram!=''\"> <a target=\"_blank\" href=\"/#/settings?main=company&sub=socials\"><i class=\"fa fa-instagram\"></i></a> </li> <li ng-show=\"companyProfile.google_plus && companyProfile.google_plus!=''\"> <a target=\"_blank\" href=\"/#/settings?main=company&sub=socials\"><i class=\"fa fa-google-plus\"></i></a> </li> <li ng-show=\"companyProfile.linkedin && companyProfile.linkedin!=''\"> <a target=\"_blank\" href=\"/#/settings?main=company&sub=socials\"><i class=\"fa fa-linkedin\"></i></a> </li> <li ng-show=\"companyProfile.youtube && companyProfile.youtube!=''\"> <a target=\"_blank\" href=\"/#/settings?main=company&sub=socials\"><i class=\"fa fa-youtube-play\"></i></a> </li> <li ng-show=\"companyProfile.github && companyProfile.github!=''\"> <a target=\"_blank\" href=\"/#/settings?main=company&sub=socials\"><i class=\"fa fa-github-alt\"></i></a> </li> <li ng-show=\"companyProfile.stackoverflow && companyProfile.stackoverflow!=''\"> <a target=\"_blank\" href=\"/#/settings?main=company&sub=socials\"><i class=\"fa fa-stack-overflow\"></i></a> </li> <li class=\"no-bg add-socials\" ng-hide=\"isActiveSocials\">Add {{companyProfile.name}}'s Social Accounts</li> <li class=\"no-bg\"> <a target=\"_blank\" href=\"/#/settings?main=company&sub=socials\" class=\"edit-icon white\"></a> </li> </ul> <!--\n" +
    "\t\t\t<div class=\"butotn-area\">\n" +
    "\t\t\t\t<button class=\"btn btn-info\">Send a Friend Request</button>\n" +
    "\t\t\t\t<button class=\"btn btn-success\">Create Conversation</button>\n" +
    "\t\t\t</div>\n" +
    "\t\t\t--> <input type=\"file\" accept=\"image/*\" file-upload style=\"display: none\" id=\"coverFile\" profile-attr=\"cover\"> <a class=\"edit-cover\" ng-click=\"changeCover()\"> <span class=\"edit-cover-icon\"></span> <span class=\"edit-cover-text\">Edit Cover</span> </a> </div> </section> <section class=\"main-container\"> <div class=\"container\"> <h5 class=\"page-title\">{{companyProfile.name}} Profile<a ng-href=\"/#/settings?main=company&sub=basic-info\" class=\"margin-left-15 edit-icon\"></a></h5> <div class=\"row\"> <div class=\"col-lg-3 col-md-3 col-sm-6 col-xs-6\"> <div class=\"basic-info\"> <h3>Website</h3> <a ng-href=\"companyProfile.website\">{{companyProfile.website}}</a> </div> <div class=\"basic-info\"> <h3>Headquarters</h3> <p>{{companyProfile.city}}, {{companyProfile.state}} {{companyProfile.country}}</p> </div> </div> <div class=\"col-lg-3 col-md-3 col-sm-6 col-xs-6\"> <div class=\"basic-info\"> <h3>Industry</h3> <p>{{companyProfile.industry}}</p> </div> <div class=\"basic-info\"> <h3>Company Size</h3> <p>{{companyProfile.company_size}}</p> </div> </div> <div class=\"col-lg-3 col-md-3 col-sm-6 col-xs-6\"> <div class=\"basic-info\"> <h3>Type</h3> <p>{{companyProfile.company_type}}</p> </div> <div class=\"basic-info\"> <h3>Founded</h3> <p>{{companyProfile.founded}}</p> </div> </div> <div class=\"col-lg-3 col-md-3 col-sm-6 col-xs-6\"> <div class=\"basic-info\"> <h3>Owner</h3> <div class=\"user-info\"> <img class=\"user-avatar\" alt=\"Nikolay Sergeev\" ng-src=\"{{companyProfile.owner.avatar || 'assets/images/no-avatar.png'}}\"> <div class=\"user-data\"> <h3 class=\"user-name\">{{companyProfile.owner.first_name}} {{companyProfile.owner.last_name}}</h3> <span class=\"user-level\">{{companyProfile.owner.role}}</span> </div> </div> </div> </div> </div> <div class=\"row basic-info-wrapper\"> <div class=\"col-lg-6 col-md-6 col-sm-12 col-xs-12\"> <div class=\"info\"> <div class=\"info-title clearfix\"> <h5 class=\"pull-left\">COMPANY DESCRIPTION</h5> <a class=\"edit-icon pull-right\" ng-href=\"/#/settings?main=company&sub=basic-info\"></a> </div> <div class=\"divider\"></div> <div class=\"info-body\" ng-nicescroll nice-option=\"{zindex: 999, autohidemode: true, cursorcolor: '#d4d4d4', cursorwidth: '7px', cursorborder: 'none', cursorborderradius: '7px'}\" ng-class=\"{'has-data': companyProfile.description}\"> <p class=\"comment\">Write your<br>company's description...</p> <div class=\"company-desc\"> <p>{{companyProfile.description}}</p> </div> </div> </div> </div> <div class=\"col-lg-6 col-md-6 col-sm-12 col-xs-12\"> <div class=\"info\"> <div class=\"info-title clearfix\"> <h5 class=\"pull-left\">{{companyProfile.name}} FRIENDS</h5> <a class=\"edit-icon pull-right\" ng-click=\"editFriends()\"></a> </div> <div class=\"divider\"></div> <div class=\"info-body\" ng-class=\"{'has-data': companyFriends.length!=0}\" ng-nicescroll nice-option=\"{zindex: 999, autohidemode: true, cursorcolor: '#d4d4d4', cursorwidth: '7px', cursorborder: 'none', cursorborderradius: '7px'}\"> <p class=\"comment\">Discover other companies<br>and add them as friends.</p> <ul class=\"friend-list\"> <li class=\"company-info clearfix\" ng-repeat=\"friend in companyFriends track by $index\"> <a href=\"/#/company-profile/{{friend.id}}\"> <img ng-src=\"{{friend.logo || 'assets/images/no-avatar.png'}}\" class=\"company-logo\"> <div class=\"company-data\"> <h3 class=\"company-name\">{{friend.name}}</h3> <span class=\"company-role\">{{friend.industry}}</span> </div> </a> </li> </ul> </div> </div> </div> <div class=\"col-lg-6 col-md-6 col-sm-12 col-xs-12\"> <div class=\"info\"> <div class=\"info-title clearfix\"> <h5 class=\"pull-left\">PEOPLE WHO WORK IN {{companyProfile.name}}</h5> <a class=\"edit-icon pull-right\" ng-href=\"/#/settings?main=team&sub=account-mng\"></a> </div> <div class=\"divider\"></div> <div class=\"info-body\" ng-class=\"{'has-data': companyMembers.length!=0}\" ng-nicescroll nice-option=\"{zindex: 999, autohidemode: true, cursorcolor: '#d4d4d4', cursorwidth: '7px', cursorborder: 'none', cursorborderradius: '7px'}\"> <p class=\"comment\">Invite people who work in<br>{{companyProfile.name}}</p> <ul class=\"fb-user-list\"> <li class=\"user-info clearfix\" ng-repeat=\"member in companyMembers track by $index\"> <img ng-src=\"{{member.avatar || 'assets/images/no-avatar.png'}}\" class=\"user-avatar\"> <div class=\"user-data\"> <h3 class=\"user-name\" ng-show=\"member.first_name!=null && member.first_name!=''\">{{member.first_name}} {{member.last_name}}</h3> <h3 class=\"user-name\" ng-hide=\"member.first_name!=null && member.first_name!=''\">{{member.email}}</h3> <span class=\"user-level\">{{member.role}}</span> </div> </li> </ul> </div> </div> </div> <div class=\"col-lg-6 col-md-6 col-sm-12 col-xs-12\"> <div class=\"info\"> <div class=\"info-title clearfix\"> <h5 class=\"pull-left\">{{companyProfile.name}} HAS AFFILIATED COMPANIES</h5> <a class=\"edit-icon pull-right\" ng-href=\"/#/settings?main=company&sub=affiliate-companies\"></a> </div> <div class=\"divider\"></div> <div class=\"info-body\" ng-class=\"{'has-data': companyProfile.affiliated_companies.length!=0}\" ng-nicescroll nice-option=\"{zindex: 999, autohidemode: true, cursorcolor: '#d4d4d4', cursorwidth: '7px', cursorborder: 'none', cursorborderradius: '7px'}\"> <p class=\"comment\">You have no affiliated companies.<br>You can add them in<br>profile settings.</p> <ul class=\"affli-company-list\" ng-repeat=\"company in affiCompanies track by $index\"> <li class=\"company-info clearfix\"> <a ng-href=\"/#/company-profile/{{company.id}}\"> <img ng-src=\"{{company.logo || 'assets/images/no-avatar.png'}}\" class=\"company-logo\"> <div class=\"company-data\"> <h3 class=\"company-name\">{{company.name}}</h3> <span class=\"company-role\">{{company.industry}}</span> </div> </a> </li> </ul> </div> </div> </div> </div> </div> </section> </div>"
  );


  $templateCache.put('views/index/index.tpl.html',
    "<div class=\"landing-page\"> <div class=\"intro\"> <div class=\"fade-bg\"></div> <div class=\"intro-body\"> <div class=\"container\"> <div class=\"row\"> <div class=\"col-md-12\"> <h1 class=\"brand-heading\">Connect and work with other companies<br>from around the world</h1> <button class=\"btn btn-info btn-lg get-started\" ng-click=\"getStarted()\">Join Bliff</button> </div> </div> </div> </div> </div> <script type=\"text/ng-template\" id=\"passwordResetCompletePopup\"><div class=\"ngdialog-message\">\n" +
    "            <h3>Congratulation</h3><hr>\n" +
    "            <div>Your password was updated!</div>\n" +
    "            <div>Now you can login.</div>\n" +
    "        </div>\n" +
    "        <div class=\"ngdialog-buttons mt\">\n" +
    "            <button type=\"button\" class=\"ngdialog-button ngdialog-button-primary\" ng-click=\"closeThisDialog()\">Close</button>\n" +
    "        </div></script> <script type=\"text/ng-template\" id=\"companyCreatedPopup\"><div class=\"ngdialog-message\">\n" +
    "            <h3>Congratulation</h3><hr>\n" +
    "            <div>Your company has been registered</div>\n" +
    "            <div>Please verify your account by verifing check your inbox for an activation link.</div>\n" +
    "        </div>\n" +
    "        <div class=\"ngdialog-buttons mt\">\n" +
    "            <button type=\"button\" class=\"ngdialog-button ngdialog-button-primary\" ng-click=\"closeThisDialog()\">Close</button>\n" +
    "        </div></script> <script type=\"text/ng-template\" id=\"magicLinkSentPopup\"><div class=\"ngdialog-message\">\n" +
    "            <h3>Magic Login</h3><hr>\n" +
    "            <div>We've sent a login link to your mail inbox.</div>\n" +
    "            <div>Please check your email</div>\n" +
    "        </div>\n" +
    "        <div class=\"ngdialog-buttons mt\">\n" +
    "            <button type=\"button\" class=\"ngdialog-button ngdialog-button-primary\" ng-click=\"closeThisDialog()\">Close</button>\n" +
    "        </div></script> <script type=\"text/ng-template\" id=\"passwordResetPopup\"><div class=\"ngdialog-message\">\n" +
    "            <h3>Password Reset</h3><hr>\n" +
    "            <div>We've sent a password reset link to your mail inbox.</div>\n" +
    "            <div>Please check your email.</div>\n" +
    "        </div>\n" +
    "        <div class=\"ngdialog-buttons mt\">\n" +
    "            <button type=\"button\" class=\"ngdialog-button ngdialog-button-primary\" ng-click=\"closeThisDialog()\">Close</button>\n" +
    "        </div></script> <script type=\"text/ng-template\" id=\"companyActivatedPopup\"><div class=\"ngdialog-message\">\n" +
    "            <h3>Verification Complete</h3><hr>\n" +
    "            <div>Your account is now active</div>\n" +
    "            <div>You can now login into your Bliff account.</div>\n" +
    "        </div>\n" +
    "        <div class=\"ngdialog-buttons mt\">\n" +
    "            <button type=\"button\" class=\"ngdialog-button ngdialog-button-primary\" ng-click=\"closeThisDialog()\">Close</button>\n" +
    "        </div></script> <section id=\"about\" class=\"steps-container gray-bg\"> <div class=\"container\"> <h2>Ultimate Business Networking Site:<br>Your Gateway to Business Development</h2> <div class=\"row\"> <div class=\"col-lg-4 col-md-4 col-sm-4 col-xs-12\"> <h4>Create your company's profile</h4> <p>It just takes a few minutes to setup your company´s profile, then you can start connecting and communicating with other companies.</p> </div> <div class=\"col-lg-4 col-md-4 col-sm-4 col-xs-12\"> <h4>Start Networking</h4> <p>Showcase your company, and connect with others. Browse companies in any industry sector across the globe. Start building a strategic business development network and boost your company´s relationships.</p> </div> <div class=\"col-lg-4 col-md-4 col-sm-4 col-xs-12\"> <h4>Make business relationships</h4> <p>With Bliff possiblites are endless. Create strategic partnerships, find sales leads or any kind of business idea that comes to your mind!</p> </div> </div> </div> </section> <section id=\"review\" class=\"company-container\"> <div class=\"container\"> <h2>What Companies Are Saying</h2> <div class=\"row\"> <div class=\"col-lg-12\"> <ul class=\"company-logos\"> </ul> </div> </div> <div class=\"row\"> <div class=\"col-lg-8 col-md-8 col-sm-10 col-xs-12 col-lg-offset-2 col-md-offset-2 col-sm-offset-1\"> <h3 class=\"text-center\">\"Excellent search capabilities, easy to setup company profile, happy to say we connected with a few companies already.\"</h3> </div> </div> <div class=\"row\"> <div class=\"col-lg-12 company-seo\"> <img class=\"user-avatar\" alt=\"Brad Tornton\" src=\"assets/images/avatar.png\"> <p class=\"user-name\">J. Harrell</p> <p class=\"user-level\">Product Manager at MyPet-Q</p> </div> </div> </div> </section> <!--<section id=\"team\" class=\"team-container gray-bg\">--> <!--<div class=\"container\">--> <!--<h2>Our Team</h2>--> <!--<div class=\"row\">--> <!--<div class=\"col-lg-4 col-md-4 col-sm-6 col-xs-12\">--> <!--<div class=\"team-member\">--> <!--<a href=\"javascript:void(0)\" class=\"twitter-link\"><i class=\"fa fa-twitter\" aria-hidden=\"true\"></i></a>--> <!--<div class=\"user-info\">--> <!--<img class=\"user-avatar\" alt=\"Carlos Gotlib\" src=\"assets/images/avatar.png\" />--> <!--<div class=\"user-data\">--> <!--<h3 class=\"user-name\">Carlos Gotlib</h3>--> <!--<span class=\"user-level\">CEO</span>--> <!--</div>--> <!--</div>--> <!--<div class=\"desc\">--> <!--<p>Since 2011, Carlos has been focusing on web and mobile technology and leading the team in various startups. The entire process from imagining and developing to producing new and innovative products to help improve everyone's is what he most enjoys. He has a background in law but a passion for entrepreneurship drives him.</p>--> <!--</div>--> <!--</div>--> <!--<div class=\"team-member\">--> <!--<a href=\"javascript:void(0)\" class=\"twitter-link\"><i class=\"fa fa-twitter\" aria-hidden=\"true\"></i></a>--> <!--<div class=\"user-info\">--> <!--<img class=\"user-avatar\" alt=\"Seung Cheol\" src=\"assets/images/avatar.png\" />--> <!--<div class=\"user-data\">--> <!--<h3 class=\"user-name\">Seung Cheol</h3>--> <!--<span class=\"user-level\">CFO</span>--> <!--</div>--> <!--</div>--> <!--<div class=\"desc\">--> <!--<p>We welcome conversation and reserve the right to remove any comments that constitute harassment, hate speech, remove any comments that constitute harassment, hate speech.</p>--> <!--</div>--> <!--</div>--> <!--</div>--> <!--</div>--> <!--</div>--> <!--</div>--> <!--</div>--> <!--</section>--> <div id=\"get_started\" class=\"intro bottom\"> <div class=\"intro-body\"> <div class=\"container\"> <div class=\"row\"> <div class=\"col-md-12\"> <h1 class=\"brand-heading\">Join the biggest business development network</h1> <button class=\"btn btn-info btn-lg get-started\" ng-click=\"getStarted()\">Join Bliff</button> </div> </div> </div> </div> </div> </div>"
  );


  $templateCache.put('views/invite/company.tpl.html',
    ""
  );


  $templateCache.put('views/invite/conversation.tpl.html',
    ""
  );


  $templateCache.put('views/invite/friend.tpl.html',
    ""
  );


  $templateCache.put('views/modal/change-pwd.modal.tpl.html',
    "<div class=\"modal-header\"> <span class=\"close-icon large-gray pull-right cursor-pointer\" ng-click=\"cancel()\" title=\"Close\"></span> </div> <div class=\"modal-body\"> <form name=\"changePwdForm\"> <h3>Set your new password</h3> <br> <div class=\"form-group\"> <input type=\"password\" class=\"form-control\" placeholder=\"New password\" ng-model=\"pwd\" minlength=\"8\" required> </div> <div class=\"form-group\"> <span ng-show=\"changePwdForm.confirmPwd.$error.compareTo\">Passwords doesn't match</span> <input type=\"password\" name=\"confirmPwd\" class=\"form-control\" placeholder=\"Confirm password\" ng-model=\"confirmPwd\" compare-to=\"pwd\" required minlength=\"8\"> </div> <div ng-messages=\"messages\" class=\"modal-error-list\" role=\"alert\" multiple> <h4 ng-message=\"reset_key\">Your link is invalid or outdated, please try to reset your password again.</h4> </div> <button class=\"btn btn-success btn-lg btn-block\" ng-click=\"changePwd()\">CHANGE PASSWORD</button> </form> </div>"
  );


  $templateCache.put('views/modal/create-conv.modal.tpl.html',
    "<div class=\"modal-header create-conv-modal-header\"> <span class=\"close-icon large-gray pull-right cursor-pointer\" ng-click=\"cancel()\" title=\"Close\"></span> <h4>Fill out the conversation request</h4> </div> <div class=\"modal-body create-conv-modal-body\"> <form class=\"create-conv-form\"> <div class=\"form-group\"> <label>Subject</label> <input type=\"text\" ng-model=\"subject\" class=\"form-control is-border input-sm conv-subject\" placeholder=\"Typing your subject\"> </div> <div class=\"form-group\"> <label>Description</label> <textarea ng-model=\"description\" class=\"form-control is-border input-sm conv-desc\" placeholder=\"Typing your friend reques description\"></textarea> </div> <div class=\"drag-drop-content\" ng-dropzone callbacks=\"dzCallbacks\" methods=\"dzMethods\" options=\"dzOptions\"> </div> <ul class=\"uploaded-file-list\"> </ul> <div class=\"form-group\"> <input ng-model=\"form_url\" type=\"text\" class=\"form-control\" placeholder=\"URL\"> </div> <div class=\"form-group\"> <input ng-model=\"form_link\" type=\"text\" class=\"form-control\" placeholder=\"Add a Link\"> </div> </form> </div> <div ng-class=\"{'show': show_spin}\" class=\"spin\"> <img src=\"/assets/images/spin.gif\"> </div> <div class=\"modal-footer create-conv-modal-footer\"> <button class=\"btn btn-success btn-block\" ng-click=\"uploadFiles()\">SEND CONVERSATION REQUEST</button> </div>"
  );


  $templateCache.put('views/modal/edit-friends.modal.tpl.html',
    "<div class=\"modal-header\"> <span class=\"close-icon large-white pull-right cursor-pointer\" ng-click=\"cancel()\" title=\"Close\"></span> <h4 class=\"modal-title\">{{companyProfile.name}} Friends ({{ companyFriends.length }})</h4> </div> <div class=\"modal-body\"> <form class=\"edit-friends-form\"> <div class=\"dropdown-wrapper\"> <div class=\"dropdown-custom white searchByIndustry\" ng-class=\"{'open': isSearchDropdownOpened}\"> <div class=\"title\" ng-click=\"toggleSearchDropdown()\">Search by industry</div> <div class=\"content\"> <div class=\"industry-list\" ng-nicescroll nice-option=\"{zindex: 999, autohidemode: true, cursorcolor: '#595959', cursorwidth: '7px', cursorborder: 'none', cursorborderradius: '7px'}\"> <div class=\"item\" ng-click=\"selectAllItem()\" ng-class=\"{'filtered': allFiltered}\">All</div> <div class=\"item\" ng-repeat=\"industry in industryList track by $index\" ng-click=\"selectItem(industry)\" ng-class=\"{'filtered': industry.filtered || allFiltered}\">{{industry.name}}</div> </div> <div class=\"button-area clearfix\"> <a ng-click=\"selectAllItem()\">Reset Filter</a> <a ng-click=\"applyFilter()\">Apply Filter</a> </div> </div> </div> </div> <div class=\"friend-list-wrapper\" ng-nicescroll nice-option=\"{zindex: 999, autohidemode: true, cursorcolor: '#595959', cursorwidth: '7px', cursorborder: 'none', cursorborderradius: '7px'}\"> <ul class=\"friend-list\"> <li ng-repeat=\"friend in companyFriends track by $index\"> <div class=\"company-info\"> <img ng-src=\"{{friend.logo || 'assets/images/no-avatar.png'}}\" class=\"company-logo\"> <div class=\"company-data\"> <h3 class=\"company-name\">{{friend.name}}</h3> <span class=\"company-role\">{{friend.industry}}</span> </div> </div> <div class=\"pull-right actions\"> <a type=\"button\" class=\"close-icon thin-white\" title=\"Close\" ng-click=\"deleteFriend(friend.id)\"></a> </div> </li> </ul> </div> </form> </div>"
  );


  $templateCache.put('views/modal/register-company.modal.tpl.html',
    "<div class=\"modal-header\"> <span class=\"close-icon large-gray pull-right cursor-pointer\" ng-click=\"cancel()\" title=\"Close\"></span> <a ng-click=\"back()\">Sign in to a different company</a> </div> <div class=\"modal-body\"> <form name=\"registerForm\"> <h3>Create a company</h3> <div class=\"form-group\"> <label>Enter your email address</label> <input type=\"email\" class=\"form-control\" placeholder=\"youremail@example.com\" ng-model=\"email\" required> </div> <button class=\"btn btn-info btn-lg btn-block\" ng-click=\"continue()\">CONTINUE</button> </form> </div>"
  );


  $templateCache.put('views/modal/register-member.modal.tpl.html',
    "<div class=\"modal-header\"> <span class=\"close-icon large-gray pull-right cursor-pointer\" ng-click=\"cancel()\" title=\"Close\"></span> </div> <div class=\"modal-body\"> <form> <h3>Become a member</h3> <div class=\"form-group\"> <input type=\"text\" class=\"form-control\" placeholder=\"First Name\" ng-model=\"firstName\" required> </div> <div class=\"form-group\"> <input type=\"text\" class=\"form-control\" placeholder=\"Last name\" ng-model=\"lastName\" required> </div> </form> <div class=\"divider\"></div> <form name=\"registerForm\"> <h3>Set your password</h3> <div class=\"form-group\"> <input type=\"password\" class=\"form-control\" placeholder=\"Password\" ng-model=\"pwd\" required minlength=\"8\"> </div> <div class=\"form-group\"> <span ng-show=\"registerForm.confirmPwd.$error.compareTo\">Passwords doesn't match</span> <input type=\"password\" name=\"confirmPwd\" class=\"form-control\" placeholder=\"Confirm password\" ng-model=\"confirmPwd\" compare-to=\"pwd\" required minlength=\"8\"> </div> <div ng-messages=\"messages\" class=\"modal-error-list\" role=\"alert\" multiple> <h4 ng-message=\"company_name\">Company name is taken</h4> <h4 ng-message=\"email\">Email is taken</h4> </div> <button class=\"btn btn-success btn-lg btn-block\" ng-click=\"createCompany()\">JOIN COMPANY</button> </form> </div>"
  );


  $templateCache.put('views/modal/register-pwd.modal.tpl.html',
    "<div class=\"modal-header\"> <span class=\"close-icon large-gray pull-right cursor-pointer\" ng-click=\"cancel()\" title=\"Close\"></span> <a ng-click=\"back()\">Sign in to a different company</a> </div> <div class=\"modal-body\"> <form> <h3>Create a company</h3> <div class=\"form-group\"> <input type=\"text\" class=\"form-control\" placeholder=\"First Name\" ng-model=\"firstName\" required> </div> <div class=\"form-group\"> <input type=\"text\" class=\"form-control\" placeholder=\"Last name\" ng-model=\"lastName\" required> </div> </form> <div class=\"divider\"></div> <form name=\"registerForm\"> <h3>Set your password</h3> <div class=\"form-group\"> <input type=\"password\" class=\"form-control\" placeholder=\"Password\" ng-model=\"pwd\" required minlength=\"8\"> </div> <div class=\"form-group\"> <span ng-show=\"registerForm.confirmPwd.$error.compareTo\">Passwords doesn't match</span> <input type=\"password\" name=\"confirmPwd\" class=\"form-control\" placeholder=\"Confirm password\" ng-model=\"confirmPwd\" compare-to=\"pwd\" required minlength=\"8\"> </div> <div ng-messages=\"messages\" class=\"modal-error-list\" role=\"alert\" multiple> <h4 ng-message=\"company_name\">Company name is taken</h4> <h4 ng-message=\"email\">Email is taken</h4> </div> <button class=\"btn btn-success btn-lg btn-block\" ng-click=\"createCompany()\">CREATE COMPANY</button> </form> </div>"
  );


  $templateCache.put('views/modal/reset-pwd.modal.tpl.html',
    "<div class=\"modal-header\"> <span class=\"close-icon large-gray pull-right cursor-pointer\" ng-click=\"cancel()\" title=\"Close\"></span> <a ng-click=\"back()\">Back</a> </div> <div class=\"modal-body\"> <form name=\"resetPwdForm\"> <h3>Reset password</h3> <div class=\"form-group\"> <label>Please enter your email address</label> <input type=\"email\" class=\"form-control\" placeholder=\"youremail@example.com\" ng-model=\"email\" required> </div> <button class=\"btn btn-success btn-lg btn-block\" ng-click=\"resetPwd()\">RESET PASSWORD</button> </form> </div>"
  );


  $templateCache.put('views/modal/signin-email.modal.tpl.html',
    "<div class=\"modal-header\"> <span class=\"close-icon large-gray pull-right cursor-pointer\" ng-click=\"cancel()\" title=\"Close\"></span> <a ng-click=\"back()\">Sign in to a different company</a> </div> <div class=\"modal-body\"> <form name=\"signInForm\"> <h3>Sign in to {{companyName}}</h3> <div class=\"form-group\"> <label>Enter your email address</label> <input type=\"email\" class=\"form-control\" placeholder=\"youremail@example.com\" ng-model=\"email\" required> </div> <button class=\"btn btn-success btn-lg btn-block\" ng-click=\"continue()\">CONTINUE</button> </form> </div>"
  );


  $templateCache.put('views/modal/signin-pwd.modal.tpl.html',
    "<div class=\"modal-header\"> <span class=\"close-icon large-gray pull-right cursor-pointer\" ng-click=\"cancel()\" title=\"Close\"></span> <a ng-click=\"back()\">Sign in with a different email address</a> </div> <div class=\"modal-body\"> <form> <h3>Sign in with a different email address</h3> <p>Please check your email for a link<br>to sign in to your company account</p> <div ng-show=\"msg_error_link\" style=\"padding-bottom:10px\">{{msg_error_link}}</div> <button class=\"btn btn-success btn-lg btn-block\" ng-click=\"signinWithDiff()\">SIGN IN</button> </form> <div class=\"divider\"></div> <form class=\"signup-modal-form\" name=\"signInForm\"> <h3>Or enter your password<a class=\"pull-right\" ng-click=\"forgotPwd()\">Forgot password?</a></h3> <div class=\"form-group\"> <span ng-show=\"msg_error_pwd\">{{msg_error_pwd}}</span> <input type=\"password\" class=\"form-control\" placeholder=\"Password\" ng-model=\"pwd\" required minlength=\"8\"> </div> <button class=\"btn btn-info btn-lg btn-block\" ng-click=\"signin()\">SIGN IN</button> </form> </div>"
  );


  $templateCache.put('views/modal/signin.modal.tpl.html',
    "<div class=\"modal-header\"> <span class=\"close-icon large-gray pull-right cursor-pointer\" ng-click=\"cancel()\" title=\"Close\"></span> <h4 class=\"modal-title\">Sign in or create new account</h4> </div> <div class=\"modal-body\"> <form class=\"signin-modal-form\" name=\"signInForm\"> <h3>Sign in</h3> <div class=\"form-group\"> <label>Enter your company name</label> <input type=\"text\" class=\"form-control\" placeholder=\"Company Name\" ng-model=\"signinCompanyName\" required> </div> <button class=\"btn btn-success btn-lg btn-block\" ng-click=\"signinNext()\">NEXT</button> </form> <div class=\"divider\"></div> <form class=\"signup-modal-form\" name=\"signUpForm\"> <h3>Create your company</h3> <div class=\"form-group\"> <label>Enter your company name</label> <input type=\"text\" class=\"form-control\" placeholder=\"Company Name\" ng-model=\"signupCompanyName\" required> </div> <button class=\"btn btn-info btn-lg btn-block\" ng-click=\"signupNext()\">NEXT</button> </form> </div>"
  );


  $templateCache.put('views/settings/company-settings.tpl.html',
    "<div class=\"row\"> <div class=\"col-lg-4 col-md-4 col-sm-4 col-xs-12 left-content\"> <ul class=\"settings-sub-tabs\"> <li ng-class=\"{'active': selSubTab=='basic-info'}\"><a ng-href=\"/#/settings?main=company&sub=basic-info\">{{companyProfile.name}} Profile</a></li> <li ng-class=\"{'active': selSubTab=='socials'}\"><a ng-href=\"/#/settings?main=company&sub=socials\">Social accounts</a></li> <li ng-class=\"{'active': selSubTab=='affiliate-companies'}\"><a ng-href=\"/#/settings?main=company&sub=affiliate-companies\">Affiliated Companies</a></li> </ul> <button class=\"btn btn-success\" ng-click=\"updateSettings('company')\">Update Settings</button> </div> <div class=\"col-lg-8 col-md-8 col-sm-8 col-xs-12\"> <!------ Settings / Company Settings / Basic Information ------> <div ng-show=\"selSubTab=='basic-info'\"> <form class=\"settings-form\" name=\"settingform\"> <div class=\"row\"> <div class=\"col-md-12\"> <div class=\"form-group\"> <input type=\"text\" class=\"form-control\" placeholder=\"Name\" ng-model=\"companyProfile.name\"> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-12\"> <div class=\"form-group\"> <label class=\"control-label\">Description</label> <textarea class=\"form-control\" placeholder=\"Typing company's description\" style=\"height: 200px\" ng-model=\"companyProfile.description\"></textarea> </div> </div> </div> <div class=\"row\"> <div class=\"col-md-6 col-xs-12\"> <div class=\"form-group\"> <div role=\"alert\"> <span class=\"error\" ng-show=\"settingform.website.$error.url\"> Not a valid url (e.g. http://website.com)</span> </div> <input type=\"url\" class=\"form-control link\" name=\"website\" placeholder=\"Website\" ng-model=\"companyProfile.website\"> </div> <div class=\"form-group\"> <div class=\"dropdown-wrapper\"> <div class=\"dropdown-custom\" ng-class=\"{'open': isComsizeDropdownOpened}\"> <div class=\"title\" ng-click=\"toggleSizeDropdown()\" ng-class=\"{'has-data': companyProfile.company_size}\">{{companyProfile.company_size || 'Company Size'}}</div> <div class=\"content\" ng-nicescroll nice-option=\"{zindex: 999, autohidemode: true, cursorcolor: '#595959', cursorwidth: '7px', cursorborder: 'none', cursorborderradius: '7px'}\"> <div ng-click=\"selectSize(size)\" class=\"item\" ng-repeat=\"size in sizeList\">{{size.name}}</div> </div> </div> </div> </div> <div class=\"form-group\"> <div class=\"dropdown-wrapper\"> <div class=\"dropdown-custom\" ng-class=\"{'open': isCountryDropdownOpened}\"> <div class=\"title\" ng-click=\"toggleCountryDropdown()\" ng-class=\"{'has-data': companyProfile.country}\">{{companyProfile.country || 'Country'}}</div> <div class=\"content\" ng-nicescroll nice-option=\"{zindex: 999, autohidemode: true, cursorcolor: '#595959', cursorwidth: '7px', cursorborder: 'none', cursorborderradius: '7px'}\"> <div ng-click=\"selectCountry(country)\" class=\"item\" ng-repeat=\"country in countryList\">{{country.name}}</div> </div> </div> </div> </div> <div class=\"form-group\"> <input type=\"text\" class=\"form-control\" placeholder=\"City\" ng-model=\"companyProfile.city\"> </div> </div> <div class=\"col-md-6 col-xs-12\"> <div class=\"form-group\"> <div class=\"dropdown-wrapper\"> <div class=\"dropdown-custom\" ng-class=\"{'open': isTypeDropdownOpened}\"> <div class=\"title\" ng-click=\"toggleTypeDropdown()\" ng-class=\"{'has-data': companyProfile.company_type}\">{{companyProfile.company_type || 'Type'}}</div> <div class=\"content\" ng-nicescroll nice-option=\"{zindex: 999, autohidemode: true, cursorcolor: '#595959', cursorwidth: '7px', cursorborder: 'none', cursorborderradius: '7px'}\"> <div ng-click=\"selectType(type)\" class=\"item\" ng-repeat=\"type in companyTypeList\">{{type.name}}</div> </div> </div> </div> </div> <div class=\"form-group\"> <input type=\"text\" class=\"form-control\" placeholder=\"Founded\" ng-model=\"companyProfile.founded\"> </div> <div class=\"form-group\"> <div class=\"dropdown-wrapper\"> <div class=\"dropdown-custom\" ng-show=\"companyProfile.country == 'United States'\" ng-class=\"{'open': isStateDropdownOpened}\"> <div class=\"title\" ng-click=\"toggleStateDropdown()\" ng-class=\"{'has-data': companyProfile.state}\">{{companyProfile.state || 'State'}}</div> <div class=\"content\" ng-nicescroll nice-option=\"{zindex: 999, autohidemode: true, cursorcolor: '#595959', cursorwidth: '7px', cursorborder: 'none', cursorborderradius: '7px'}\"> <div ng-click=\"selectState(state)\" class=\"item\" ng-repeat=\"state in stateList\">{{state.name}}</div> </div> </div> </div> </div> <div class=\"form-group\"> <div class=\"dropdown-wrapper\"> <div class=\"dropdown-custom\" ng-class=\"{'open': isIndustryDropdownOpened}\"> <div class=\"title\" ng-click=\"toggleIndustryDropdown()\" ng-class=\"{'has-data': companyProfile.industry}\">{{companyProfile.industry || 'Industry'}}</div> <div class=\"content\" ng-nicescroll nice-option=\"{zindex: 999, autohidemode: true, cursorcolor: '#595959', cursorwidth: '7px', cursorborder: 'none', cursorborderradius: '7px'}\"> <div ng-click=\"selectIndustry(industry)\" class=\"item\" ng-repeat=\"industry in industryList\">{{industry.name}}</div> </div> </div> </div> </div> </div> </div> </form> </div> <!------ Settings / Company Settings / Socials ------> <div ng-show=\"selSubTab=='socials'\"> <form class=\"settings-form\" name=\"$parent.socialForm\"> <h3 class=\"settings-title\">Add company's social accounts.</h3> <div class=\"row\"> <div class=\"col-md-12\"> <ul class=\"socials-settings\"> <li id=\"facebook\"> <span class=\"social-icon\"><i class=\"fa fa-facebook\"></i></span> <div role=\"alert\"> <span class=\"error\" ng-show=\"socialForm.fb.$error.url\"> Not a valid url (e.g. http://website.com) </span> </div> <input type=\"url\" name=\"fb\" class=\"form-control\" placeholder=\"Facebook\" ng-model=\"socialURL.facebook\" ng-focus=\"focusSocialURL('facebook')\"> <div class=\"social-actions\"> <a class=\"check cursor-pointer\" ng-show=\"socialURLAction['facebook']=='update'\" ng-click=\"saveSocialURL('facebook')\"></a> <a class=\"close-icon thin-gray cursor-pointer\" ng-show=\"socialURLAction['facebook']=='update'\" ng-click=\"cancelSocialURL('facebook')\"></a> <button class=\"btn btn-sm btn-success btn-save\" ng-show=\"socialURLAction['facebook']=='create'\" ng-click=\"saveSocialURL('facebook')\">Save</button> </div> </li> <li id=\"twitter\"> <span class=\"social-icon\"><i class=\"fa fa-twitter\"></i></span> <div role=\"alert\"> <span class=\"error\" ng-show=\"socialForm.tw.$error.url\"> Not a valid url (e.g. http://website.com) </span> </div> <input name=\"tw\" type=\"url\" class=\"form-control\" placeholder=\"Twitter\" ng-model=\"socialURL.twitter\" ng-focus=\"focusSocialURL('twitter')\"> <div class=\"social-actions\"> <a class=\"check cursor-pointer\" ng-show=\"socialURLAction['twitter']=='update'\" ng-click=\"saveSocialURL('twitter')\"></a> <a class=\"close-icon thin-gray cursor-pointer\" ng-show=\"socialURLAction['twitter']=='update'\" ng-click=\"cancelSocialURL('twitter')\"></a> <button class=\"btn btn-sm btn-success btn-save\" ng-show=\"socialURLAction['twitter']=='create'\" ng-click=\"saveSocialURL('twitter')\">Save</button> </div> </li> <li id=\"instagram\"> <span class=\"social-icon\"><i class=\"fa fa-instagram\"></i></span> <div role=\"alert\"> <span class=\"error\" ng-show=\"socialForm.ins.$error.url\"> Not a valid url (e.g. http://website.com) </span> </div> <input id=\"ins\" type=\"url\" class=\"form-control\" placeholder=\"Instagram\" ng-model=\"socialURL.instagram\" ng-focus=\"focusSocialURL('instagram')\"> <div class=\"social-actions\"> <a class=\"check cursor-pointer\" ng-show=\"socialURLAction['instagram']=='update'\" ng-click=\"saveSocialURL('instagram')\"></a> <a class=\"close-icon thin-gray cursor-pointer\" ng-show=\"socialURLAction['instagram']=='update'\" ng-click=\"cancelSocialURL('instagram')\"></a> <button class=\"btn btn-sm btn-success btn-save\" ng-show=\"socialURLAction['instagram']=='create'\" ng-click=\"saveSocialURL('instagram')\">Save</button> </div> </li> <li id=\"google_plus\"> <span class=\"social-icon\"><i class=\"fa fa-google-plus\"></i></span> <input type=\"url\" class=\"form-control\" placeholder=\"Google+\" ng-model=\"socialURL.google_plus\" ng-focus=\"focusSocialURL('google_plus')\"> <div class=\"social-actions\"> <a class=\"check cursor-pointer\" ng-show=\"socialURLAction['google_plus']=='update'\" ng-click=\"saveSocialURL('google_plus')\"></a> <a class=\"close-icon thin-gray cursor-pointer\" ng-show=\"socialURLAction['google_plus']=='update'\" ng-click=\"cancelSocialURL('google_plus')\"></a> <button class=\"btn btn-sm btn-success btn-save\" ng-show=\"socialURLAction['google_plus']=='create'\" ng-click=\"saveSocialURL('google_plus')\">Save</button> </div> </li> <li id=\"linkedin\"> <span class=\"social-icon\"><i class=\"fa fa-linkedin\"></i></span> <input type=\"url\" class=\"form-control\" placeholder=\"Linkedin\" ng-model=\"socialURL.linkedin\" ng-focus=\"focusSocialURL('linkedin')\"> <div class=\"social-actions\"> <a class=\"check cursor-pointer\" ng-show=\"socialURLAction['linkedin']=='update'\" ng-click=\"saveSocialURL('linkedin')\"></a> <a class=\"close-icon thin-gray cursor-pointer\" ng-show=\"socialURLAction['linkedin']=='update'\" ng-click=\"cancelSocialURL('linkedin')\"></a> <button class=\"btn btn-sm btn-success btn-save\" ng-show=\"socialURLAction['linkedin']=='create'\" ng-click=\"saveSocialURL('linkedin')\">Save</button> </div> </li> <li id=\"youtube\"> <span class=\"social-icon\"><i class=\"fa fa-youtube-play\"></i></span> <input type=\"url\" class=\"form-control\" placeholder=\"Youtube\" ng-model=\"socialURL.youtube\" ng-focus=\"focusSocialURL('youtube')\"> <div class=\"social-actions\"> <a class=\"check cursor-pointer\" ng-show=\"socialURLAction['youtube']=='update'\" ng-click=\"saveSocialURL('youtube')\"></a> <a class=\"close-icon thin-gray cursor-pointer\" ng-show=\"socialURLAction['youtube']=='update'\" ng-click=\"cancelSocialURL('youtube')\"></a> <button class=\"btn btn-sm btn-success btn-save\" ng-show=\"socialURLAction['youtube']=='create'\" ng-click=\"saveSocialURL('youtube')\">Save</button> </div> </li> <li id=\"github\"> <span class=\"social-icon\"><i class=\"fa fa-github-alt\"></i></span> <input type=\"url\" class=\"form-control\" placeholder=\"Github\" ng-model=\"socialURL.github\" ng-focus=\"focusSocialURL('github')\"> <div class=\"social-actions\"> <a class=\"check cursor-pointer\" ng-show=\"socialURLAction['github']=='update'\" ng-click=\"saveSocialURL('github')\"></a> <a class=\"close-icon thin-gray cursor-pointer\" ng-show=\"socialURLAction['github']=='update'\" ng-click=\"cancelSocialURL('github')\"></a> <button class=\"btn btn-sm btn-success btn-save\" ng-show=\"socialURLAction['github']=='create'\" ng-click=\"saveSocialURL('github')\">Save</button> </div> </li> <li id=\"stackoverflow\"> <span class=\"social-icon\"><i class=\"fa fa-stack-overflow\"></i></span> <input type=\"url\" class=\"form-control\" placeholder=\"Stackoverflow\" ng-model=\"socialURL.stackoverflow\" ng-focus=\"focusSocialURL('stackoverflow')\"> <div class=\"social-actions\"> <a class=\"check cursor-pointer\" ng-show=\"socialURLAction['stackoverflow']=='update'\" ng-click=\"saveSocialURL('stackoverflow')\"></a> <a class=\"close-icon thin-gray cursor-pointer\" ng-show=\"socialURLAction['stackoverflow']=='update'\" ng-click=\"cancelSocialURL('stackoverflow')\"></a> <button class=\"btn btn-sm btn-success btn-save\" ng-show=\"socialURLAction['stackoverflow']=='create'\" ng-click=\"saveSocialURL('stackoverflow')\">Save</button> </div> </li> </ul> </div> </div> </form> </div> <!------ Settings / Company Settings / Affliated Companies ------> <div ng-show=\"selSubTab=='affiliate-companies'\"> <form class=\"settings-form\"> <h3 class=\"settings-title\">Add affiliated companies</h3> <div class=\"row\"> <div class=\"col-md-12\"> <ul class=\"affi-list\"> <li ng-if=\"company.id!=companyProfile.id\" ng-repeat=\"company in companyList track by $index\" ng-class=\"{'affiliated': isAffiliated(company)}\"> <img class=\"company-icon\" ng-src=\"{{company.logo || 'assets/images/no-avatar.png'}}\"> <div class=\"company-name\">{{company.name || company.owner.email}}</div> <div class=\"actions\"> <button class=\"btn btn-sm btn-success save\" ng-click=\"sendAffilateRequest(company.id)\">Save</button> <a class=\"check\"></a> <a class=\"close-icon thin-gray cursor-pointer delete\" ng-click=\"deleteAffilateCompany(company.id)\"></a> </div> </li> </ul> </div> </div> </form> </div> </div> </div>"
  );


  $templateCache.put('views/settings/index.tpl.html',
    "<div class=\"settings-page\"> <div class=\"container\"> <div class=\"top-container\"> <div class=\"row\"> <div class=\"col-lg-4 col-md-4 col-sm-4 col-xs-12\"> <h1>Settings</h1> </div> <div class=\"col-lg-8 col-md-8 col-sm-8 col-xs-12\"> <ul class=\"settings-main-tabs\"> <li ng-class=\"{'active': selMainTab=='personal'}\"><a ng-href=\"/#/settings?main=personal&sub=personal-profile\">Personal Settings</a></li> <li ng-show=\"isOwner\" ng-class=\"{'active': selMainTab=='company'}\"><a ng-href=\"/#/settings?main=company&sub=basic-info\">Company Settings</a></li> <li ng-show=\"isOwner\" ng-class=\"{'active': selMainTab=='team'}\"><a ng-href=\"/#/settings?main=team&sub=account-mng\">Manage Company Team</a></li> </ul> </div> </div> </div> <div class=\"main-container\"> <div ng-include=\"'views/settings/personal-settings.tpl.html'\" ng-show=\"selMainTab=='personal'\"></div> <div ng-include=\"'views/settings/company-settings.tpl.html'\" ng-show=\"isOwner && selMainTab=='company'\"></div> <div ng-include=\"'views/settings/team-settings.tpl.html'\" ng-show=\"isOwner && selMainTab=='team'\"></div> </div> </div> </div>"
  );


  $templateCache.put('views/settings/personal-settings.tpl.html',
    "<div class=\"row\"> <div class=\"col-lg-4 col-md-4 col-sm-4 col-xs-12 left-content\"> <ul class=\"settings-sub-tabs\"> <li ng-class=\"{'active': selSubTab=='personal-profile'}\"><a ng-href=\"/#/settings?main=personal&sub=personal-profile\">Profile</a></li> <li ng-class=\"{'active': selSubTab=='personal-pwd'}\"><a ng-href=\"/#/settings?main=personal&sub=personal-pwd\">Password</a></li> </ul> <button class=\"btn btn-success\" ng-click=\"updateSettings('personal')\">Update Settings</button> </div> <div class=\"col-lg-8 col-md-8 col-sm-8 col-xs-12\"> <!-- Personal Setting -> Profile start --> <div ng-show=\"selSubTab=='personal-profile'\"> <div class=\"update-avatar\"> <h3>Change your profile picture</h3> <div class=\"user-avatar\"> <img class=\"user-avatar\" ng-src=\"{{personalProfile.avatar || 'assets/images/no-avatar.png'}}\"> <input type=\"file\" accept=\"image/*\" file-upload style=\"display: none\" id=\"logoFile\" profile-attr=\"avatar\"> <a class=\"edit-icon white\" ng-click=\"changeAvatar()\"></a> </div> </div> <form class=\"settings-form\" name=\"$parent.profileForm\"> <div class=\"row\"> <div class=\"col-md-6 col-xs-12\"> <div class=\"form-group\"> <input type=\"text\" class=\"form-control\" placeholder=\"First Name\" ng-model=\"personalProfile.first_name\"> </div> <div class=\"form-group\"> <input type=\"text\" class=\"form-control\" placeholder=\"Role in The Company\" ng-model=\"personalProfile.role\"> </div> <div class=\"form-group\"> <input type=\"email\" class=\"form-control\" placeholder=\"example@email.com\" ng-model=\"personalProfile.email\"> </div> </div> <div class=\"col-md-6 col-xs-12\"> <div class=\"form-group\"> <input type=\"text\" class=\"form-control\" placeholder=\"Last Name\" ng-model=\"personalProfile.last_name\"> </div> <div class=\"form-group\"> <div class=\"dropdown-wrapper\"> <div class=\"dropdown-custom\" ng-class=\"{'open': isDepartDropdownOpened}\"> <div class=\"title\" ng-click=\"toggleDepartDropdown()\">{{personalProfile.department}}</div> <div class=\"content\" ng-nicescroll nice-option=\"{zindex: 999, autohidemode: true, cursorcolor: '#595959', cursorwidth: '7px', cursorborder: 'none', cursorborderradius: '7px'}\"> <div ng-click=\"selectDepart(depart)\" class=\"item\" ng-repeat=\"depart in departList\">{{depart.name}}</div> </div> </div> </div> </div> </div> </div> </form> </div> <!-- Personal Setting -> Profile end --> <!-- Personal Setting -> Password start --> <div ng-show=\"selSubTab=='personal-pwd'\"> <form class=\"settings-form\" name=\"$parent.changePwdForm\"> <h3 class=\"settings-title\">Change your password</h3> <div ng-show=\"msg\">{{ msg }}</div> <div class=\"row\"> <div class=\"col-md-6 col-xs-12\"> <div class=\"form-group\"> <input type=\"password\" class=\"form-control\" placeholder=\"Old Password\" ng-model=\"$parent.oldPwd\" minlength=\"8\" required> </div> </div> <div class=\"col-md-6 col-xs-12\"> <div class=\"form-group\"> <input type=\"password\" class=\"form-control\" placeholder=\"New Password\" ng-model=\"$parent.newPwd\" minlength=\"8\" required> </div> </div> </div> </form> </div> <!-- Personal Setting -> Password end --> </div> </div>"
  );


  $templateCache.put('views/settings/team-settings.tpl.html',
    "<div class=\"row\"> <div class=\"col-lg-4 col-md-4 col-sm-4 col-xs-12 left-content\"> <ul class=\"settings-sub-tabs\"> <li ng-class=\"{'active': selSubTab=='account-mng'}\"><a ng-href=\"/#/settings?main=team&sub=account-mng\">Account Management</a></li> </ul> </div> <div class=\"col-lg-8 col-md-8 col-sm-8 col-xs-12\"> <div ng-show=\"selSubTab=='account-mng'\"> <form class=\"settings-form member\" name=\"$parent.teamForm\"> <h3 class=\"settings-title\">Add your team members to invite them to conversations</h3> <div class=\"row\"> <div class=\"col-md-6\"> <input type=\"email\" class=\"form-control member-email\" placeholder=\"Send an invite by email address\" ng-model=\"newMember.email\" required> </div> <div class=\"col-md-6\"> <div class=\"form-group\"> <div class=\"dropdown-wrapper\"> <div class=\"dropdown-custom\" ng-class=\"{'open': isDepartDropdownOpened}\"> <div class=\"title\" ng-click=\"toggleDepartDropdown()\"><span ng-show=\"newMember.department\" class=\"token-content\">{{newMember.department}}</span><span class=\"close-icon thin-white\"></span></div> <div class=\"content\" ng-nicescroll nice-option=\"{zindex: 999, autohidemode: true, cursorcolor: '#595959', cursorwidth: '7px', cursorborder: 'none', cursorborderradius: '7px'}\"> <div ng-click=\"selectMemberDepart(depart)\" class=\"item\" ng-repeat=\"depart in departList\">{{depart.name}}</div> </div> </div> </div> </div> </div> </div> <button class=\"btn btn-success\" ng-click=\"updateSettings('team')\">Add new team member</button> <div class=\"row\"> <div class=\"col-md-12\"> <div class=\"invite-wrapper\"> <h4>People already invited</h4> <div class=\"departments-area pull-left\"> <div class=\"title clearfix\"> <span class=\"pull-left\">Departments</span> <span class=\"pull-right unlocked\">Unlocked</span> </div> <div class=\"divider\"></div> <div class=\"content\" ng-nicescroll nice-option=\"{zindex: 999, autohidemode: true, cursorcolor: '#636363', cursorwidth: '7px', cursorborder: 'none', cursorborderradius: '7px'}\"> <ul class=\"departments\"> <li ng-click=\"filterMember('All')\" ng-class=\"{'active': filteredDepart=='All'}\"> <span class=\"depart-name\">All</span> <span class=\"close-icon thin-white pull-right\"></span> </li> <li ng-repeat=\"depart in departList track by $index\" ng-click=\"filterMember(depart.name)\" ng-class=\"{'active': filteredDepart==depart.name}\"> <span class=\"depart-name\">{{depart.name}}</span> <span class=\"close-icon thin-white pull-right\"></span> </li> </ul> </div> </div> <div class=\"departments-area special-list pull-left\"> <div class=\"title clearfix\"> <span class=\"pull-left\">Specialists</span> <span class=\"pull-right locked\">Locked</span> </div> <div class=\"divider\"></div> <div class=\"content\" ng-nicescroll nice-option=\"{zindex: 999, autohidemode: true, cursorcolor: '#636363', cursorwidth: '7px', cursorborder: 'none', cursorborderradius: '7px'}\"> <ul class=\"specialists\"> <li ng-repeat=\"member in filteredMembers track by $index\"> <div class=\"user-info\"> <img class=\"user-avatar\" ng-src=\"{{member.avatar || 'assets/images/no-avatar.png'}}\"> <div class=\"user-data\"> <p class=\"user-name\" ng-show=\"member.first_name!=null && member.first_name!=''\">{{member.first_name}} {{member.last_name}}</p> <p class=\"user-name\" ng-hide=\"member.first_name!=null && member.first_name!=''\">{{member.email}}</p> <p class=\"user-level\">{{member.role}}</p> </div> <span class=\"close-icon thin-gray pull-right\" ng-click=\"deleteMember(member.id)\" ng-hide=\"member.email === companyProfile.owner.email\"></span> </div> </li> </ul> </div> </div> </div> </div> </div> </form> </div> </div> </div>"
  );

}]);

'use strict';
angular
    .module('app')
    .controller('IndexController',
            ['$scope', '$rootScope', '$uibModal', '$location',
            function($scope, $rootScope, $uibModal, $location) {
        $rootScope.hasMenuBG = false;

    	$scope.getStarted = function() {
    		var modalInstance = $uibModal.open({
    			animation: true,
    			templateUrl: 'views/modal/signin.modal.tpl.html',
    			controller: 'SigninModalCtrl',
    			windowClass: 'vcenter-modal'
    		});
    		modalInstance.result.then(
    			function(data) {

    			},
    			function() {
    				console.info('Modal dismissed at: ' + new Date());
    			}
			);
    	};
    }]);

'use strict';
angular
    .module('app')
    .controller('HeaderController', ['$scope', '$rootScope', '$uibModal', 'localStorageService', '$location', 'Auth', 'Chat',
            function($scope, $rootScope, $uibModal, localStorageService, $location, Auth, Chat) {
        $scope.signin = function () {
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'views/modal/signin.modal.tpl.html',
                controller: 'SigninModalCtrl',
                windowClass: 'vcenter-modal'
            });
            modalInstance.result.then(
                function(data) {

                },
                function() {
                    console.info('Modal dismissed at: ' + new Date());
                }
            );
        };

        $scope.link_accept = function(n) {
            var link = n.link;
            $scope.link_deny(n);
            link = '/' + link.split('/').splice(3).join('/');
            window.location = link;
        }

        $scope.link_deny = function(n) {
            var data = {
                link: ''
            }
            n.link = '';
            Chat.updateNotifications(n.id, data).then(function(resp) {
                console.log('mark used', n, resp);
            });
        }

        $scope.markAsRead = function(n) {
            if (n.is_viewed) {
                return;
            }
            n.is_viewed = true;
            var data = {
                is_viewed: true
            }
            $rootScope.globals.notifications_new -= 1;
            localStorageService.set('globals', $rootScope.globals);
            Chat.updateNotifications(n.id, data).then(function(resp) {
                console.log('mark as read', n, resp);
            });
        }

        $scope.getFormattedTime = function(datetime_input){
            var datetime = moment(datetime_input).fromNow();
            return datetime;
        }

        $scope.signout = function () {
            $rootScope.isAuthorized = false;
            $rootScope.isOwner = false;
            Auth.clearCredentials();
            $location.path('/');
        };
    }]);

'use strict';
angular
    .module('app')
    .controller('FooterController', ['$scope', '$rootScope', '$uibModal', 'localStorageService', '$location', 'Auth', 'Chat',
            function($scope, $rootScope, $uibModal, localStorageService, $location, Auth, Chat) {
        $scope.signin = function () {
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'views/modal/signin.modal.tpl.html',
                controller: 'SigninModalCtrl',
                windowClass: 'vcenter-modal'
            });
            modalInstance.result.then(
                function(data) {

                },
                function() {
                    console.info('Modal dismissed at: ' + new Date());
                }
            );
        };
    }]);

'use strict';

angular
    .module('app')
    .controller('SigninModalCtrl', ['$scope', '$uibModalInstance', '$uibModal', 'UserStorage', 'Auth',
            function($scope, $uibModalInstance, $uibModal, UserStorage, Auth) {
        $scope.signinCompanyName = '';
        $scope.signupCompanyName = '';

        $scope.signinNext = function () {
            if ($scope.signInForm.$valid) {
                UserStorage.setCompany($scope.signinCompanyName);
                $uibModalInstance.close();
                var modalInstance = $uibModal.open({
                    animation: true,
                    templateUrl: 'views/modal/signin-email.modal.tpl.html',
                    controller: 'SigninEmailModalCtrl',
                    windowClass: 'vcenter-modal'
                });
                modalInstance.result.then(
                    function(data) {

                    },
                    function() {
                        console.info('Modal dismissed at: ' + new Date());
                    }
                );
            }
    	};

    	$scope.signupNext = function () {
            if ($scope.signUpForm.$valid) {
                UserStorage.setCompany($scope.signupCompanyName);
                $uibModalInstance.close();
    			var modalInstance = $uibModal.open({
        			animation: true,
        			templateUrl: 'views/modal/register-company.modal.tpl.html',
        			controller: 'RegisterCompanyModalCtrl',
        			windowClass: 'vcenter-modal'
        		});
        		modalInstance.result.then(
        			function(data) {

        			},
        			function() {
        				console.info('Modal dismissed at: ' + new Date());
        			}
    			);
            }
    	};

    	$scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
    }])
    .controller('SigninEmailModalCtrl', ['$scope', '$uibModalInstance', '$uibModal', 'UserStorage', 'Auth',
            function($scope, $uibModalInstance, $uibModal, UserStorage, Auth) {
        $scope.companyName = UserStorage.getCompany();
        $scope.email = '';

    	$scope.back = function () {
			$uibModalInstance.close();
			var modalInstance = $uibModal.open({
    			animation: true,
    			templateUrl: 'views/modal/signin.modal.tpl.html',
    			controller: 'SigninModalCtrl',
    			windowClass: 'vcenter-modal'
    		});
    		modalInstance.result.then(
    			function(data) {

    			},
    			function() {
    				console.info('Modal dismissed at: ' + new Date());
    			}
			);
    	};

    	$scope.continue = function () {
            if($scope.signInForm.$valid) {
                UserStorage.setEmail($scope.email);
                $uibModalInstance.close();
                var modalInstance = $uibModal.open({
                    animation: true,
                    templateUrl: 'views/modal/signin-pwd.modal.tpl.html',
                    controller: 'SigninPwdModalCtrl',
                    windowClass: 'vcenter-modal'
                });
                modalInstance.result.then(
                    function(data) {
                    },
                    function() {
                        console.info('Modal dismissed at: ' + new Date());
                    }
                );
            }
    	};

    	$scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
    }])
    .controller('SigninPwdModalCtrl', ['$scope', '$rootScope', '$uibModalInstance', '$uibModal', '$location', 'localStorageService', 'UserStorage', 'Auth', 'Profile', 'ngDialog',
            function($scope, $rootScope, $uibModalInstance, $uibModal, $location, localStorageService, UserStorage, Auth, Profile, ngDialog) {
        $scope.pwd = '';
        $scope.msg_error_pwd = '';
        $scope.msg_error_link = '';

    	$scope.back = function () {
			$uibModalInstance.close();
			var modalInstance = $uibModal.open({
    			animation: true,
    			templateUrl: 'views/modal/signin-email.modal.tpl.html',
    			controller: 'SigninEmailModalCtrl',
    			windowClass: 'vcenter-modal'
    		});
    		modalInstance.result.then(
    			function(data) {
    			},
    			function() {
    				console.info('Modal dismissed at: ' + new Date());
    			}
			);
    	};

    	$scope.signinWithDiff = function () {
            /*
            $uibModalInstance.close();
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'views/modal/signin-email.modal.tpl.html',
                controller: 'SigninEmailModalCtrl',
                windowClass: 'vcenter-modal'
            });
            modalInstance.result.then(
                function(data) {
                },
                function() {
                    console.info('Modal dismissed at: ' + new Date());
                }
            );*/
            Auth
                .sendMagic(UserStorage.getCompany(), UserStorage.getEmail())
                .then(
                    function (response) {
                        $uibModalInstance.close();
                        console.log(response);
                        ngDialog.open({
                            template: 'magicLinkSentPopup',
                        });
                    },
                    function (response) {
                        console.log(response);
                        var key = '';
                        for (key in response.data) break;
                        $scope.msg_error_link = response.data[key][0];
                    }
                );
    	};

        $scope.$watch('pwd', function() {
            $scope.msg_error_pwd = '';
        });

    	$scope.signin = function () {
            if ($scope.signInForm.$valid) {
                Auth
                    .signin(UserStorage.getCompany(), UserStorage.getEmail(), $scope.pwd)
                    .then(
                        function (response) {
                            $uibModalInstance.close();
                            Auth.setCredentials(UserStorage.getCompany(), UserStorage.getEmail(), response.token);

                            Profile
                                .getCompanyOwner()
                                .then(
                                    function (response) {
                                        $rootScope.globals.companyOwner = response;
                                        Profile
                                            .getProfile()
                                            .then(
                                                function (response) {
                                                    $rootScope.isOwner = $rootScope.globals.companyOwner.id == response.id;
                                                    $rootScope.isAuthorized = true;
                                                    $rootScope.globals.currentUser.profile = response;
                                                    localStorageService.set('globals', $rootScope.globals);
                                                    $location.path('/home');
                                                },
                                                function (response) {
                                                }
                                            );
                                    },
                                    function (response) {
                                        console.log(response);
                                    }
                                );
                        },
                        function (response) {
                            var key = '';
                            for (key in response.data) break;
                            $scope.msg_error_pwd = response.data[key][0];
                        }
                    );
            }
    	};

    	$scope.forgotPwd = function () {
    		$uibModalInstance.close();
			var modalInstance = $uibModal.open({
    			animation: true,
    			templateUrl: 'views/modal/reset-pwd.modal.tpl.html',
    			controller: 'ResetPwdModalCtrl',
    			windowClass: 'vcenter-modal'
    		});
    		modalInstance.result.then(
    			function(data) {
    			},
    			function() {
    				console.info('Modal dismissed at: ' + new Date());
    			}
			);
    	};

    	$scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
    }])
    .controller('ResetPwdModalCtrl', ['$scope', '$uibModalInstance', '$uibModal', 'UserStorage', 'Auth', 'ngDialog',
            function($scope, $uibModalInstance, $uibModal, UserStorage, Auth, ngDialog) {
        $scope.email = '';

        $scope.back = function () {
			$uibModalInstance.close();
			var modalInstance = $uibModal.open({
    			animation: true,
    			templateUrl: 'views/modal/signin-pwd.modal.tpl.html',
    			controller: 'SigninPwdModalCtrl',
    			windowClass: 'vcenter-modal'
    		});
    		modalInstance.result.then(
    			function(data) {
    			},
    			function() {
    				console.info('Modal dismissed at: ' + new Date());
    			}
			);
    	};

    	$scope.resetPwd = function () {
            if ($scope.resetPwdForm.$valid) {
                Auth.
                    sendResetPwdRequest(UserStorage.getCompany(), $scope.email)
                    .then(
                        function (response) {
                            $uibModalInstance.close();
                            ngDialog.open({
                                template: 'passwordResetPopup',
                            });
                        },
                        function (response) {
                            console.log(response);
                        }
                    );
            }
    	};

    	$scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
    }])
    .controller('ChangePwdModalCtrl', ['$scope', '$uibModalInstance', '$uibModal', 'UserStorage', 'Auth', 'key', 'ngDialog',
            function($scope, $uibModalInstance, $uibModal, UserStorage, Auth, key, ngDialog) {
        $scope.messages = [];

    	$scope.back = function () {
			$uibModalInstance.close();
			var modalInstance = $uibModal.open({
    			animation: true,
    			templateUrl: 'views/modal/reset-pwd.modal.tpl.html',
    			controller: 'ResetPwdModalCtrl',
    			windowClass: 'vcenter-modal'
    		});
    		modalInstance.result.then(
    			function(data) {
    			},
    			function() {
    				console.info('Modal dismissed at: ' + new Date());
    			}
			);
    	};

    	$scope.changePwd = function () {
            if (($scope.changePwdForm.$valid) && ($scope.pwd === $scope.confirmPwd)) {
                $scope.messages = [];
                Auth
                    .setNewPwd(key, $scope.pwd)
                    .then(
                        function (response) {
                            $uibModalInstance.close();
                            ngDialog.open({
                                template: 'passwordResetCompletePopup',
                            });
                        },
                        function (response) {
                            console.log(response);
                            $scope.messages = response.data;
                        }
                    );
            }
    	};

    	$scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
    }])
    .controller('RegisterCompanyModalCtrl', ['$scope', '$uibModalInstance', '$uibModal', 'UserStorage', 'Auth',
            function($scope, $uibModalInstance, $uibModal, UserStorage, Auth) {
        $scope.email = '';

    	$scope.back = function () {
			$uibModalInstance.close();
			var modalInstance = $uibModal.open({
    			animation: true,
    			templateUrl: 'views/modal/signin.modal.tpl.html',
    			controller: 'SigninModalCtrl',
    			windowClass: 'vcenter-modal'
    		});
    		modalInstance.result.then(
    			function(data) {
    			},
    			function() {
    				console.info('Modal dismissed at: ' + new Date());
    			}
			);
    	};

    	$scope.continue = function () {
            if ($scope.registerForm.$valid) {
                UserStorage.setEmail($scope.email);
                $uibModalInstance.close();
                var modalInstance = $uibModal.open({
                    animation: true,
                    templateUrl: 'views/modal/register-pwd.modal.tpl.html',
                    controller: 'RegisterPwdModalCtrl',
                    windowClass: 'vcenter-modal'
                });
                modalInstance.result.then(
                    function(data) {
                    },
                    function() {
                        console.info('Modal dismissed at: ' + new Date());
                    }
                );
            }
    	};

    	$scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
    }])
    .controller('RegisterMemberModalCtrl', ['$scope', '$rootScope', '$uibModalInstance', '$uibModal', '$location', 'UserStorage', 'Auth', 'Company', 'ngDialog',
            function($scope, $rootScope, $uibModalInstance, $uibModal, $location, UserStorage, Auth, Company, ngDialog) {
        $scope.firstName = '';
        $scope.lastName = '';
        $scope.pwd = '';
        $scope.confirmPwd = '';
        $scope.messages = [];

    	$scope.createCompany = function () {
            if (($scope.registerForm.$valid) && ($scope.pwd === $scope.confirmPwd)) {
                $scope.messages = [];
                $uibModalInstance.close({'firstName': $scope.firstName, 'lastName': $scope.lastName, 'pwd': $scope.pwd});
            }
    	};

    	$scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
    }])
    .controller('RegisterPwdModalCtrl', ['$scope', '$rootScope', '$uibModalInstance', '$uibModal', '$location', 'UserStorage', 'Auth', 'ngDialog',
            function($scope, $rootScope, $uibModalInstance, $uibModal, $location, UserStorage, Auth, ngDialog) {
        $scope.firstName = '';
        $scope.lastName = '';
        $scope.pwd = '';
        $scope.confirmPwd = '';
        $scope.messages = [];

    	$scope.back = function () {
			$uibModalInstance.close();
			var modalInstance = $uibModal.open({
    			animation: true,
    			templateUrl: 'views/modal/register-company.modal.tpl.html',
    			controller: 'RegisterCompanyModalCtrl',
    			windowClass: 'vcenter-modal'
    		});
    		modalInstance.result.then(
    			function(data) {
    			},
    			function() {
    				console.info('Modal dismissed at: ' + new Date());
    			}
			);
    	};

    	$scope.createCompany = function () {
            if (($scope.registerForm.$valid) && ($scope.pwd === $scope.confirmPwd)) {
                $scope.messages = [];
                Auth
                    .signup(UserStorage.getCompany(), UserStorage.getEmail(), $scope.pwd)
                    .then(
                        function (response) {
                            $uibModalInstance.close();
                            ngDialog.open({
                                template: 'companyCreatedPopup',
                            });
                        },
                        function (response) {
                            console.log(response);
                            $scope.messages = response.data;
                        }
                    );
            }

    	};

    	$scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
    }])
    .controller('EditFriendsModalCtrl', ['$scope', '$uibModalInstance', '$uibModal', 'UserStorage', 'Auth', 'Company', 'Common', 'friends', 'company',
            function($scope, $uibModalInstance, $uibModal, UserStorage, Auth, Company, Common, friends, company) {
        $scope.isSearchDropdownOpened = false;
        $scope.allFiltered = true;
        $scope.companyFriends = [];
        $scope.industryList = [];
        $scope.filters = [];

        $scope.getIndustryList = getIndustryList;
        $scope.filter = filter;
        $scope.selectItem = selectItem;
        $scope.selectAllItem = selectAllItem;
        $scope.applyFilter = applyFilter;
        //$scope.resetFilter = resetFilter;
        $scope.deleteFilter = deleteFilter;
        $scope.deleteFriend = deleteFriend;
        $scope.deleteAllFilters = deleteAllFilters;
        $scope.toggleSearchDropdown = toggleSearchDropdown;
        $scope.cancel = cancel;

        init();

        function init() {
            $scope.companyFriends = friends;
            $scope.companyProfile = company;
            console.log('friends:', friends);
            getIndustryList();
        }

        function getIndustryList() {
            Common
                .getIndustryList()
                .then(function(response) {
                    console.log('Industry List:', response);
                    $scope.industryList = response;

                    angular.forEach($scope.industryList, function(item) {
                        angular.extend(item, {
                            filtered: true
                        });
                    });
                });
        }

        function filter() {
            $scope.companyFriends = friends.filter(function(friend) {
                var filtered = true;
                if(!$scope.allFiltered) {
                    for(var i=0; i<$scope.filters.length; i++) {
                        filtered = filtered && (friend.industry == $scope.filters[i].name);
                    }
                }
                return filtered;
            });
        }

        function selectItem(item) {
            if($scope.allFiltered) {
                $scope.allFiltered = false;
                angular.forEach($scope.industryList, function(item) {
                    item.filtered = false;
                });
                item.filtered = true;
                return;
            }
            item.filtered = !item.filtered;
        }

        function selectAllItem() {
            if(!$scope.allFiltered) {
                $scope.allFiltered = true;
                angular.forEach($scope.industryList, function(item) {
                    item.filtered = true;
                });
            }
        }

        function deleteFriend(id) {
            Company
                .deleteFriend(id)
                .then(function(response) {
                    friends = friends.filter(function(friend) {
                        return friend.id != id;
                    });
                    filter();
                });

        }

        function applyFilter() {
            $scope.filters = [];
            angular.forEach($scope.industryList, function(item) {
                if (item.filtered) {
                    $scope.filters.push(item);
                }
            });
            $scope.isSearchDropdownOpened = false;
            filter();
        }

        /*
        function resetFilter() {
            $scope.allFiltered = false;
            angular.forEach($scope.industryList, function(item) {
                item.filtered = false;
            });
        }*/

        function deleteFilter(item) {
            for (var i=0; i<$scope.filters.length; i++) {
                if (item.id === $scope.filters[i].id) {
                    $scope.filters.splice(i, 1);
                }
            }

            angular.forEach($scope.industryList, function(itm) {
                if (item.id === itm.id) {
                    itm.filtered = false;
                }
            });
        }

        function deleteAllFilters() {
            $scope.filters = [];
            $scope.allFiltered = false;

            angular.forEach($scope.industryList, function(itm) {
                itm.filtered = false;
            });
        }

        function toggleSearchDropdown() {
            $scope.isSearchDropdownOpened = !$scope.isSearchDropdownOpened;
        }

        function cancel() {
            $uibModalInstance.close(friends);
        }
    }])
    .controller('CreateConvModalCtrl', ['$scope', '$http', 'config', '$uibModalInstance', '$uibModal', 'ngDialog', 'Chat',
            function($scope, $http, config, $uibModalInstance, $uibModal, ngDialog, Chat) {
        $scope.subject = "";
        $scope.uploaded = 0;
        $scope.description = "";
        $scope.form_url = "";
        $scope.form_link = "";
        $scope.files_id = [];
        $scope.files = [];
        $scope.show_spin = false;
        $scope.dzOptions = {
            url : '/',
            dictRemoveFile: '',
            previewTemplate: ''+
                '<li id="dz-preview-template" class="clearfix">' +
                '    <div class="pull-left file-name">' +
                '        <span data-dz-name></span>' +
                '        <span class="dz-size" data-dz-size></span>' +
                '    </div>' +
                '    <div class="pull-right actions">' +
                '        <a class="check"></a>' +
                '        <a class="close-icon thin-gray margin-left-30" data-dz-remove></a>' +
                '    </div>' +
                '</li>',
            previewsContainer:'.uploaded-file-list',
            autoProcessQueue: false,
            addRemoveLinks : true,
        };
        $scope.dzCallbacks = {
            'addedfile' : function(file){
                $scope.files.push(file);
                console.log('added', file);
            },
            'removedfile': function(file) {
                $scope.files.splice($scope.files.indexOf(file), 1);
                console.log('removed', file);
            }
        };
        $scope.dzMethods = {
        };


        $scope.uploadFiles = function () {
            $scope.show_spin = true;
            $scope.uploaded = 0;
            if ($scope.files.length) {
                angular.forEach($scope.files, function(file) {
                    Chat.uploadFile(file)
                        .success(function(data, status, headers, cfg) {
                            console.log('uploaded', data);
                            $scope.files_id.push(data.id);
                            $scope.$emit("fileUploaded");
                        }).error(function(data, status, headers, cfg) {
                            console.log('error: ', data);
                            $scope.$emit("fileUploaded");
                        });
                });
            } else {
                $scope.$emit("fileUploaded");
            }
        }

        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };

        $scope.$on("fileUploaded", function () {
            $scope.uploaded += 1;
            if ($scope.uploaded >= $scope.files.length) {
                $scope.createConversation();
                $scope.show_spin = false;
            }
        });

        $scope.createConversation = function () {
            var currentURI = window.location.href.toString().split(window.location.host)[1];
            var uriCompnents = currentURI.split('/');
            var companyID = uriCompnents[uriCompnents.length - 1];

            if (Number.isInteger(parseInt(companyID))) {
                var request = {
                    target_id  : companyID,
                    subject  : $scope.subject,
                    description  : $scope.description,
                    url : $scope.form_url,
                    link : $scope.form_link,
                    files: $scope.files_id
                };

                $http.post(config.baseURL + '/api/conversation/new_room/', request).then(function(response) {
                    ngDialog.open({
                        template: 'convCreatedPopup',
                    });
                });
            } else {
                alert("Please select company");
            }

            $uibModalInstance.dismiss('ok');

        };
    }]);

'use strict';

angular
    .module('app')
    .controller('HomeController', ['$scope', '$rootScope', '$uibModal', 'Company', 'Profile', 'config',
            function($scope, $rootScope, $uibModal, Company, Profile, config) {
    	$rootScope.hasMenuBG = false;

        $scope.companyProfile = {};
        $scope.companyFriends = [];
        $scope.companyMembers = [];
        $scope.affiCompanies = [];
        $scope.socials = [];
        $scope.owner = {};
        $scope.isActiveSocials = false;

        $scope.getCompanyProfile = getCompanyProfile;
        $scope.getCompanyMembers = getCompanyMembers;
        $scope.changeLogo = changeLogo;
        $scope.changeCover = changeCover;
        $scope.editFriends = editFriends;

        $scope.$on("fileSelected", function (event, args) {
            $scope.$apply(function () {
                var formData = new FormData();
                formData.append(args.key, args.file);
                Company
                    .updateUploadedFile(formData)
                    .then(function(response) {
                        $scope.companyProfile = response;
                        console.log('updated company profile:', response);
                    });
            });
        });

        init();

        function init() {
            getCompanyProfile();
            //getNotifications();
        }

        function getCompanyProfile() {
            Company
                .getCompanyProfile()
                .then(function(response) {
                    console.log('Company profile:', response);
                    $scope.companyProfile = response;

                    $scope.affiCompanies = $scope.companyProfile.affiliated_companies.filter(function(company) {
                        return company.id!=$scope.companyProfile.id;
                    });

                    for(var i=0; i<config.socials.length; i++) {
                        var key = config.socials[i];
                        $scope.isActiveSocials = $scope.isActiveSocials || ($scope.companyProfile[key]) && ($scope.companyProfile[key] != '');
                    }

                    $scope.companyFriends = response.friends;
                    getCompanyMembers();
                });
        }

        function getCompanyMembers() {
            Company
                .getCompanyMemberWithId($scope.companyProfile.id)
                .then(function(response) {
                    console.log('Company members:', response);
                    $scope.companyMembers = response.results;
                });
        }

        function changeLogo() {
            $('#logoFile').click();
        }

        function changeCover() {
            $('#coverFile').click();
        }

        function editFriends() {
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'views/modal/edit-friends.modal.tpl.html',
                controller: 'EditFriendsModalCtrl',
                windowClass: 'vcenter-modal transparent-modal',
                backdrop: 'static',
                backdropClass: 'transparent-backdrop',
                resolve: {
                    company: function() {
                        return $scope.companyProfile;
                    },
                    friends: function () {
                      return $scope.companyFriends;
                    }
                }
            });
            modalInstance.result.then(
                function(data) {
                    $scope.companyFriends = data;
                },
                function() {
                    console.info('Modal dismissed at: ' + new Date());
                }
            );
        }


    }]);

'use strict';

angular
    .module('app')
    .controller('CompanyController', ['$scope', '$rootScope', '$timeout', '$location', 'Company', 'Common',
            function($scope, $rootScope, $timeout, $location, Company, Common) {
    	$rootScope.hasMenuBG = true;

    	$scope.isSearchDropdownOpened = false;
        $scope.isOnMap = false;
        $scope.mapCompanies = [];
        $scope.map = null;
        $scope.allFiltered = true;
        $scope.companyList = [];
        $scope.filters = [];
        $scope.companyProfile = {};
        $scope.searchKey = '';
        $scope.markerCluster = {};
        $scope.companyFriends = [];
        $scope.companyFriendsReq = [];

        $scope.getIndustryList = getIndustryList;
        $scope.getCompanyList = getCompanyList;
        $scope.getCompanyProfile = getCompanyProfile;
        $scope.selectItem = selectItem;
        $scope.selectAllItem = selectAllItem;
        $scope.applyFilter = applyFilter;
        $scope.resetAllFilters = resetAllFilters;
        $scope.deleteFilter = deleteFilter;
        $scope.deleteAllFilters = deleteAllFilters;
        $scope.toggleSearchDropdown = toggleSearchDropdown;
        $scope.toggleOnMap = toggleOnMap;
        $scope.filter = filter;
        $scope.inviteFriend = inviteFriend;
        $scope.visitCompany = visitCompany;
        $scope.isFriend = isFriend;
        $scope.isFriendReq = isFriendReq;

        init();

        function init() {
            getCompanyProfile();
            getIndustryList();
            if ($scope.isOnMap) {
                initMap();
            }
        }

        $scope.getLinkPreview = function(message) {
            if (message && message.length > 20) {
                return message.slice(0, 20) + '...';
            } else {
                return message;
            }
        }

        function initMap() {
            var mapOptions = {
                zoom: 4,
                center: new google.maps.LatLng(40.0000, -98.0000),
                mapTypeId: google.maps.MapTypeId.TERRAIN
            };
            $scope.map = new google.maps.Map(document.getElementById('map'), mapOptions);
        }

        function addMapCluster() {

            $scope.markers = [];

            var createMarker = function(company) {
                if (null != company.lat && null != company.lon) {
                    var latlng = new google.maps.LatLng(company.lat, company.lon);
                    var marker = new google.maps.Marker({
                        position: latlng,
                        company_id: company.id
                    });

                    $scope.markers.push(marker);
                }
            }

            for (var i = 0; i < $scope.companyList.length; i++) {
                createMarker($scope.companyList[i]);
            }

            var options = {
                styles:[
                    {"url":"/assets/images/map/c1.png","height":53,"textColor": "white", "width":53},
                    {"url":"/assets/images/map/c2.png","height":56,"textColor": "white", "width":56},
                    {"url":"/assets/images/map/c3.png","height":66,"textColor": "white", "width":66},
                    {"url":"/assets/images/map/c4.png","height":78,"textColor": "white", "width":78},
                    {"url":"/assets/images/map/c5.png","height":90,"textColor": "white", "width":90}
                ],
                zoomOnClick: false,
                minimumClusterSize: 1,
                imagePath: '/assets/images/map/c'
            };

            $scope.markerCluster = new MarkerClusterer($scope.map, $scope.markers, options);

            google.maps.event.addListener($scope.markerCluster, 'clusterclick', function(data) {
                $scope.mapCompanies = data.markers_.map(function(el) {
                    return el.company_id;
                });
                $timeout(function () {
                    filter();
                }, 100);

            });
        }

        function getCompanyProfile() {
            Company
                .getCompanyProfile()
                .then(function(response) {
                    console.log('Company profile:', response);
                    $scope.companyProfile = response;
                    $scope.companyFriends = response.friends.map(function(c) {
                        return c.id;
                    });
                    $scope.companyFriendsReq = response.friends_source.map(function(c) {
                        return c.target_company;
                    });
                    getCompanyList();
                });
        }

        function isFriend(id) {
            var res = -1 !=  $scope.companyFriends.indexOf(id);
            if (!res) {
                res = -1 !=  $scope.companyFriendsReq.indexOf(id);
            }
            return res;
        }

        function isFriendReq(id) {
            var res = -1 !=  $scope.companyFriendsReq.indexOf(id);
            return res;
        }

        function getIndustryList() {
            Common
                .getIndustryList()
                .then(function(response) {
                    console.log('Industry List:', response);
                    $scope.industryList = response;

                    angular.forEach($scope.industryList, function(item) {
                        angular.extend(item, {
                            filtered: true
                        });
                    });
                });
        }

        function getCompanyList() {
            Company
                .getCompanyList()
                .then(function(response) {
                    console.log('company list:', response.results);
                    $scope.companyList = response.results.filter(function(c) {
                        return (c.id != $scope.companyProfile.id);
                    });
                    if ($scope.map) {
                        addMapCluster();
                    }
                    filter();
                });
        }

        function selectItem(item) {
            if($scope.allFiltered) {
                $scope.allFiltered = false;
                angular.forEach($scope.industryList, function(item) {
                    item.filtered = false;
                });
                item.filtered = true;
                return;
            }
            item.filtered = !item.filtered;
        }

        function selectAllItem() {
            if(!$scope.allFiltered) {
                $scope.allFiltered = true;
                angular.forEach($scope.industryList, function(item) {
                    item.filtered = true;
                });
            }
            applyFilter();
        }

        function applyFilter() {
            $scope.filters = [];
            angular.forEach($scope.industryList, function(item) {
                if (item.filtered) {
                    $scope.filters.push(item);
                }
            });
            $scope.isSearchDropdownOpened = false;
            filter();
        }


        function resetAllFilters() {
            $scope.allFiltered = true
            $scope.mapCompanies = [];
            $scope.searchKey = '';
            angular.forEach($scope.industryList, function(item) {
                item.filtered = false;
            });
            applyFilter();
        }

        function deleteFilter(name) {
            angular.forEach($scope.industryList, function(itm) {
                if (name == itm.name) {
                    $scope.allFiltered = false;
                    itm.filtered = false;
                }
            });
            applyFilter();
        }

        function deleteAllFilters() {
            $scope.filters = [];
            $scope.allFiltered = false;

            angular.forEach($scope.industryList, function(itm) {
                itm.filtered = false;
            });
        }

        function toggleSearchDropdown(toggle) {
            if(toggle) {
                $scope.isSearchDropdownOpened = toggle;
                return;
            }
            $scope.isSearchDropdownOpened = !$scope.isSearchDropdownOpened;
        }

        function toggleOnMap() {
            $scope.isOnMap = !$scope.isOnMap;
            if (null == $scope.map) {
                initMap();
                addMapCluster();
            }
            $scope.mapCompanies = [];
        }

        function filter() {
            $scope.filteredCompanyList = $scope.companyList.filter(function (company) {
                var filtered = true;
                filtered = filtered && (company.name.toLowerCase().indexOf($scope.searchKey.toLowerCase())!=-1) || (company.website && company.website.toLowerCase()==$scope.searchKey.toLowerCase()) || (company.owner.email && company.owner.email.toLowerCase()==$scope.searchKey.toLowerCase());
                if(!$scope.allFiltered && $scope.filters.length>0) {
                    var matched = false;
                    for(var i=0; i<$scope.filters.length; i++) {
                        matched = matched || (company.industry == $scope.filters[i].name);
                    }
                    filtered = filtered && matched;
                }
                if ($scope.mapCompanies.length) {
                    filtered = filtered && (-1!=$scope.mapCompanies.indexOf(company.id));
                }
                return filtered;
            });
        }

        function inviteFriend(id, $event) {
            $event.stopPropagation();
            $event.preventDefault();
            Company
                .inviteFriend(id)
                .then(function(response) {
                    $scope.companyFriendsReq.push(id);
                    console.log('Invite friend:', response);
                });
        }

        function visitCompany(id) {
            $location.path('/company-profile/' + id);
        }
    }])
    .controller('CompanyProfileController', ['$scope', '$rootScope', '$uibModal', '$route', 'Company',
            function($scope, $rootScope, $uibModal, $route, Company) {
        $rootScope.hasMenuBG = false;

        $scope.companyProfile = {};
        $scope.companyList = [];
        $scope.companyFriends = [];
        $scope.companyMembers = [];
        $scope.affiCompanies = [];
        $scope.companyFriendsReq = [];

        $scope.getCompanyList = getCompanyList;
        $scope.getCompanyDetail = getCompanyDetail;
        $scope.sendFriendRequest = sendFriendRequest;
        $scope.createConv = createConv;
        $scope.isFriend = isFriend;
        $scope.isFriendReq = isFriendReq;

        init();

        function init() {
            getCurCompanyProfile();
            getCompanyList();
            getCompanyDetail();
            getCompanyMembers();
        }

        function getCompanyList() {
            Company
            .getCompanyList()
            .then(function(response) {
                console.log("Company List:", response.results);
                $scope.companyList = response.results;
            });
        }

        function getCurCompanyProfile() {
            Company
                .getCompanyProfile()
                .then(function(response) {
                    console.log('Company profile:', response);
                    $scope.companyFriendsReq = response.friends_source.map(function(c) {
                        return c.target_company;
                    });
                });
        }

        function getCompanyDetail() {
            Company
                .getCompanyDetail($route.current.params.id)
                .then(function(response) {
                    console.log('Company profile:', response);
                    $scope.companyProfile = response;
                    $scope.companyFriends = response.friends;
                    $scope.affiCompanies = $scope.companyProfile.affiliated_companies.filter(function(company) {
                        return company.id!=$scope.companyProfile.id;
                    });
                });
        }

        function getCompanyMembers() {
            Company
                .getCompanyMemberWithId($route.current.params.id)
                .then(function(response) {
                    console.log('Company members:', response);
                    $scope.companyMembers = response.results;
                });
        }

        function sendFriendRequest() {
            Company
                .inviteFriend($route.current.params.id)
                .then(function(response) {
                    console.log('invite friend:', response);
                    $scope.companyFriendsReq.push(parseInt($route.current.params.id));
                });
        }

        function isFriend() {
            var res = isFriendReq();
            angular.forEach($scope.companyFriends, function (c) {
                res = res || c.id == $rootScope.globals.companyOwner.company;
            });
            return res;
        }

        function isFriendReq() {
            var res = -1 !=  $scope.companyFriendsReq.indexOf($scope.companyProfile.id);
            return res;
        }

        function createConv() {
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'views/modal/create-conv.modal.tpl.html',
                controller: 'CreateConvModalCtrl',
                windowClass: 'vcenter-modal auto-height'
            });
            modalInstance.result.then(
                function(data) {

                },
                function() {
                    console.info('Modal dismissed at: ' + new Date());
                }
            );
        };
    }]);

'use strict';

angular
    .module('app')
    .controller('SettingsController', ['$scope', '$rootScope', '$uibModal', '$routeParams', 'localStorageService', 'Profile', 'Company', 'Common', 'ngDialog',
            function($scope, $rootScope, $uibModal, $routeParams, localStorageService, Profile, Company, Common, ngDialog) {
    	$rootScope.hasMenuBG = true;

    	$scope.selMainTab = $routeParams.main ? $routeParams.main : 'personal';
    	$scope.selSubTab = $routeParams.sub ? $routeParams.sub : 'personal-profile';
    	$scope.isDepartDropdownOpened = false;
    	$scope.isComsizeDropdownOpened = false;
    	$scope.isCountryDropdownOpened = false;
    	$scope.isStateDropdownOpened = false;
    	$scope.isTyperopdownOpened = false;
        $scope.isIndustryDropdownOpened = false;
        $scope.oldPwd = '';
        $scope.newPwd = '';
        $scope.filteredDepart = 'All';
        $scope.newMember = {email: '', department: ''};
        $scope.companyList = [];
        $scope.industryList = [];
        $scope.departList = [];
        $scope.members = [];
        $scope.filterMembers = [];
        $scope.personalProfile = {};
        $scope.companyProfile = {};
        $scope.socialURL = {};
        $scope.socialURLAction = {};
        $scope.companyTypeList = Common.getCompanyTypeList();
        $scope.sizeList = Common.getCompanySizeList();
        $scope.countryList = Common.getCountryList();
        $scope.stateList = Common.getStateList();
        $scope.msg = '';

        $scope.filterMember = filterMember;

        init();

        $scope.$on("fileSelected", function (event, args) {
            $scope.$apply(function () {
                var formData = new FormData();
                formData.append(args.key, args.file);
                Profile
                    .updateUploadedFile(formData)
                    .then(function(response) {
                        $scope.personalProfile = response;
                        $rootScope.globals.currentUser.profile = response;
                        localStorageService.set('globals', $rootScope.globals);
                        console.log('updated user profile:', response);
                    });
            });
        });

        function init() {
            if ($scope.selMainTab === 'personal') {
                if ($scope.selSubTab === 'personal-profile') {
                    getProfile();
                    getDepartList();
                }
            } else if ($scope.selMainTab == 'company') {
                getCompanyProfile();
                getIndustryList();
                if ($scope.selSubTab == 'affiliate-companies') {
                    getCompanyList();
                }
            } else if ($scope.selMainTab == 'team') {
                if ($scope.selSubTab == 'account-mng') {
                    getCompanyProfile();
                    getDepartList();
                }
            }
        }

        function getProfile() {
            Profile
                .getProfile()
                .then(function(response) {
                    $scope.personalProfile = response;
                    console.log('Personal profile:', $scope.personalProfile);
                });
        }

        function getCompanyProfile() {
            Company
                .getCompanyProfile()
                .then(function(response) {
                    $scope.companyProfile = response;
                    angular.copy(response, $scope.socialURL);
                    console.log('Company profile:', $scope.companyProfile);
                    getCompanyMembers();
                });
        }

        function getCompanyList() {
            Company
                .getCompanyList()
                .then(function(response) {
                    console.log('Company list:', response);
                    $scope.companyList = response.results;
                });
        }

        function getIndustryList() {
            Common
                .getIndustryList()
                .then(function(response) {
                    console.log('industry list:', response);
                    $scope.industryList = response;
                });
        }

        function getDepartList() {
            Common
                .getDepartList()
                .then(function(response) {
                    console.log('depart list:', response);
                    $scope.departList = response;
                });
        }

        function getCompanyMembers() {
            Company
                .getCompanyMemberWithId($scope.companyProfile.id)
                .then(function(response) {
                    console.log('Company members:', response);
                    $scope.members = response.results;
                    filterMember($scope.filteredDepart);
                });
        }

    	$scope.toggleDepartDropdown = function () {
    		$scope.isDepartDropdownOpened = !$scope.isDepartDropdownOpened;
    	};

        $scope.changeAvatar = function () {
            $('#logoFile').click();
        }
        $scope.selectDepart = function (depart) {
            $scope.isDepartDropdownOpened = false;
            $scope.personalProfile.department = depart.name;
        };

        $scope.selectMemberDepart = function (depart) {
            $scope.isDepartDropdownOpened = false;
            $scope.newMember.department = depart.name;
        };


    	$scope.toggleSizeDropdown = function () {
    		$scope.isComsizeDropdownOpened = !$scope.isComsizeDropdownOpened;
    	};

        $scope.selectSize = function (size) {
            $scope.isComsizeDropdownOpened = false;
            $scope.companyProfile.company_size = size.name;
        };

    	$scope.toggleCountryDropdown = function (toggle) {
    		$scope.isCountryDropdownOpened = !$scope.isCountryDropdownOpened;
    	};

        $scope.selectCountry = function (country) {
            $scope.isCountryDropdownOpened = false;
            $scope.companyProfile.country = country.name;
        };

    	$scope.toggleStateDropdown = function (toggle) {
    		$scope.isStateDropdownOpened = !$scope.isStateDropdownOpened;
    	};

    	$scope.toggleTypeDropdown = function (toggle) {
    		$scope.isTypeDropdownOpened = !$scope.isTypeDropdownOpened;
    	};

        $scope.selectState = function (state) {
            $scope.isStateDropdownOpened = false;
            $scope.companyProfile.state = state.name;
        };

        $scope.selectType = function (type) {
            $scope.isTypeDropdownOpened = false;
            $scope.companyProfile.company_type = type.name;
        };

        $scope.toggleIndustryDropdown = function (toggle) {
            $scope.isIndustryDropdownOpened = !$scope.isIndustryDropdownOpened;
        };

        $scope.selectIndustry = function (industry) {
            $scope.isIndustryDropdownOpened = false;
            $scope.companyProfile.industry = industry.name;
        };

        $scope.focusSocialURL = function (type) {
            if ($scope.companyProfile[type] == null || $scope.companyProfile[type] == '') {
                $scope.socialURLAction[type] = 'create';
            } else {
                $scope.socialURLAction[type] = 'update';
            }
        };

        $scope.blurSocialURL = function (type) {
            $scope.socialURLAction[type] = '';
            $scope.socialURL[type] = $scope.companyProfile[type];
        };

        $scope.cancelSocialURL = function (type) {
            $scope.socialURLAction[type] = '';
            $scope.socialURL[type] = $scope.companyProfile[type];
        };

        $scope.saveSocialURL = function (type) {
            if ($scope.socialForm.$valid && $scope.socialURL[type] != '' && $scope.socialURL[type] != null) {
                $scope.socialURLAction[type] = '';
                $scope.companyProfile[type] = $scope.socialURL[type];
                Company
                    .updateCompanyProfile($scope.companyProfile)
                    .then(function(response) {
                        console.log(response);
                    });
            }
        };

        $scope.isAffiliated = function (company) {
            for(var i=0; i<$scope.companyProfile.affiliated_companies.length; i++) {
                var affi_company = $scope.companyProfile.affiliated_companies[i];
                if(company.id == affi_company.id) {
                    return true;
                }
            }
            return false;
        };

        function filterMember(depart) {
            $scope.filteredDepart = depart;
            $scope.filteredMembers = $scope.members.filter(function(member) {
                var filtered = true;
                if(depart != 'All') {
                    filtered = filtered && (member.department == depart);
                }
                return filtered;
            });
        }

        $scope.sendAffilateRequest = function (id) {
            Company.sendAffilateRequest(id).then(function(response) {
                $scope.ctx = response;
                ngDialog.open({
                    template: 'notificationPopup',
                    scope: $scope
                });
                console.log(response);
            });
        }

        $scope.deleteMember = function (id) {
            Company.deleteMember(id).then(function(response) {
                if ('Success' == response.status){
                    $scope.members = $scope.members.filter(function(member) {
                        return member.id != id;
                    });
                    filterMember($scope.filteredDepart);
                }
                $scope.ctx = response;
                ngDialog.open({
                    template: 'notificationPopup',
                    scope: $scope
                });
                console.log(response);
            });
        }

        $scope.deleteAffilateCompany = function (id) {
            Company.deleteAffilateCompany(id).then(function(response) {
                if ('Success' == response.status){
                    $scope.companyProfile.affiliated_companies = $scope.companyProfile.affiliated_companies.filter(function(company) {
                        return company.id != id;
                    });
                }
                $scope.ctx = response;
                ngDialog.open({
                    template: 'notificationPopup',
                    scope: $scope
                });
                console.log(response);
            });
        }

        $scope.updateSettings = function (type) {
            if (type === 'personal') {
                if ($scope.selSubTab === 'personal-profile') {
                    if ($scope.profileForm.$valid) {
                        Profile
                            .saveProfile($scope.personalProfile)
                            .then(function(response) {
                                $rootScope.globals.currentUser.profile = response;
                                localStorageService.set('globals', $rootScope.globals);
                            }, function(response) {
                                console.log(response);
                            });
                    }
                } else if ($scope.selSubTab === 'personal-pwd') {
                    if ($scope.changePwdForm.$valid) {
                        Profile
                            .changePassword($scope.oldPwd, $scope.newPwd)
                            .then(function(response) {
                                $scope.msg = 'Password was updated';
                                console.log(response);
                            }, function(response) {
                                $scope.msg = 'You entered incorrect password';
                                console.log('b',response);
                            });
                        $scope.oldPwd = '';
                        $scope.newPwd = '';
                    }
                }
            } else if (type == 'company') {
                if ('United States' != $scope.companyProfile.country) {
                    $scope.companyProfile.state = '';
                }
                var full_address = $scope.companyProfile.country + ', ' + $scope.companyProfile.state + ', ' + $scope.companyProfile.city;

                $.getJSON('https://maps.googleapis.com/maps/api/geocode/json?address='+full_address+'&sensor=false', null, function (data) {
                    if ('OK' == data.status) {
                        var p = data.results[0].geometry.location;
                        $scope.companyProfile.lat = p.lat;
                        $scope.companyProfile.lon = p.lng;
                    }
                    Company
                        .updateCompanyProfile($scope.companyProfile)
                        .then(function(response) {
                            console.log(response);
                        });
                    if ($scope.selSubTab == 'basic-info') {

                    } else if ($scope.selSubTab == 'socials') {

                    } else if ($scope.selSubTab == 'affiliate-companies') {

                    }
                });
            } else if (type == 'team') {
                if ($scope.selSubTab == 'account-mng') {
                    if ($scope.teamForm.$valid) {
                        Company
                            .inviteMember($scope.newMember)
                            .then(function(response) {
                                $scope.ctx = response;
                                ngDialog.open({
                                    template: 'notificationPopup',
                                    scope: $scope
                                });
                                $scope.newMember = {email: '', department: ''};
                                console.log(response);
                            });
                    }
                }
            }
        };
    }]);

'use strict';

angular
    .module('app')
    .controller('ConvController', ['$scope', '$http', 'config', '$rootScope', '$filter', '$uibModal', 'UserStorage', '$location', 'Chat',
            function($scope, $http, config, $rootScope, $filter, $uibModal, UserStorage, $location, Chat) {
        $rootScope.hasMenuBG = true;
        $rootScope.hideFooter = true;
        $scope.rooms = [];
        $scope.roomsFiltered = [];
        $scope.currentRoom = [];
        $scope.selection = 'list';
        $scope.filter_type = 'see_all';
        $scope.search = {text: ''};
        $scope.opt = { showSelect : false };
        $scope.msg_text = '';
        $scope.files = [];
        $scope.files_id = [];
        $scope.uploaded = 0;
        $scope.show_spin = false;

        $scope.$on("fileSelected", function (event, args) {
            $scope.$apply(function () {
                if ($scope.is_image(args.file.name)) {
                    args.file.preview = URL.createObjectURL(args.file)
                }
                $scope.files.push(args.file);
            });
        });

        $scope.uploadFile = function() {
            $scope.opt.showSelect = false;
            $('#uploadfile').click();
        }

        $scope.uploadImage = function() {
            $scope.opt.showSelect = false;
            $('#uploadimage').click();
        }

        $scope.is_image = function(filename) {
            var ext = filename.split('.').slice(-1)[0].toLowerCase();
            return -1 != $.inArray(ext, ["bmp", "png", "jpg", "jpeg", "gif", "svg"])
        }

        var currentUserInfo = JSON.parse(localStorage['ls.globals']);
        var user_id = currentUserInfo.currentUser.profile.user_id;

        var ws_scheme = window.location.protocol == "https:" ? "wss" : "ws";
        $scope.chatsock = null;

        Chat.getChatRooms().then(function(response) {
            $scope.rooms = response.results;
            $scope.filter();
        });

        $scope.gotoManage = function() {
            $location.path('/manage-conversation/'+$scope.currentRoom.id);
        }

        $scope.enterRoom = function(room) {
            // body.../api/company/friend/list/
            var roomID = room.id;
            if (false == room.confirmed) {
                $scope.selection = 'list';
                return;
            }
            $scope.selection = 'detail';
            $scope.currentRoom = $filter('filter')($scope.rooms, { id: roomID })[0];
            $scope.chatsock = new ReconnectingWebSocket(ws_scheme + '://' + config.chatURL + "/chat/" + roomID);
            $scope.chatsock.onmessage = function(message) {
                var data = JSON.parse(message.data);
                var chat = $("#chat");
                var hdr = {};
                var msg = {}
                var files = $('<span></span>');
                var ele = $('<li class="wrapper"></li>');
                if (data.handle.avatar) {
                    ele.append('<img class="user-avatar" src="'+data.handle.avatar+'" />');
                } else {
                    ele.append('<img class="user-avatar" src="assets/images/no-avatar.png" />');
                }


                hdr = $("<div class=chat-header></div>");
                hdr.append($('<span class="user-name"></span>').text(data.handle.first_name + " " + data.handle.last_name));
                hdr.append($('<span class="chat-time"></span>').text($scope.getFormattedTime(data.timestamp)));

                if (data.type == 'file') {
                    return;
                    var fileUrl = data.message;
                    var fileName = fileUrl.substring(fileUrl.lastIndexOf('/') + 1);
                    msg = $("<p class=chat-data></p>").html("<a href=" + fileUrl + " target=_blank>" + fileName + "</a>")
                } else {
                    msg = $("<p class=chat-data></p>").html(data.message)
                }

                angular.forEach(data.files, function(file){
                    if ($scope.is_image(file.filename)) {
                        files.append('<a href="'+file.file+'" target="_blank"><img class="chat-preview" src="'+file.file+'" title="'+file.filename+'"></a>');
                    } else {
                        files.append('<a href="'+file.file+'" target="_blank">'+file.filename+'</a>');
                    }
                });

                ele.append($('<div class="conv-wrapper"></div>').append($('<div class="chat-content"></div>').append(hdr).append(msg).append(files)));

                chat.append(ele)
            };
        }

        $scope.getFileName = function(fileUrl){
            var fileName = fileUrl.substring(fileUrl.lastIndexOf('/') + 1);
            return fileName;
        }

        $scope.getFormattedTime = function(datetime_input){
            var datetime = moment(datetime_input).fromNow();
            return datetime;
        }

        $scope.getMessagePreview = function(message) {
            if (message.length > 100) {
                return message.slice(0, 100) + '...';
            } else {
                return message;
            }
        }

        $scope.filterChat = function(type) {
            $scope.filter_type = type;
            $scope.filter();
        }

        $scope.filter = function() {
            console.log('filtering', $scope.search, $scope.filter_type);
            $scope.roomsFiltered = $scope.rooms.filter(function (room) {
                var filtered = true;
                var filter_text = $scope.search.text.toLocaleLowerCase();

                if ('confirmed' == $scope.filter_type) {
                    filtered = filtered && true == room.confirmed;
                } else if ('unconfirmed' == $scope.filter_type) {
                    filtered = filtered && false == room.confirmed;
                }

                filtered = filtered && ((room.name.toLowerCase().indexOf(filter_text) !==-1) || (room.last_message.message.toLowerCase().indexOf(filter_text)!=-1) || (room.target.name.toLowerCase().indexOf(filter_text)!=-1) || (room.target.website && room.target.website.toLowerCase()==filter_text) || (room.target.owner.email && room.target.owner.email.toLowerCase()==filter_text));

                return filtered;
            });
        }

        $scope.deleteMember = function(room_id, member_id) {
            Chat.delChatMember(room_id, member_id).then(function(resp) {
                $scope.currentRoom.members = $scope.currentRoom.members.filter(function(m) {
                    return member_id != m.id;
                });
                $scope.ctx = resp;
                ngDialog.open({
                    template: 'notificationPopup',
                    scope: $scope
                });
            });
        }

        $scope.getMemberName = function(member) {
            var result = "";

            if (member.first_name == "" && member.last_name == "") {
                result = member.email;
            } else {
                result = member.first_name + " " + member.last_name;
            }

            return result;

        }

        $scope.send = function() {
            $scope.uploaded = 0;
            $scope.show_spin = true;
            if ($scope.files.length) {
                angular.forEach($scope.files, function(file) {
                    Chat.uploadFile(file)
                        .success(function(data, status, headers, cfg) {
                            console.log('uploaded', data);
                            $scope.files_id.push(data.id);
                            $scope.$emit("fileUploaded");
                        }).error(function(data, status, headers, cfg) {
                            console.log('error: ', data);
                            $scope.$emit("fileUploaded");
                        });
                });
            } else {
                $scope.$emit("fileUploaded");
            }
        }

        $scope.$on("fileUploaded", function () {
            $scope.uploaded += 1;
            if ($scope.uploaded >= $scope.files.length) {
                $scope.real_send();
            }
        });

        $scope.real_send = function() {
            var message = {
                handle: user_id,
                type: "text",
                files: $scope.files_id,
                message: $('#message').val(),
            }
            $scope.chatsock.send(JSON.stringify(message));
            $("#message").val('').focus();
            $scope.files = [];
            $scope.msg_text = '';
            $scope.show_spin = false;
            return false;
        }

        $scope.checkFile = function() {
            var fileName = $("#file").val();
            if (fileName) {
                // Added code
                var fd = new FormData();
                var files = $('#file').prop("files");
                if (files.length) {
                    fd.append("file", files[0], files[0].name);
                }
                $http({
                    method: 'POST',
                    dataType: 'jsonp',
                    url: config.baseURL + '/api/v1/files/',
                    data: fd,
                    processData: false,
                    crossDomain: true,
                    headers: {
                        'Content-Type': undefined
                    },
                    xhrFields: {
                        withCredentials: true
                    },
                    transformRequest: angular.identity
                }).success(function(data, status, headers, cfg) {
                        var message = {
                            handle: user_id,
                            type: "file",
                            message: data.file,
                        }
                        $scope.chatsock.send(JSON.stringify(message));
                        $("#message").val('').focus();
                        $("#file").val('');
                        return false;
                }).error(function(data, status, headers, cfg) {
                    console.log('error: ', data);
                });
                // End of code

            } else {
                console.log("not selected");
            }
        }

    }])
    .controller('ConvMngController',
            ['$scope', '$route', '$http', 'config', '$rootScope', '$uibModal', '$location', 'Company', 'Chat', 'Common', 'ngDialog',
            function($scope, $route, $http, config, $rootScope, $uibModal, $location, Company, Chat, Common, ngDialog) {
        $rootScope.hasMenuBG = true;
        $rootScope.hideFooter = true;

        $scope.members = [];
        $scope.members_filtered = [];
        $scope.currentRoom = {};
        $scope.departList = [];
        $scope.filter_source = false;
        $scope.filter_target = false;
        $scope.filterSource = filterSource;
        $scope.filterTarget = filterTarget;
        $scope.allFiltered = true;
        $scope.chatSourceMembers = {};
        $scope.chatTargetMembers = {};
        $scope.dragging = false;

        var currentRoomID = $route.current.params.id;

        function filter() {
            $scope.members_filtered = $scope.members.filter(function(member) {
                var filtered = true;
                filtered = filtered && ((!$scope.filter_source || member.company != $scope.currentRoom.source.id));
                filtered = filtered && ((!$scope.filter_target || member.company != $scope.currentRoom.target.id));
                if(!$scope.allFiltered) {
                    var matched = false;
                    $scope.departList.forEach(function(depart) {
                        if (depart.filtered) {
                            matched = matched || (member.department == depart.name);
                        }
                    });
                    filtered = filtered && matched;
                }
                return filtered;
            });
        }

        $scope.selectDepartment = function(name) {
            if ('all' == name) {
                $scope.allFiltered = !$scope.allFiltered;
                $scope.departList.forEach(function(el) {
                    el.filtered = $scope.allFiltered;
                });
            } else {
                $scope.allFiltered = false;
                $scope.departList.forEach(function(el) {
                    if (el.name == name) {
                        el.filtered = !el.filtered;
                    }
                });
            }
            filter();
        }

        function filterTarget() {
            $scope.filter_target = !$scope.filter_target;
            filter();
        }

        function filterSource() {
            $scope.filter_source = !$scope.filter_source;
            filter();
        }

        Common
            .getDepartList()
            .then(function(response) {
                $scope.departList = response.map(function(el) {
                    return {'filtered':true, 'name': el.name }
                });
            });

        function loadCompanyMemebers(company_id, company_name) {
            Company
                .getCompanyMemberWithId(company_id)
                .then(function(response) {
                    response.results.forEach(function(m) {
                        m.company_name = company_name;
                    });

                    $scope.members = $scope.members.concat(response.results);
                    $scope.members_filtered = $scope.members_filtered.concat(response.results);

                    for (var i = 0; i < $scope.members.length; i++) {
                        $scope.members[i].selected = false;
                        for (var j = 0; j < $scope.currentRoom.members.length; j++) {
                            if ($scope.currentRoom.members[j].id == $scope.members[i].user_id) {
                                $scope.members[i].selected = true;
                            }
                        }
                    }
                });
        }

        function addCompanyMemeber(m) {
            var company = '';
            if (m.company == $scope.currentRoom.source.id) {
                company = $scope.chatSourceMembers;
            } else {
                company = $scope.chatTargetMembers;
            }

            if (!(m.department in company)) {
                var name = m.department;
                if (!m.department) {
                    name = 'Unknown';
                }

                company[m.department] = {'name': name, list: {}};
            }

            if (!(m.id in company[m.department]))
                company[m.department].list[m.id] = m;
        }

        function updateCompanyMembers(members) {
            $scope.chatSourceMembers = {};
            $scope.chatTargetMembers = {};
            members.forEach(function(m) {
                addCompanyMemeber(m);
            });
        }

        function syncCompanyMembers() {
            var selected_members = $scope.members.filter(function(m) {
                return m.selected;
            });
            updateCompanyMembers(selected_members);
        }

        Chat.getChatRoom(currentRoomID).then(function(response) {
            $scope.currentRoom = response;
            updateCompanyMembers(response.members);

            loadCompanyMemebers(response.source.id, response.source.name);
            loadCompanyMemebers(response.target.id, response.target.name);
        });

        $scope.dragStart = function(data, evt) {
            console.log('drag start', data, evt);
            $scope.dragging = true;
        }

        $scope.dragStop = function(data, evt) {
            console.log('drag stop', data, evt);
            $scope.dragging = false;
        }

        $scope.onDropComplete = function(data, evt) {
            console.log('drop complete', data, evt);
            if (data.type == 'depart') {
                var dep_name = data.obj;
                $scope.selectDepartment(dep_name);

                $scope.members_filtered.forEach(function(m) {
                    if (dep_name == 'all' || m.department == dep_name) {
                        m.selected = true;
                    }
                });
                syncCompanyMembers();
            } else {
                var member = $scope.members.find(function (m) {
                    return m.id === data.obj.id;
                });
                member.selected = true;

                syncCompanyMembers();
            }
        }

        $scope.getMemberName = function(member) {
            var result = "";

            if (member.first_name == "" && member.last_name == "") {
                result = member.email;
            } else {
                result = member.first_name + " " + member.last_name;
            }

            return result;
        }


        $scope.deleteConversation = function() {
            Chat.deleteChatRoom(currentRoomID).then(function(resp) {
                $scope.ctx = resp;
                ngDialog.open({
                    template: 'notificationPopup',
                    scope: $scope
                });
            });
        }

        $scope.closeManager = function() {
            $location.path('conversations');
        }

        $scope.deleteDepartmentFromChat = function(is_source, name) {
            var dep_list = "";
            if (is_source) {
                dep_list = $scope.chatSourceMembers;
            } else {
                dep_list = $scope.chatTargetMembers;
            }

            if (name == 'Unknown') {
                name = null;
            }

            angular.forEach(dep_list[name].list, (function(m) {
                m.selected = false;
            }));
            syncCompanyMembers();
        }

        $scope.toggleSelect = function(member_id) {
            var member = $scope.members.find(function (m) {
                return m.id === member_id;
            });
            member.selected = !(member.selected);
            syncCompanyMembers();
        }

        $scope.saveSettings = function() {
            var members = [];
            for (var i = 0; i < $scope.members.length; i++) {
                if ($scope.members[i].selected) {
                    members.push($scope.members[i].user_id);
                }
            }
            Chat.addChatMember(currentRoomID, members).then(function(resp) {
                $scope.ctx = resp;
                ngDialog.open({
                    template: 'notificationPopup',
                    scope: $scope
                });
            });
        }
    }]);

'use strict';

angular
    .module('app')
    .controller('AuthMagicLoginController',
            ['$scope', '$rootScope', '$route', '$uibModal', '$location', 'ngDialog', 'Auth', 'UserStorage', 'Profile', 'localStorageService', 'Company', 'Chat',
            function($scope, $rootScope, $route, $uibModal, $location, ngDialog, Auth, UserStorage, Profile, localStorageService, Company, Chat) {
                $scope.ctx = {};

                var key = $route.current.params.key;
                Auth.signinMagic(key)
                    .then(
                        function (response) {
                            UserStorage.setCompany(response.company);
                            UserStorage.setEmail(response.email);
                            Auth.setCredentials(UserStorage.getCompany(), UserStorage.getEmail(), response.token);

                            Profile
                                .getCompanyOwner()
                                .then(
                                    function (response) {
                                        $rootScope.globals.companyOwner = response;
                                        Profile
                                            .getProfile()
                                            .then(
                                                function (response) {
                                                    $rootScope.isOwner = $rootScope.globals.companyOwner.id == response.id;
                                                    $rootScope.isAuthorized = true;
                                                    $rootScope.globals.currentUser.profile = response;
                                                    localStorageService.set('globals', $rootScope.globals);
                                                    $location.path('/home');
                                                },
                                                function (response) {
                                                    console.log(response);
                                                }
                                            );
                                    },
                                    function (response) {
                                        console.log(response);
                                    }
                                );
                        },
                        function error(response) {
                            console.log(response);
                        }
                    );
            }
        ]
    )
    .controller('AuthVerifyController',
            ['$scope', '$rootScope', '$route', '$uibModal', '$location', 'ngDialog', 'Auth', 'UserStorage', 'Profile', 'localStorageService', 'Company', 'Chat',
            function($scope, $rootScope, $route, $uibModal, $location, ngDialog, Auth, UserStorage, Profile, localStorageService, Company, Chat) {
                $scope.ctx = {};

                var key = $route.current.params.key;
                Auth.activate(key)
                    .then(
                        function (response) {
                            ngDialog.open({
                                template: 'companyActivatedPopup',
                            });
                            $location.path('/');
                        },
                        function error(response) {
                            console.log(response);
                        }
                    );
            }
        ]
    )
    .controller('AuthPasswordResetController',
            ['$scope', '$rootScope', '$route', '$uibModal', '$location', 'ngDialog', 'Auth', 'UserStorage', 'Profile', 'localStorageService', 'Company', 'Chat',
            function($scope, $rootScope, $route, $uibModal, $location, ngDialog, Auth, UserStorage, Profile, localStorageService, Company, Chat) {
                $scope.ctx = {};

                var key = $route.current.params.key;
                var modalInstance = $uibModal.open({
                    animation: true,
                    templateUrl: 'views/modal/change-pwd.modal.tpl.html',
                    controller: 'ChangePwdModalCtrl',
                    windowClass: 'vcenter-modal',
                    resolve: {
                        key: function () {
                            return key;
                        }
                    }
                });
                modalInstance.result.then(
                    function(data) {
                    },
                    function() {
                        console.info('Modal dismissed at: ' + new Date());
                    }
                );
            }
        ]
    )
    .controller('AithInviteController',
            ['$scope', '$rootScope', '$route', '$uibModal', '$location', 'ngDialog', 'Auth', 'UserStorage', 'Profile', 'localStorageService', 'Company', 'Chat',
            function($scope, $rootScope, $route, $uibModal, $location, ngDialog, Auth, UserStorage, Profile, localStorageService, Company, Chat) {
                $scope.ctx = {};

                var key = $route.current.params.key;
                var modalInstance = $uibModal.open({
                    animation: true,
                    templateUrl: 'views/modal/register-member.modal.tpl.html',
                    controller: 'RegisterMemberModalCtrl',
                    windowClass: 'vcenter-modal'
                });
                modalInstance.result.then(
                    function(data) {
                        Company
                            .confirmInviteMember(data.firstName, data.lastName, data.pwd, key)
                            .then(
                                function (response) {
                                    if ('Success' == response.status) {
                                        UserStorage.setCompany(response.company);
                                        UserStorage.setEmail(response.email);
                                        Auth.setCredentials(UserStorage.getCompany(), UserStorage.getEmail(), response.token);

                                        Profile
                                            .getCompanyOwner()
                                            .then(
                                                function (response) {
                                                    $rootScope.globals.companyOwner = response;
                                                    Profile
                                                        .getProfile()
                                                        .then(
                                                            function (response) {
                                                                $rootScope.isOwner = $rootScope.globals.companyOwner.id == response.id;
                                                                $rootScope.isAuthorized = true;
                                                                $rootScope.globals.currentUser.profile = response;
                                                                localStorageService.set('globals', $rootScope.globals);
                                                                $location.path('/settings');
                                                            },
                                                            function (response) {
                                                                console.log(response);
                                                            }
                                                        );
                                                },
                                                function (response) {
                                                    console.log(response);
                                                }
                                            );
                                    }
                                },
                                function (response) {
                                    console.log(response);
                                    $scope.messages = response.data;
                                }
                            );
                    },
                    function() {
                        console.info('Modal dismissed at: ' + new Date());
                    }
                );
            }
        ]
    )
    .controller('CompanyInviteController',
            ['$scope', '$rootScope', '$route', '$uibModal', '$location', 'ngDialog', 'Auth', 'UserStorage', 'Profile', 'localStorageService', 'Company', 'Chat',
            function($scope, $rootScope, $route, $uibModal, $location, ngDialog, Auth, UserStorage, Profile, localStorageService, Company, Chat) {
                $scope.ctx = {};

                var key = $route.current.params.key;
                Company.confirmInviteFriend(key)
                    .then(
                        function (response) {
                            ngDialog.open({
                                template: 'notificationPopupFriendship',
                            });
                            $location.path('/company-profile/' + response.source);
                        },
                        function error(response) {
                            console.log(response);
                        }
                    );
            }
        ]
    )
    .controller('ConversationInviteController',
            ['$scope', '$rootScope', '$route', '$uibModal', '$location', 'ngDialog', 'Auth', 'UserStorage', 'Profile', 'localStorageService', 'Company', 'Chat',
            function($scope, $rootScope, $route, $uibModal, $location, ngDialog, Auth, UserStorage, Profile, localStorageService, Company, Chat) {
                $scope.ctx = {};

                var key = $route.current.params.key;
                Chat.confirmConversationInvite(key)
                    .then(
                        function (response) {
                            console.log(response.chatroom_id);
                            $location.path('conversations');
                        },
                        function error(response) {
                            console.log(response);
                        }
                    );
            }
        ]
    )
    .controller('ChatDeleteController',
            ['$scope', '$rootScope', '$route', '$uibModal', '$location', 'ngDialog', 'Auth', 'UserStorage', 'Profile', 'localStorageService', 'Company', 'Chat',
            function($scope, $rootScope, $route, $uibModal, $location, ngDialog, Auth, UserStorage, Profile, localStorageService, Company, Chat) {
                $scope.ctx = {};

                var key = $route.current.params.key;
                Chat.confirmConversationDelete(key)
                    .then(
                        function (response) {
                            console.log(response.chatroom_id);
                            $location.path('conversations');
                        },
                        function error(response) {
                            console.log(response);
                        }
                    );
            }
        ]
    )
    .controller('CompanyAffiliatedInviteController',
            ['$scope', '$rootScope', '$route', '$uibModal', '$location', 'ngDialog', 'Auth', 'UserStorage', 'Profile', 'localStorageService', 'Company', 'Chat',
            function($scope, $rootScope, $route, $uibModal, $location, ngDialog, Auth, UserStorage, Profile, localStorageService, Company, Chat) {
                $scope.ctx = {};

                var key = $route.current.params.key;
                Company.confirmAffilateInvite(key)
                    .then(
                        function (response) {
                            ngDialog.open({
                                template: 'notificationPopupAffiliated',
                            });
                            $location.path('/');
                        },
                        function error(response) {
                            console.log(response);
                        }
                    );
            }
        ]
    );

(function(){
'use strict';

angular
	.module('app')
	.factory('UserStorage', UserStorageService);

	UserStorageService.$inject = [];

	function UserStorageService() {
		var user = {
			company 	: '',
			email 		: '',
			firstName 	: '',
			lastName	: ''
		};

		var service = {};

		service.getUser = getUser;
		service.setUser = setUser;
		service.getCompany = getCompany;
		service.setCompany = setCompany;
		service.getEmail = getEmail;
		service.setEmail = setEmail;
		service.getFirstName = getFirstName;
		service.setFirstName = setFirstName;
		service.getLastName = getLastName;
		service.setLastName = setLastName;

		function getUser() {
			return user;
		}

		function setUser(u) {
			user = {
				company: u.company,
				email: u.email,
				firstName: u.firstName,
				lastName: u.lastName
			};
		}

		function getCompany() {
			return user.company;
		}

		function setCompany(company) {
			user.company = company;
		}

		function getEmail() {
			return user.email;
		}

		function setEmail(email) {
			user.email = email;
		}

		function getFirstName() {
			return user.firstName;
		}

		function setFirstName(firstName) {
			user.firstName = firstName;
		}

		function getLastName() {
			return user.lastName;
		}

		function setLastName(lastName) {
			user.lastName = lastName;
		}

		return service;
	}
})();
(function(){
'use strict';

angular
	.module('app')
	.factory('Auth', AuthService);

	AuthService.$inject = ['$http', '$rootScope', '$timeout', 'config', 'localStorageService'];

	function AuthService($http, $rootScope, $timeout, config, localStorageService) {
		var service = {};

		service.signin = signin;
		service.signup = signup;
		service.activate = activate;
		service.sendMagic = sendMagic;
		service.signinMagic = signinMagic;
		service.sendResetPwdRequest = sendResetPwdRequest;
		service.setNewPwd = setNewPwd;
		service.setCredentials = setCredentials;
		service.clearCredentials = clearCredentials;

		/**
	 	 * @name signin
	 	 * @desc signin with password
	 	 */
		function signin(company_name, email, password) {
			var request = {
				'company_name'	: company_name,
				'email'			: email,
				'password'		: password
			};

			return $http.post(config.baseURL + '/api/auth/login/', request).then(function(response) {
				return response.data;
			});
		}

		/**
	 	 * @name signup
	 	 * @desc signup with company name, email, password
	 	 */
		function signup(company_name, email, password) {
			var request = {
				'company_name'	: company_name,
				'email'			: email,
				'password'		: password
			};

			return $http.post(config.baseURL + '/api/auth/register/', request).then(function(response) {
				return response.data;
			});
		}

		/**
	 	 * @name activate
	 	 * @desc activate with verify key
	 	 */
		function activate(verify_key) {
			var request = {
				'verify_key'	: verify_key
			};

			return $http.post(config.baseURL + '/api/auth/activate/', request).then(function(response) {
				return response.data;
			});
		}

		/**
	 	 * @name sendMagic
	 	 * @desc send the magic link
	 	 */
		function sendMagic(company_name, email) {
			var request = {
				'company_name'	: company_name,
				'email'			: email
			};

			return $http.post(config.baseURL + '/api/auth/magic-link/', request).then(function(response) {
				return response.data;
			});
		}

		/**
	 	 * @name signinMagic
	 	 * @desc sign in with magic link
	 	 */
		function signinMagic(magic_key) {
			var request = {
				'magic_key'	: magic_key
			};

			return $http.post(config.baseURL + '/api/auth/login-by-link/', request).then(function(response) {
				return response.data;
			});
		}

		/**
	 	 * @name sendResetPwdRequest
	 	 * @desc send reset password request
	 	 */
		function sendResetPwdRequest(company_name, email) {
			var request = {
				'company_name'	: company_name,
				'email'			: email
			};

			return $http.post(config.baseURL + '/api/auth/reset-password/', request).then(function(response) {
				return response.data;
			});
		}

		function setNewPwd(reset_key, password) {
			var request = {
				'reset_key'	: reset_key,
				'password'	: password
			};

			return $http.post(config.baseURL + '/api/auth/new-password/', request).then(function(response) {
				return response.data;
			});
		}

		/**
	 	 * @name setCredentials
	 	 * @desc
	 	 */
	 	function setCredentials(company, profile, token) {
	 		$rootScope.globals = {
	 			currentUser: {
	 				company: company,
	 				profile: profile,
	 				token: token
	 			}
	 		};

	 		$http.defaults.headers.common['Authorization'] = 'JWT ' + token; // jshint ignore:line
	 		localStorageService.set('globals', $rootScope.globals);
	 	}

	 	/**
	 	 * @name clearCredentials
	 	 * @desc
	 	 */
	 	function clearCredentials() {
	 		$rootScope.globals = {};
	 		localStorageService.remove('globals');
	 		$http.defaults.headers.common.Authorization = 'Basic';
	 	}

	 	return service;
	}
})();

(function(){
'use strict';

angular
	.module('app')
	.factory('Profile', ProfileService);

	ProfileService.$inject = ['$http', '$rootScope', '$timeout', 'config'];

	function ProfileService($http, $rootScope, $timeout, config) {
		var service = {};

		service.getProfile = getProfile;
		service.saveProfile = saveProfile;
		service.changePassword = changePassword;
		service.getCompanyOwner = getCompanyOwner;
		service.updateUploadedFile = updateUploadedFile;

		/**
	 	 * @name getProfile
	 	 * @desc get profile
	 	 */
		function getCompanyOwner() {
			return $http.get(config.baseURL + '/api/company_owner/').then(function(response) {
				return response.data;
			});
		}

		/**
	 	 * @name updateUploadedFile
	 	 * @desc update company profile by uploading file
	 	 */
		function updateUploadedFile(data) {
			var request = {
                method: 'PATCH',
                url: config.baseURL + '/api/profile/',
                headers: {
                    'Content-Type': undefined
                },
                data: data,
            };

			return $http(request).then(function(response) {
				return response.data;
			});
		}

		/**
	 	 * @name getProfile
	 	 * @desc get profile
	 	 */
		function getProfile() {
			return $http.get(config.baseURL + '/api/profile/').then(function(response) {
				return response.data;
			});
		}

		/**
	 	 * @name saveProfile
	 	 * @desc save profile
	 	 */
		function saveProfile(objData) {
            var request = {};
            angular.copy(objData, request);
			delete request['avatar'];
			return $http.put(config.baseURL + '/api/profile/', request).then(function(response) {
				return response.data;
			});
		}

		/**
	 	 * @name changePassword
	 	 * @desc Change password
	 	 */
		function changePassword(oldPwd, newPwd) {
			var objData = {
				old_password: oldPwd,
				new_password: newPwd
			};

			return $http.put(config.baseURL + '/api/profile/change-password/', objData).then(function(response) {
				return response.data;
			});
		}

	 	return service;
	}
})();

(function(){
'use strict';

angular
	.module('app')
	.factory('Company', CompanyService);

	CompanyService.$inject = ['$http', '$rootScope', '$timeout', 'config'];

	function CompanyService($http, $rootScope, $timeout, config) {
		var service = {};

		service.getCompanyList = getCompanyList;
		service.getCompanyDetail = getCompanyDetail;
		service.getCompanyProfile = getCompanyProfile;
		service.updateCompanyProfile = updateCompanyProfile;
		service.updateUploadedFile = updateUploadedFile;
		service.createCompanyInfo = createCompanyInfo;
		service.createCompanyFriend = createCompanyFriend;
		service.getCompanyMembers = getCompanyMembers;
		service.getCompanyMemberWithId = getCompanyMemberWithId;
		service.inviteMember = inviteMember;
		service.confirmInviteMember = confirmInviteMember;
		service.inviteFriend = inviteFriend;
		service.deleteFriend = deleteFriend;
		service.confirmInviteFriend = confirmInviteFriend;
		service.createCompanyMember = createCompanyMember;
		service.updateCompanyMember = updateCompanyMember;
		service.deleteCompanyMember = deleteCompanyMember;
		service.getAffiliCompanies = getAffiliCompanies;
        service.sendAffilateRequest = sendAffilateRequest;
        service.confirmAffilateInvite = confirmAffilateInvite;
        service.deleteAffilateCompany = deleteAffilateCompany;
        service.deleteMember = deleteMember;
        service.getCompanyFriends = getCompanyFriends;

		/**
	 	 * @name getCompanyList
	 	 * @desc get company list
	 	 */
		function getCompanyList() {
			return $http.get(config.baseURL + '/api/company/list/').then(function(response) {
				return response.data;
			});
		}

		/**
	 	 * @name getCompanyDetail
	 	 * @desc get company detail
	 	 * @param id: company id
	 	 */
		function getCompanyDetail(id) {
			return $http.get(config.baseURL + '/api/company/list/' + id + '/').then(function(response) {
				return response.data;
			});
		}

		/**
	 	 * @name getCompanyProfile
	 	 * @desc get company profile
	 	 */
		function getCompanyProfile() {
			return $http.get(config.baseURL + '/api/company/profile/get/').then(function(response) {
				return response.data;
			});
		}

		/**
	 	 * @name updateCompanyProfile
	 	 * @desc update company profile
	 	 */
		function updateCompanyProfile(data) {
			delete data['logo'];
			delete data['cover'];
			return $http.put(config.baseURL + '/api/company/profile/put/', data).then(function(response) {
				return response.data;
			});
		}

		/**
	 	 * @name updateUploadedFile
	 	 * @desc update company profile by uploading file
	 	 */
		function updateUploadedFile(data) {
			var request = {
                method: 'PATCH',
                url: config.baseURL + '/api/company/profile/put/',
                headers: {
                    'Content-Type': undefined
                },
                data: data,
            };

			return $http(request).then(function(response) {
				return response.data;
			});
		}

		/**
	 	 * @name createCompanyInfo
	 	 * @desc create company info
	 	 */
		function createCompanyInfo() {
			var request = {
			};

			return $http.post(config.baseURL + '/api/company/info/', request).then(function(response) {
				return response.data;
			});
		}

		/**
	 	 * @name createCompanyFriend
	 	 * @desc create company friend
	 	 */
		function createCompanyFriend() {
			var request = {

			};

			return $http.post(config.baseURL + '/api/company/friends/', request).then(function(response) {
				return response.data;
			});
		}


		function getCompanyFriends() {
			return $http.get(config.baseURL + '/api/company/friends/list/').then(function(response) {
				return response.data;
			});
		}

		/**
	 	 * @name sendAffilateRequest
	 	 * @desc send request to add company with 'id' as affiliated
	 	 */
		function sendAffilateRequest(id) {
			var request = {
                target: id
			};

			return $http.post(config.baseURL + '/api/company/affiliated/invite/', request).then(function(response) {
				return response.data;
			});
		}

		/**
	 	 * @name deleteAffilateCompany
	 	 * @desc delete company fro affilated list
	 	 */
		function deleteAffilateCompany(id) {
			var request = {
                target: id
			};

			return $http.post(config.baseURL + '/api/company/affiliated/delete/', request).then(function(response) {
				return response.data;
			});
		}

		/**
	 	 * @name confirmAffilateInvite
	 	 * @desc confirm affiliated company
	 	 */
		function confirmAffilateInvite(key) {
			return $http.post(config.baseURL + '/api/company/affiliated/confirm/' + key + '/').then(function(response) {
				return response.data;
			});
		}

		/**
	 	 * @name getCompanyMembers
	 	 * @desc get company member
	 	 */
		function getCompanyMembers() {
			return $http.get(config.baseURL + '/api/v1/members/').then(function(response) {
				return response.data;
			});
		}

		/**
	 	 * @name getCompanyMember
	 	 * @desc get company member with id
	 	 */
		function getCompanyMemberWithId(id) {
			return $http.get(config.baseURL + '/api/v1/members/?company=' + id).then(function(response) {
				return response.data;
			});
		}

		/**
	 	 * @name inviteMember
	 	 * @desc invite member via email
	 	 */
		function inviteMember(data) {
			return $http.post(config.baseURL + '/api/company/member/invite/', data).then(function(response) {
				return response.data;
			});
		}

		/**
	 	 * @name confirmInviteMember
	 	 * @desc confirm member invitation
	 	 */
		function confirmInviteMember(firstName, lastName, pwd, key) {
			var data = {
                first_name: firstName,
                last_name: lastName,
                password: pwd
			};
			return $http.post(config.baseURL + '/api/company/invite/confirm/' + key + '/', data).then(function(response) {
				return response.data;
			});
		}

		/**
	 	 * @name deleteMember
	 	 * @desc delete member by id
	 	 */
		function deleteMember(id) {
			var data = {
				user_id: id
			};
			return $http.post(config.baseURL + '/api/company/member/delete/', data).then(function(response) {
				return response.data;
			});
		}

		/**
	 	 * @name inviteFriend
	 	 * @desc invite friend via email
	 	 */
		function inviteFriend(id) {
			var data = {
				target: id
			};
			return $http.post(config.baseURL + '/api/company/friend/invite/', data).then(function(response) {
				return response.data;
			});
		}

		/**
	 	 * @name deleteFriend
	 	 * @desc delete friend by id
	 	 */
		function deleteFriend(id) {
			var data = {
				target: id
			};
			return $http.post(config.baseURL + '/api/company/friend/delete/', data).then(function(response) {
				return response.data;
			});
		}

		/**
	 	 * @name confirmInviteFriend
	 	 * @desc confirm friend invitation
	 	 */
		function confirmInviteFriend(key) {
			return $http.post(config.baseURL + '/api/company/friend/confirm/' + key + '/').then(function(response) {
				return response.data;
			});
		}

		/**
	 	 * @name createCompanyMember
	 	 * @desc create company member
	 	 */
		function createCompanyMember() {
			var request = {
			};

			return $http.post(config.baseURL + '/api/company/member/', request).then(function(response) {
				return response.data;
			});
		}

		/**
	 	 * @name updateCompanyMember
	 	 * @desc update company member
	 	 */
		function updateCompanyMember() {
			var request = {

			};

			return $http.put(config.baseURL + '/api/company/member/', request).then(function(response) {
				return response.data;
			});
		}

		/**
	 	 * @name getCompanyList
	 	 * @desc get company list
	 	 */
		function deleteCompanyMember() {
			var request = {
			};

			return $http.delete(config.baseURL + '/api/company/member/', request).then(function(response) {
				return response.data;
			});
		}

		/**
	 	 * @name getAffiliCompanies
	 	 * @desc get affiliated company list
	 	 */
		function getAffiliCompanies() {
			return $http.get(config.baseURL + '/api/company/affiliated/list/').then(function(response) {
				return response.data;
			});
		}

	 	return service;
	}
})();

(function(){
'use strict';

angular
    .module('app')
    .factory('Common', CommonService);

    CommonService.$inject = ['$http', '$rootScope', '$timeout', 'config'];

    function CommonService($http, $rootScope, $timeout, config) {
        var service = {};

        service.getIndustryList = getIndustryList;
        service.getDepartList = getDepartList;
        service.getCompanySizeList = getCompanySizeList;
        service.getCountryList = getCountryList;
        service.getStateList = getStateList;
        service.getCompanyTypeList = getCompanyTypeList;

        /**
         * @name getIndustryList
         * @desc get company list
         */
        function getIndustryList() {
            return $http.get(config.baseURL + '/api/company/industry/list/').then(function(response) {
                var list = [];
                angular.forEach(response.data, function(item) {
                    list.push({name: item});
                });
                return list;
            });
        }

        function getDepartList() {
            return $http.get(config.baseURL + '/api/company/department/list/').then(function(response) {
                var list = [];
                angular.forEach(response.data, function(item) {
                    list.push({name: item});
                });
                return list;
            });
        }

        function getCompanyTypeList() {
            var list = [
                {
                    id: 1,
                    name: 'Privately Held'
                },
                {
                    id: 2,
                    name: 'Public Company'
                },
                {
                    id: 3,
                    name: 'Educational'
                },
                {
                    id: 4,
                    name: 'Government Agency'
                },
                {
                    id: 5,
                    name: 'Non Profit'
                },
            ];
            return list;
        }


        function getCompanySizeList() {
            var list = [
                {
                    id: 1,
                    name: '1-10 employees'
                },
                {
                    id: 2,
                    name: '11-50 employees'
                },
                {
                    id: 3,
                    name: '51-100 employees'
                },
                {
                    id: 4,
                    name: '101-200 employees'
                },
                {
                    id: 5,
                    name: '201-500 employees'
                },
                {
                    id: 6,
                    name: '501-1000 employees'
                },
            ];
            return list;
        }

        function getCountryList() {
            var list = [
                {
                    id: 1,
                    name: 'Netherlands'
                },
                {
                    id: 2,
                    name: 'United States'
                },
                {
                    id: 3,
                    name: 'Sweden'
                }
            ];
            return list;
        }

        function getStateList() {
            var list = [
                {
                    "name": "Alabama",
                    "abbreviation": "AL"
                },
                {
                    "name": "Alaska",
                    "abbreviation": "AK"
                },
                {
                    "name": "American Samoa",
                    "abbreviation": "AS"
                },
                {
                    "name": "Arizona",
                    "abbreviation": "AZ"
                },
                {
                    "name": "Arkansas",
                    "abbreviation": "AR"
                },
                {
                    "name": "California",
                    "abbreviation": "CA"
                },
                {
                    "name": "Colorado",
                    "abbreviation": "CO"
                },
                {
                    "name": "Connecticut",
                    "abbreviation": "CT"
                },
                {
                    "name": "Delaware",
                    "abbreviation": "DE"
                },
                {
                    "name": "District Of Columbia",
                    "abbreviation": "DC"
                },
                {
                    "name": "Federated States Of Micronesia",
                    "abbreviation": "FM"
                },
                {
                    "name": "Florida",
                    "abbreviation": "FL"
                },
                {
                    "name": "Georgia",
                    "abbreviation": "GA"
                },
                {
                    "name": "Guam",
                    "abbreviation": "GU"
                },
                {
                    "name": "Hawaii",
                    "abbreviation": "HI"
                },
                {
                    "name": "Idaho",
                    "abbreviation": "ID"
                },
                {
                    "name": "Illinois",
                    "abbreviation": "IL"
                },
                {
                    "name": "Indiana",
                    "abbreviation": "IN"
                },
                {
                    "name": "Iowa",
                    "abbreviation": "IA"
                },
                {
                    "name": "Kansas",
                    "abbreviation": "KS"
                },
                {
                    "name": "Kentucky",
                    "abbreviation": "KY"
                },
                {
                    "name": "Louisiana",
                    "abbreviation": "LA"
                },
                {
                    "name": "Maine",
                    "abbreviation": "ME"
                },
                {
                    "name": "Marshall Islands",
                    "abbreviation": "MH"
                },
                {
                    "name": "Maryland",
                    "abbreviation": "MD"
                },
                {
                    "name": "Massachusetts",
                    "abbreviation": "MA"
                },
                {
                    "name": "Michigan",
                    "abbreviation": "MI"
                },
                {
                    "name": "Minnesota",
                    "abbreviation": "MN"
                },
                {
                    "name": "Mississippi",
                    "abbreviation": "MS"
                },
                {
                    "name": "Missouri",
                    "abbreviation": "MO"
                },
                {
                    "name": "Montana",
                    "abbreviation": "MT"
                },
                {
                    "name": "Nebraska",
                    "abbreviation": "NE"
                },
                {
                    "name": "Nevada",
                    "abbreviation": "NV"
                },
                {
                    "name": "New Hampshire",
                    "abbreviation": "NH"
                },
                {
                    "name": "New Jersey",
                    "abbreviation": "NJ"
                },
                {
                    "name": "New Mexico",
                    "abbreviation": "NM"
                },
                {
                    "name": "New York",
                    "abbreviation": "NY"
                },
                {
                    "name": "North Carolina",
                    "abbreviation": "NC"
                },
                {
                    "name": "North Dakota",
                    "abbreviation": "ND"
                },
                {
                    "name": "Northern Mariana Islands",
                    "abbreviation": "MP"
                },
                {
                    "name": "Ohio",
                    "abbreviation": "OH"
                },
                {
                    "name": "Oklahoma",
                    "abbreviation": "OK"
                },
                {
                    "name": "Oregon",
                    "abbreviation": "OR"
                },
                {
                    "name": "Palau",
                    "abbreviation": "PW"
                },
                {
                    "name": "Pennsylvania",
                    "abbreviation": "PA"
                },
                {
                    "name": "Puerto Rico",
                    "abbreviation": "PR"
                },
                {
                    "name": "Rhode Island",
                    "abbreviation": "RI"
                },
                {
                    "name": "South Carolina",
                    "abbreviation": "SC"
                },
                {
                    "name": "South Dakota",
                    "abbreviation": "SD"
                },
                {
                    "name": "Tennessee",
                    "abbreviation": "TN"
                },
                {
                    "name": "Texas",
                    "abbreviation": "TX"
                },
                {
                    "name": "Utah",
                    "abbreviation": "UT"
                },
                {
                    "name": "Vermont",
                    "abbreviation": "VT"
                },
                {
                    "name": "Virgin Islands",
                    "abbreviation": "VI"
                },
                {
                    "name": "Virginia",
                    "abbreviation": "VA"
                },
                {
                    "name": "Washington",
                    "abbreviation": "WA"
                },
                {
                    "name": "West Virginia",
                    "abbreviation": "WV"
                },
                {
                    "name": "Wisconsin",
                    "abbreviation": "WI"
                },
                {
                    "name": "Wyoming",
                    "abbreviation": "WY"
                }
            ];
            return list;
        }

        return service;
    }

})();

(function(){
'use strict';

angular
    .module('app')
    .factory('Chat', ChatService);

    ChatService.$inject = ['$http', '$rootScope', '$timeout', 'config'];

    function ChatService($http, $rootScope, $timeout, config) {
        var service = {};

        service.getNotifications = getNotifications;
        service.createChatRoom = createChatRoom;
        service.confirmConversationInvite = confirmConversationInvite;
        service.confirmConversationDelete = confirmConversationDelete;
        service.deleteChatRoom = deleteChatRoom;
        service.getChatRooms = getChatRooms;
        service.getChatRoom = getChatRoom;
        service.addChatMember = addChatMember;
        service.delChatMember = delChatMember;
        service.uploadFile = uploadFile;
        service.updateNotifications = updateNotifications;

        function uploadFile(file) {
            var fd = new FormData();
            fd.append("file", file, file.name);
            fd.append("filename", file.name);

            return $http({
                method: 'POST',
                url: config.baseURL + '/api/v1/files/',
                data: fd,
                headers: {
                    'Content-Type': undefined
                }
            });
        }

        function getChatRooms() {
            return $http.get(config.baseURL + '/api/v1/rooms/').then(function(response) {
                return response.data;
            });
        }

        function deleteChatRoom(id) {
            var request = {
                room_id: id,
            }
            return $http.post(config.baseURL + '/api/conversation/delete/', request).then(function(response) {
                return response.data;
            });
        }

        function confirmConversationDelete(key) {
            return $http.post(config.baseURL + '/api/conversation/confirm_delete/' + key + '/').then(function(response) {
                return response.data;
            });
        }

        function addChatMember(id, members) {
            var request = {
                room_id: id,
                members_id: members
            }

            return $http.post(config.baseURL + '/api/conversation/add_member/', request).then(function(response) {
                return response.data;
            });
        }

        function delChatMember(id, member) {
            var request = {
                room_id: id,
                member_id: member
            }

            return $http.post(config.baseURL + '/api/conversation/del_member/', request).then(function(response) {
                return response.data;
            });
        }

        function getChatRoom(id) {
            return $http.get(config.baseURL + '/api/v1/rooms/' + id + '/').then(function(response) {
                return response.data;
            });
        }

        /**
         * @name confirmConversationInvite
         * @desc confirm affiliated company
         */
        function confirmConversationInvite(key) {
            return $http.post(config.baseURL + '/api/conversation/confirm/' + key + '/').then(function(response) {
                return response.data;
            });
        }

        /**
         * @name getNotifications
         * @desc get notifications
         */
        function getNotifications(userId) {
            return $http.get(config.baseURL + '/api/v1/notifications/?user=' + userId).then(function(response) {
                return response.data;
            });
        }

        function updateNotifications(id, data) {
            return $http.patch(config.baseURL + '/api/v1/notifications/' + id + '/', data).then(function(response) {
                return response.data;
            });
        }


        /**
         * @name getNotifications
         * @desc create chat room
         */
        function createChatRoom(data) {
            return $http.post(config.baseURL + '/api/conversation/new_room/', data).then(function(response) {
                return response.data;
            });
        }


        return service;
    }
})();

'use strict';

angular
    .module('app')
    .directive('fileUpload', function () {
        return {
            scope: true,        //create a new scope
            link: function (scope, el, attrs) {
                el.bind('change', function (event) {
                    var files = event.target.files;
                    scope.$emit("fileSelected", {key: attrs.profileAttr, file: files[0] });
                });
            }
        }
    });

'use strict';

angular
    .module('app')
    .directive('backImg', function () {
        return {
            scope: true,        //create a new scope
            link: function (scope, element, attrs) {
                scope.$watch(function() { return attrs.backImg; }, function () {
                    element.css({
                        'background-image': 'url(' + attrs.backImg +')',
                        'background-size' : 'cover'
                    });
                });                
            }
        }
    });
'use strict';

angular
    .module('app')
    .directive('compareTo', function () {
        return {
            require: "ngModel",
            scope: {
                otherModelValue: "=compareTo"
            },
            link: function(scope, element, attributes, ngModel) {

                ngModel.$validators.compareTo = function(modelValue) {
                    return modelValue == scope.otherModelValue;
                };

                scope.$watch("otherModelValue", function() {
                    ngModel.$validate();
                });
            }
        };
});

'use strict';

angular
    .module('app')
    .directive('autoGrow', function() {
        return function(scope, element, attr){
            var minHeight = element[0].offsetHeight,
            paddingLeft = element.css('paddingLeft'),
            paddingRight = element.css('paddingRight');

            var $shadow = angular.element('<div></div>').css({
                position: 'absolute',
                opacity: 0,
                top: -10000,
                left: -10000,
                width: element[0].offsetWidth - parseInt(paddingLeft || 0) - parseInt(paddingRight || 0),
                fontSize: element.css('fontSize'),
                fontFamily: element.css('fontFamily'),
                lineHeight: element.css('lineHeight'),
                resize:     'none'
            });
            angular.element(document.body).append($shadow);

            var update = function() {
                var times = function(string, number) {
                    for (var i = 0, r = ''; i < number; i++) {
                        r += string;
                    }
                    return r;
                }

                var val = element.val().replace(/</g, '&lt;')
                    .replace(/>/g, '&gt;')
                    .replace(/&/g, '&amp;')
                    .replace(/\n$/, '<br/>&nbsp;')
                    .replace(/\n/g, '<br/>')
                    .replace(/\s{2,}/g, function(space) { return times('&nbsp;', space.length - 1) + ' ' });
                $shadow.html(val);

                element.css('height', Math.max($shadow[0].offsetHeight + 10 /* the "threshold" */, minHeight) + 'px');
            }

            scope.$on('$destroy', function() {
                $shadow.remove();
            });

            element.bind('keyup keydown keypress change', update);
            update();
        }
});
