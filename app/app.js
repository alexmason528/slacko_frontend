'use strict';

angular.module('app', [
	'ngRoute',
    'ngMessages',
    'ngDialog',
	'ngAnimate',
	'ngTouch',
	'ngResource',
    'ngDraggable',
	'ui.bootstrap',
	'angular-nicescroll',
	'LocalStorageModule',
	'angular-loading-bar',
	'angular-click-outside',
    'thatisuday.dropzone',
	'app.routes',
	'app.constants',
	'app.config'
]);
