'use strict';

angular
    .module('app.constants', [])
    .constant('config', {
    	'baseURL': 'http://127.0.0.1:8000',
    	'chatURL': '127.0.0.1:8000',
    	'socials': ['facebook', 'twitter', 'instagram', 'google_plus', 'linkedin', 'youtube', 'github', 'stackoverflow']
	});
