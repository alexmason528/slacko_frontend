'use strict';

angular
    .module('app')
    .controller('AuthModalCtrl', ['$scope', '$uibModalInstance', '$uibModal', 'UserStorage', 'Auth',
            function($scope, $uibModalInstance, $uibModal, UserStorage, Auth) {
        $scope.signinCompanyName = '';
        $scope.signupCompanyName = '';

        $scope.signin = function () {
            if ($scope.signInForm.$valid) {
                UserStorage.setCompany($scope.signinCompanyName);
                $uibModalInstance.close();
                var modalInstance = $uibModal.open({
                    animation: true,
                    templateUrl: 'views/modal/signin-email.modal.tpl.html',
                    controller: 'SigninEmailModalCtrl',
                    windowClass: 'vcenter-modal'
                });
                modalInstance.result.then(
                    function(data) {

                    },
                    function() {
                        console.info('Modal dismissed at: ' + new Date());
                    }
                );
            }
        };

        $scope.signup = function () {
            if ($scope.signUpForm.$valid) {
                UserStorage.setCompany($scope.signupCompanyName);
                $uibModalInstance.close();
                var modalInstance = $uibModal.open({
                    animation: true,
                    templateUrl: 'views/modal/register-company.modal.tpl.html',
                    controller: 'RegisterCompanyModalCtrl',
                    windowClass: 'vcenter-modal'
                });
                modalInstance.result.then(
                    function(data) {

                    },
                    function() {
                        console.info('Modal dismissed at: ' + new Date());
                    }
                );
            }
        };

        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
    }])
    .controller('SigninModalCtrl', ['$scope', '$uibModalInstance', '$uibModal', 'UserStorage', 'Auth',
            function($scope, $uibModalInstance, $uibModal, UserStorage, Auth) {
        $scope.signinCompanyName = '';
        $scope.signupCompanyName = '';

        $scope.signinNext = function () {
            if ($scope.signInForm.$valid) {
                UserStorage.setCompany($scope.signinCompanyName);
                $uibModalInstance.close();
                var modalInstance = $uibModal.open({
                    animation: true,
                    templateUrl: 'views/modal/signin-email.modal.tpl.html',
                    controller: 'SigninEmailModalCtrl',
                    windowClass: 'vcenter-modal'
                });
                modalInstance.result.then(
                    function(data) {

                    },
                    function() {
                        console.info('Modal dismissed at: ' + new Date());
                    }
                );
            }
    	};

    	$scope.signupNext = function () {
            if ($scope.signUpForm.$valid) {
                UserStorage.setCompany($scope.signupCompanyName);
                $uibModalInstance.close();
    			var modalInstance = $uibModal.open({
        			animation: true,
        			templateUrl: 'views/modal/register-company.modal.tpl.html',
        			controller: 'RegisterCompanyModalCtrl',
        			windowClass: 'vcenter-modal'
        		});
        		modalInstance.result.then(
        			function(data) {

        			},
        			function() {
        				console.info('Modal dismissed at: ' + new Date());
        			}
    			);
            }
    	};

    	$scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
    }])
    .controller('SigninEmailModalCtrl', ['$scope', '$uibModalInstance', '$uibModal', 'UserStorage', 'Auth',
            function($scope, $uibModalInstance, $uibModal, UserStorage, Auth) {
        $scope.companyName = UserStorage.getCompany();
        $scope.email = '';

    	$scope.back = function () {
			$uibModalInstance.close();
			var modalInstance = $uibModal.open({
    			animation: true,
    			templateUrl: 'views/modal/signin.modal.tpl.html',
    			controller: 'SigninModalCtrl',
    			windowClass: 'vcenter-modal'
    		});
    		modalInstance.result.then(
    			function(data) {

    			},
    			function() {
    				console.info('Modal dismissed at: ' + new Date());
    			}
			);
    	};

    	$scope.continue = function () {
            if($scope.signInForm.$valid) {
                UserStorage.setEmail($scope.email);
                $uibModalInstance.close();
                var modalInstance = $uibModal.open({
                    animation: true,
                    templateUrl: 'views/modal/signin-pwd.modal.tpl.html',
                    controller: 'SigninPwdModalCtrl',
                    windowClass: 'vcenter-modal'
                });
                modalInstance.result.then(
                    function(data) {
                    },
                    function() {
                        console.info('Modal dismissed at: ' + new Date());
                    }
                );
            }
    	};

    	$scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
    }])
    .controller('SigninPwdModalCtrl', ['$scope', '$rootScope', '$uibModalInstance', '$uibModal', '$location', 'localStorageService', 'UserStorage', 'Auth', 'Profile', 'ngDialog',
            function($scope, $rootScope, $uibModalInstance, $uibModal, $location, localStorageService, UserStorage, Auth, Profile, ngDialog) {
        $scope.pwd = '';
        $scope.msg_error_pwd = '';
        $scope.msg_error_link = '';

    	$scope.back = function () {
			$uibModalInstance.close();
			var modalInstance = $uibModal.open({
    			animation: true,
    			templateUrl: 'views/modal/signin-email.modal.tpl.html',
    			controller: 'SigninEmailModalCtrl',
    			windowClass: 'vcenter-modal'
    		});
    		modalInstance.result.then(
    			function(data) {
    			},
    			function() {
    				console.info('Modal dismissed at: ' + new Date());
    			}
			);
    	};

    	$scope.signinWithDiff = function () {
            /*
            $uibModalInstance.close();
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'views/modal/signin-email.modal.tpl.html',
                controller: 'SigninEmailModalCtrl',
                windowClass: 'vcenter-modal'
            });
            modalInstance.result.then(
                function(data) {
                },
                function() {
                    console.info('Modal dismissed at: ' + new Date());
                }
            );*/
            Auth
                .sendMagic(UserStorage.getCompany(), UserStorage.getEmail())
                .then(
                    function (response) {
                        $uibModalInstance.close();
                        console.log(response);
                        ngDialog.open({
                            template: 'magicLinkSentPopup',
                        });
                    },
                    function (response) {
                        console.log(response);
                        var key = '';
                        for (key in response.data) break;
                        $scope.msg_error_link = response.data[key][0];
                    }
                );
    	};

        $scope.$watch('pwd', function() {
            $scope.msg_error_pwd = '';
        });

    	$scope.signin = function () {
            if ($scope.signInForm.$valid) {
                Auth
                    .signin(UserStorage.getCompany(), UserStorage.getEmail(), $scope.pwd)
                    .then(
                        function (response) {
                            $uibModalInstance.close();
                            Auth.setCredentials(UserStorage.getCompany(), UserStorage.getEmail(), response.token);

                            Profile
                                .getCompanyOwner()
                                .then(
                                    function (response) {
                                        $rootScope.globals.companyOwner = response;
                                        Profile
                                            .getProfile()
                                            .then(
                                                function (response) {
                                                    $rootScope.isOwner = $rootScope.globals.companyOwner.id == response.id;
                                                    $rootScope.isAuthorized = true;
                                                    $rootScope.globals.currentUser.profile = response;
                                                    localStorageService.set('globals', $rootScope.globals);
                                                    $location.path('/home');
                                                },
                                                function (response) {
                                                }
                                            );
                                    },
                                    function (response) {
                                        console.log(response);
                                    }
                                );
                        },
                        function (response) {
                            var key = '';
                            for (key in response.data) break;
                            $scope.msg_error_pwd = response.data[key][0];
                        }
                    );
            }
    	};

    	$scope.forgotPwd = function () {
    		$uibModalInstance.close();
			var modalInstance = $uibModal.open({
    			animation: true,
    			templateUrl: 'views/modal/reset-pwd.modal.tpl.html',
    			controller: 'ResetPwdModalCtrl',
    			windowClass: 'vcenter-modal'
    		});
    		modalInstance.result.then(
    			function(data) {
    			},
    			function() {
    				console.info('Modal dismissed at: ' + new Date());
    			}
			);
    	};

    	$scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
    }])
    .controller('ResetPwdModalCtrl', ['$scope', '$uibModalInstance', '$uibModal', 'UserStorage', 'Auth', 'ngDialog',
            function($scope, $uibModalInstance, $uibModal, UserStorage, Auth, ngDialog) {
        $scope.email = '';

        $scope.back = function () {
			$uibModalInstance.close();
			var modalInstance = $uibModal.open({
    			animation: true,
    			templateUrl: 'views/modal/signin-pwd.modal.tpl.html',
    			controller: 'SigninPwdModalCtrl',
    			windowClass: 'vcenter-modal'
    		});
    		modalInstance.result.then(
    			function(data) {
    			},
    			function() {
    				console.info('Modal dismissed at: ' + new Date());
    			}
			);
    	};

    	$scope.resetPwd = function () {
            if ($scope.resetPwdForm.$valid) {
                Auth.
                    sendResetPwdRequest(UserStorage.getCompany(), $scope.email)
                    .then(
                        function (response) {
                            $uibModalInstance.close();
                            ngDialog.open({
                                template: 'passwordResetPopup',
                            });
                        },
                        function (response) {
                            console.log(response);
                        }
                    );
            }
    	};

    	$scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
    }])
    .controller('ChangePwdModalCtrl', ['$scope', '$uibModalInstance', '$uibModal', 'UserStorage', 'Auth', 'key', 'ngDialog',
            function($scope, $uibModalInstance, $uibModal, UserStorage, Auth, key, ngDialog) {
        $scope.messages = [];

    	$scope.back = function () {
			$uibModalInstance.close();
			var modalInstance = $uibModal.open({
    			animation: true,
    			templateUrl: 'views/modal/reset-pwd.modal.tpl.html',
    			controller: 'ResetPwdModalCtrl',
    			windowClass: 'vcenter-modal'
    		});
    		modalInstance.result.then(
    			function(data) {
    			},
    			function() {
    				console.info('Modal dismissed at: ' + new Date());
    			}
			);
    	};

    	$scope.changePwd = function () {
            if (($scope.changePwdForm.$valid) && ($scope.pwd === $scope.confirmPwd)) {
                $scope.messages = [];
                Auth
                    .setNewPwd(key, $scope.pwd)
                    .then(
                        function (response) {
                            $uibModalInstance.close();
                            ngDialog.open({
                                template: 'passwordResetCompletePopup',
                            });
                        },
                        function (response) {
                            console.log(response);
                            $scope.messages = response.data;
                        }
                    );
            }
    	};

    	$scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
    }])
    .controller('RegisterCompanyModalCtrl', ['$scope', '$uibModalInstance', '$uibModal', 'UserStorage', 'Auth',
            function($scope, $uibModalInstance, $uibModal, UserStorage, Auth) {
        $scope.email = '';

    	$scope.back = function () {
			$uibModalInstance.close();
			var modalInstance = $uibModal.open({
    			animation: true,
    			templateUrl: 'views/modal/signin.modal.tpl.html',
    			controller: 'SigninModalCtrl',
    			windowClass: 'vcenter-modal'
    		});
    		modalInstance.result.then(
    			function(data) {
    			},
    			function() {
    				console.info('Modal dismissed at: ' + new Date());
    			}
			);
    	};

    	$scope.continue = function () {
            if ($scope.registerForm.$valid) {
                UserStorage.setEmail($scope.email);
                $uibModalInstance.close();
                var modalInstance = $uibModal.open({
                    animation: true,
                    templateUrl: 'views/modal/register-pwd.modal.tpl.html',
                    controller: 'RegisterPwdModalCtrl',
                    windowClass: 'vcenter-modal'
                });
                modalInstance.result.then(
                    function(data) {
                    },
                    function() {
                        console.info('Modal dismissed at: ' + new Date());
                    }
                );
            }
    	};

    	$scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
    }])
    .controller('RegisterMemberModalCtrl', ['$scope', '$rootScope', '$uibModalInstance', '$uibModal', '$location', 'UserStorage', 'Auth', 'Company', 'ngDialog',
            function($scope, $rootScope, $uibModalInstance, $uibModal, $location, UserStorage, Auth, Company, ngDialog) {
        $scope.firstName = '';
        $scope.lastName = '';
        $scope.pwd = '';
        $scope.confirmPwd = '';
        $scope.messages = [];

    	$scope.createCompany = function () {
            if (($scope.registerForm.$valid) && ($scope.pwd === $scope.confirmPwd)) {
                $scope.messages = [];
                $uibModalInstance.close({'firstName': $scope.firstName, 'lastName': $scope.lastName, 'pwd': $scope.pwd});
            }
    	};

    	$scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
    }])
    .controller('RegisterPwdModalCtrl', ['$scope', '$rootScope', '$uibModalInstance', '$uibModal', '$location', 'UserStorage', 'Auth', 'ngDialog',
            function($scope, $rootScope, $uibModalInstance, $uibModal, $location, UserStorage, Auth, ngDialog) {
        $scope.firstName = '';
        $scope.lastName = '';
        $scope.pwd = '';
        $scope.confirmPwd = '';
        $scope.messages = [];

    	$scope.back = function () {
			$uibModalInstance.close();
			var modalInstance = $uibModal.open({
    			animation: true,
    			templateUrl: 'views/modal/register-company.modal.tpl.html',
    			controller: 'RegisterCompanyModalCtrl',
    			windowClass: 'vcenter-modal'
    		});
    		modalInstance.result.then(
    			function(data) {
    			},
    			function() {
    				console.info('Modal dismissed at: ' + new Date());
    			}
			);
    	};

    	$scope.createCompany = function () {
            if (($scope.registerForm.$valid) && ($scope.pwd === $scope.confirmPwd)) {
                $scope.messages = [];
                Auth
                    .signup(UserStorage.getCompany(), UserStorage.getEmail(), $scope.pwd)
                    .then(
                        function (response) {
                            $uibModalInstance.close();
                            ngDialog.open({
                                template: 'companyCreatedPopup',
                            });
                        },
                        function (response) {
                            console.log(response);
                            $scope.messages = response.data;
                        }
                    );
            }

    	};

    	$scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };
    }])
    .controller('EditFriendsModalCtrl', ['$scope', '$uibModalInstance', '$uibModal', 'UserStorage', 'Auth', 'Company', 'Common', 'friends', 'company',
            function($scope, $uibModalInstance, $uibModal, UserStorage, Auth, Company, Common, friends, company) {
        $scope.isSearchDropdownOpened = false;
        $scope.allFiltered = true;
        $scope.companyFriends = [];
        $scope.industryList = [];
        $scope.filters = [];

        $scope.getIndustryList = getIndustryList;
        $scope.filter = filter;
        $scope.selectItem = selectItem;
        $scope.selectAllItem = selectAllItem;
        $scope.applyFilter = applyFilter;
        //$scope.resetFilter = resetFilter;
        $scope.deleteFilter = deleteFilter;
        $scope.deleteFriend = deleteFriend;
        $scope.deleteAllFilters = deleteAllFilters;
        $scope.toggleSearchDropdown = toggleSearchDropdown;
        $scope.cancel = cancel;

        init();

        function init() {
            $scope.companyFriends = friends;
            $scope.companyProfile = company;
            console.log('friends:', friends);
            getIndustryList();
        }

        function getIndustryList() {
            Common
                .getIndustryList()
                .then(function(response) {
                    console.log('Industry List:', response);
                    $scope.industryList = response;

                    angular.forEach($scope.industryList, function(item) {
                        angular.extend(item, {
                            filtered: true
                        });
                    });
                });
        }

        function filter() {
            $scope.companyFriends = friends.filter(function(friend) {
                var filtered = true;
                if(!$scope.allFiltered) {
                    for(var i=0; i<$scope.filters.length; i++) {
                        filtered = filtered && (friend.industry == $scope.filters[i].name);
                    }
                }
                return filtered;
            });
        }

        function selectItem(item) {
            if($scope.allFiltered) {
                $scope.allFiltered = false;
                angular.forEach($scope.industryList, function(item) {
                    item.filtered = false;
                });
                item.filtered = true;
                return;
            }
            item.filtered = !item.filtered;
        }

        function selectAllItem() {
            if(!$scope.allFiltered) {
                $scope.allFiltered = true;
                angular.forEach($scope.industryList, function(item) {
                    item.filtered = true;
                });
            }
        }

        function deleteFriend(id) {
            Company
                .deleteFriend(id)
                .then(function(response) {
                    friends = friends.filter(function(friend) {
                        return friend.id != id;
                    });
                    filter();
                });

        }

        function applyFilter() {
            $scope.filters = [];
            angular.forEach($scope.industryList, function(item) {
                if (item.filtered) {
                    $scope.filters.push(item);
                }
            });
            $scope.isSearchDropdownOpened = false;
            filter();
        }

        /*
        function resetFilter() {
            $scope.allFiltered = false;
            angular.forEach($scope.industryList, function(item) {
                item.filtered = false;
            });
        }*/

        function deleteFilter(item) {
            for (var i=0; i<$scope.filters.length; i++) {
                if (item.id === $scope.filters[i].id) {
                    $scope.filters.splice(i, 1);
                }
            }

            angular.forEach($scope.industryList, function(itm) {
                if (item.id === itm.id) {
                    itm.filtered = false;
                }
            });
        }

        function deleteAllFilters() {
            $scope.filters = [];
            $scope.allFiltered = false;

            angular.forEach($scope.industryList, function(itm) {
                itm.filtered = false;
            });
        }

        function toggleSearchDropdown() {
            $scope.isSearchDropdownOpened = !$scope.isSearchDropdownOpened;
        }

        function cancel() {
            $uibModalInstance.close(friends);
        }
    }])
    .controller('CreateConvModalCtrl', ['$scope', '$http', 'config', '$uibModalInstance', '$uibModal', 'ngDialog', 'Chat',
            function($scope, $http, config, $uibModalInstance, $uibModal, ngDialog, Chat) {
        $scope.subject = "";
        $scope.uploaded = 0;
        $scope.description = "";
        $scope.form_url = "";
        $scope.form_link = "";
        $scope.files_id = [];
        $scope.files = [];
        $scope.show_spin = false;
        $scope.dzOptions = {
            url : '/',
            dictRemoveFile: '',
            previewTemplate: ''+
                '<li id="dz-preview-template" class="clearfix">' +
                '    <div class="pull-left file-name">' +
                '        <span data-dz-name></span>' +
                '        <span class="dz-size" data-dz-size></span>' +
                '    </div>' +
                '    <div class="pull-right actions">' +
                '        <a class="check"></a>' +
                '        <a class="close-icon thin-gray margin-left-30" data-dz-remove></a>' +
                '    </div>' +
                '</li>',
            previewsContainer:'.uploaded-file-list',
            autoProcessQueue: false,
            addRemoveLinks : true,
        };
        $scope.dzCallbacks = {
            'addedfile' : function(file){
                $scope.files.push(file);
                console.log('added', file);
            },
            'removedfile': function(file) {
                $scope.files.splice($scope.files.indexOf(file), 1);
                console.log('removed', file);
            }
        };
        $scope.dzMethods = {
        };


        $scope.uploadFiles = function () {
            $scope.show_spin = true;
            $scope.uploaded = 0;
            if ($scope.files.length) {
                angular.forEach($scope.files, function(file) {
                    Chat.uploadFile(file)
                        .success(function(data, status, headers, cfg) {
                            console.log('uploaded', data);
                            $scope.files_id.push(data.id);
                            $scope.$emit("fileUploaded");
                        }).error(function(data, status, headers, cfg) {
                            console.log('error: ', data);
                            $scope.$emit("fileUploaded");
                        });
                });
            } else {
                $scope.$emit("fileUploaded");
            }
        }

        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };

        $scope.$on("fileUploaded", function () {
            $scope.uploaded += 1;
            if ($scope.uploaded >= $scope.files.length) {
                $scope.createConversation();
                $scope.show_spin = false;
            }
        });

        $scope.createConversation = function () {
            var currentURI = window.location.href.toString().split(window.location.host)[1];
            var uriCompnents = currentURI.split('/');
            var companyID = uriCompnents[uriCompnents.length - 1];

            if (Number.isInteger(parseInt(companyID))) {
                var request = {
                    target_id  : companyID,
                    subject  : $scope.subject,
                    description  : $scope.description,
                    url : $scope.form_url,
                    link : $scope.form_link,
                    files: $scope.files_id
                };

                $http.post(config.baseURL + '/api/conversation/new_room/', request).then(function(response) {
                    ngDialog.open({
                        template: 'convCreatedPopup',
                    });
                });
            } else {
                alert("Please select company");
            }

            $uibModalInstance.dismiss('ok');

        };
    }]);
