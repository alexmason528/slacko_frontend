'use strict';
angular
    .module('app')
    .controller('HeaderController', ['$scope', '$rootScope', '$uibModal', 'localStorageService', '$location', 'Auth', 'Chat',
            function($scope, $rootScope, $uibModal, localStorageService, $location, Auth, Chat) {
        $scope.signin = function () {
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'views/modal/auth.modal.tpl.html',
                controller: 'AuthModalCtrl',
                windowClass: 'vcenter-modal'
            });
            modalInstance.result.then(
                function(data) {

                },
                function() {
                    console.info('Modal dismissed at: ' + new Date());
                }
            );
        };

        $scope.signup = function () {
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'views/modal/auth.modal.tpl.html',
                controller: 'AuthModalCtrl',
                windowClass: 'vcenter-modal'
            });
            modalInstance.result.then(
                function(data) {

                },
                function() {
                    console.info('Modal dismissed at: ' + new Date());
                }
            );
        }

        $scope.link_accept = function(n) {
            var link = n.link;
            $scope.link_deny(n);
            link = '/' + link.split('/').splice(3).join('/');
            window.location = link;
        }

        $scope.link_deny = function(n) {
            var data = {
                link: ''
            }
            n.link = '';
            Chat.updateNotifications(n.id, data).then(function(resp) {
                console.log('mark used', n, resp);
            });
        }

        $scope.markAsRead = function(n) {
            if (n.is_viewed) {
                return;
            }
            n.is_viewed = true;
            var data = {
                is_viewed: true
            }
            $rootScope.globals.notifications_new -= 1;
            localStorageService.set('globals', $rootScope.globals);
            Chat.updateNotifications(n.id, data).then(function(resp) {
                console.log('mark as read', n, resp);
            });
        }

        $scope.getFormattedTime = function(datetime_input){
            var datetime = moment(datetime_input).fromNow();
            return datetime;
        }

        $scope.signout = function () {
            $rootScope.isAuthorized = false;
            $rootScope.isOwner = false;
            Auth.clearCredentials();
            $location.path('/');
        };
    }]);
