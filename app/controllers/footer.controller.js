'use strict';
angular
    .module('app')
    .controller('FooterController', ['$scope', '$rootScope', '$uibModal', 'localStorageService', '$location', 'Auth', 'Chat',
            function($scope, $rootScope, $uibModal, localStorageService, $location, Auth, Chat) {
        $scope.signin = function () {
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'views/modal/signin.modal.tpl.html',
                controller: 'SigninModalCtrl',
                windowClass: 'vcenter-modal'
            });
            modalInstance.result.then(
                function(data) {

                },
                function() {
                    console.info('Modal dismissed at: ' + new Date());
                }
            );
        };
    }]);
