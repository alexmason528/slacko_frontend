'use strict';

angular
    .module('app')
    .controller('CompanyController', ['$scope', '$rootScope', '$timeout', '$location', 'Company', 'Common',
            function($scope, $rootScope, $timeout, $location, Company, Common) {
    	$rootScope.hasMenuBG = true;

    	$scope.isSearchDropdownOpened = false;
        $scope.isOnMap = false;
        $scope.mapCompanies = [];
        $scope.map = null;
        $scope.allFiltered = true;
        $scope.companyList = [];
        $scope.filters = [];
        $scope.companyProfile = {};
        $scope.searchKey = '';
        $scope.markerCluster = {};
        $scope.companyFriends = [];
        $scope.companyFriendsReq = [];

        $scope.getIndustryList = getIndustryList;
        $scope.getCompanyList = getCompanyList;
        $scope.getCompanyProfile = getCompanyProfile;
        $scope.selectItem = selectItem;
        $scope.selectAllItem = selectAllItem;
        $scope.applyFilter = applyFilter;
        $scope.resetAllFilters = resetAllFilters;
        $scope.deleteFilter = deleteFilter;
        $scope.deleteAllFilters = deleteAllFilters;
        $scope.toggleSearchDropdown = toggleSearchDropdown;
        $scope.toggleOnMap = toggleOnMap;
        $scope.filter = filter;
        $scope.inviteFriend = inviteFriend;
        $scope.visitCompany = visitCompany;
        $scope.isFriend = isFriend;
        $scope.isFriendReq = isFriendReq;

        init();

        function init() {
            getCompanyProfile();
            getIndustryList();
            if ($scope.isOnMap) {
                initMap();
            }
        }

        $scope.getLinkPreview = function(message) {
            if (message && message.length > 20) {
                return message.slice(0, 20) + '...';
            } else {
                return message;
            }
        }

        function initMap() {
            var mapOptions = {
                zoom: 4,
                center: new google.maps.LatLng(40.0000, -98.0000),
                mapTypeId: google.maps.MapTypeId.TERRAIN
            };
            $scope.map = new google.maps.Map(document.getElementById('map'), mapOptions);
        }

        function addMapCluster() {

            $scope.markers = [];

            var createMarker = function(company) {
                if (null != company.lat && null != company.lon) {
                    var latlng = new google.maps.LatLng(company.lat, company.lon);
                    var marker = new google.maps.Marker({
                        position: latlng,
                        company_id: company.id
                    });

                    $scope.markers.push(marker);
                }
            }

            for (var i = 0; i < $scope.companyList.length; i++) {
                createMarker($scope.companyList[i]);
            }

            var options = {
                styles:[
                    {"url":"/assets/images/map/c1.png","height":53,"textColor": "white", "width":53},
                    {"url":"/assets/images/map/c2.png","height":56,"textColor": "white", "width":56},
                    {"url":"/assets/images/map/c3.png","height":66,"textColor": "white", "width":66},
                    {"url":"/assets/images/map/c4.png","height":78,"textColor": "white", "width":78},
                    {"url":"/assets/images/map/c5.png","height":90,"textColor": "white", "width":90}
                ],
                zoomOnClick: false,
                minimumClusterSize: 1,
                imagePath: '/assets/images/map/c'
            };

            $scope.markerCluster = new MarkerClusterer($scope.map, $scope.markers, options);

            google.maps.event.addListener($scope.markerCluster, 'clusterclick', function(data) {
                $scope.mapCompanies = data.markers_.map(function(el) {
                    return el.company_id;
                });
                $timeout(function () {
                    filter();
                }, 100);

            });
        }

        function getCompanyProfile() {
            Company
                .getCompanyProfile()
                .then(function(response) {
                    console.log('Company profile:', response);
                    $scope.companyProfile = response;
                    $scope.companyFriends = response.friends.map(function(c) {
                        return c.id;
                    });
                    $scope.companyFriendsReq = response.friends_source.map(function(c) {
                        return c.target_company;
                    });
                    getCompanyList();
                });
        }

        function isFriend(id) {
            var res = -1 !=  $scope.companyFriends.indexOf(id);
            if (!res) {
                res = -1 !=  $scope.companyFriendsReq.indexOf(id);
            }
            return res;
        }

        function isFriendReq(id) {
            var res = -1 !=  $scope.companyFriendsReq.indexOf(id);
            return res;
        }

        function getIndustryList() {
            Common
                .getIndustryList()
                .then(function(response) {
                    console.log('Industry List:', response);
                    $scope.industryList = response;

                    angular.forEach($scope.industryList, function(item) {
                        angular.extend(item, {
                            filtered: true
                        });
                    });
                });
        }

        function getCompanyList() {
            Company
                .getCompanyList()
                .then(function(response) {
                    console.log('company list:', response.results);
                    $scope.companyList = response.results.filter(function(c) {
                        return (c.id != $scope.companyProfile.id);
                    });
                    if ($scope.map) {
                        addMapCluster();
                    }
                    filter();
                });
        }

        function selectItem(item) {
            if($scope.allFiltered) {
                $scope.allFiltered = false;
                angular.forEach($scope.industryList, function(item) {
                    item.filtered = false;
                });
                item.filtered = true;
                return;
            }
            item.filtered = !item.filtered;
        }

        function selectAllItem() {
            if(!$scope.allFiltered) {
                $scope.allFiltered = true;
                angular.forEach($scope.industryList, function(item) {
                    item.filtered = true;
                });
            }
            applyFilter();
        }

        function applyFilter() {
            $scope.filters = [];
            angular.forEach($scope.industryList, function(item) {
                if (item.filtered) {
                    $scope.filters.push(item);
                }
            });
            $scope.isSearchDropdownOpened = false;
            filter();
        }


        function resetAllFilters() {
            $scope.allFiltered = true
            $scope.mapCompanies = [];
            $scope.searchKey = '';
            angular.forEach($scope.industryList, function(item) {
                item.filtered = false;
            });
            applyFilter();
        }

        function deleteFilter(name) {
            angular.forEach($scope.industryList, function(itm) {
                if (name == itm.name) {
                    $scope.allFiltered = false;
                    itm.filtered = false;
                }
            });
            applyFilter();
        }

        function deleteAllFilters() {
            $scope.filters = [];
            $scope.allFiltered = false;

            angular.forEach($scope.industryList, function(itm) {
                itm.filtered = false;
            });
        }

        function toggleSearchDropdown(toggle) {
            if(toggle) {
                $scope.isSearchDropdownOpened = toggle;
                return;
            }
            $scope.isSearchDropdownOpened = !$scope.isSearchDropdownOpened;
        }

        function toggleOnMap() {
            $scope.isOnMap = !$scope.isOnMap;
            if (null == $scope.map) {
                initMap();
                addMapCluster();
            }
            $scope.mapCompanies = [];
        }

        function filter() {
            $scope.filteredCompanyList = $scope.companyList.filter(function (company) {
                var filtered = true;
                filtered = filtered && (company.name.toLowerCase().indexOf($scope.searchKey.toLowerCase())!=-1) || (company.website && company.website.toLowerCase()==$scope.searchKey.toLowerCase()) || (company.owner.email && company.owner.email.toLowerCase()==$scope.searchKey.toLowerCase());
                if(!$scope.allFiltered && $scope.filters.length>0) {
                    var matched = false;
                    for(var i=0; i<$scope.filters.length; i++) {
                        matched = matched || (company.industry == $scope.filters[i].name);
                    }
                    filtered = filtered && matched;
                }
                if ($scope.mapCompanies.length) {
                    filtered = filtered && (-1!=$scope.mapCompanies.indexOf(company.id));
                }
                return filtered;
            });
        }

        function inviteFriend(id, $event) {
            $event.stopPropagation();
            $event.preventDefault();
            Company
                .inviteFriend(id)
                .then(function(response) {
                    $scope.companyFriendsReq.push(id);
                    console.log('Invite friend:', response);
                });
        }

        function visitCompany(id) {
            $location.path('/company-profile/' + id);
        }
    }])
    .controller('CompanyProfileController', ['$scope', '$rootScope', '$uibModal', '$route', 'Company',
            function($scope, $rootScope, $uibModal, $route, Company) {
        $rootScope.hasMenuBG = false;

        $scope.companyProfile = {};
        $scope.companyList = [];
        $scope.companyFriends = [];
        $scope.companyMembers = [];
        $scope.affiCompanies = [];
        $scope.companyFriendsReq = [];

        $scope.getCompanyList = getCompanyList;
        $scope.getCompanyDetail = getCompanyDetail;
        $scope.sendFriendRequest = sendFriendRequest;
        $scope.createConv = createConv;
        $scope.isFriend = isFriend;
        $scope.isFriendReq = isFriendReq;

        init();

        function init() {
            getCurCompanyProfile();
            getCompanyList();
            getCompanyDetail();
            getCompanyMembers();
        }

        function getCompanyList() {
            Company
            .getCompanyList()
            .then(function(response) {
                console.log("Company List:", response.results);
                $scope.companyList = response.results;
            });
        }

        function getCurCompanyProfile() {
            Company
                .getCompanyProfile()
                .then(function(response) {
                    console.log('Company profile:', response);
                    $scope.companyFriendsReq = response.friends_source.map(function(c) {
                        return c.target_company;
                    });
                });
        }

        function getCompanyDetail() {
            Company
                .getCompanyDetail($route.current.params.id)
                .then(function(response) {
                    console.log('Company profile:', response);
                    $scope.companyProfile = response;
                    $scope.companyFriends = response.friends;
                    $scope.affiCompanies = $scope.companyProfile.affiliated_companies.filter(function(company) {
                        return company.id!=$scope.companyProfile.id;
                    });
                });
        }

        function getCompanyMembers() {
            Company
                .getCompanyMemberWithId($route.current.params.id)
                .then(function(response) {
                    console.log('Company members:', response);
                    $scope.companyMembers = response.results;
                });
        }

        function sendFriendRequest() {
            Company
                .inviteFriend($route.current.params.id)
                .then(function(response) {
                    console.log('invite friend:', response);
                    $scope.companyFriendsReq.push(parseInt($route.current.params.id));
                });
        }

        function isFriend() {
            var res = isFriendReq();
            angular.forEach($scope.companyFriends, function (c) {
                res = res || c.id == $rootScope.globals.companyOwner.company;
            });
            return res;
        }

        function isFriendReq() {
            var res = -1 !=  $scope.companyFriendsReq.indexOf($scope.companyProfile.id);
            return res;
        }

        function createConv() {
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'views/modal/create-conv.modal.tpl.html',
                controller: 'CreateConvModalCtrl',
                windowClass: 'vcenter-modal auto-height'
            });
            modalInstance.result.then(
                function(data) {

                },
                function() {
                    console.info('Modal dismissed at: ' + new Date());
                }
            );
        };
    }]);
