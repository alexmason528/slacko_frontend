'use strict';

angular
    .module('app')
    .controller('ConvController', ['$scope', '$http', 'config', '$rootScope', '$filter', '$uibModal', 'UserStorage', '$location', 'Chat',
            function($scope, $http, config, $rootScope, $filter, $uibModal, UserStorage, $location, Chat) {
        $rootScope.hasMenuBG = true;
        $rootScope.hideFooter = true;
        $scope.rooms = [];
        $scope.roomsFiltered = [];
        $scope.currentRoom = [];
        $scope.selection = 'list';
        $scope.filter_type = 'see_all';
        $scope.search = {text: ''};
        $scope.opt = { showSelect : false };
        $scope.msg_text = '';
        $scope.files = [];
        $scope.files_id = [];
        $scope.uploaded = 0;
        $scope.show_spin = false;

        $scope.$on("fileSelected", function (event, args) {
            $scope.$apply(function () {
                if ($scope.is_image(args.file.name)) {
                    args.file.preview = URL.createObjectURL(args.file)
                }
                $scope.files.push(args.file);
            });
        });

        $scope.uploadFile = function() {
            $scope.opt.showSelect = false;
            $('#uploadfile').click();
        }

        $scope.uploadImage = function() {
            $scope.opt.showSelect = false;
            $('#uploadimage').click();
        }

        $scope.is_image = function(filename) {
            var ext = filename.split('.').slice(-1)[0].toLowerCase();
            return -1 != $.inArray(ext, ["bmp", "png", "jpg", "jpeg", "gif", "svg"])
        }

        var currentUserInfo = JSON.parse(localStorage['ls.globals']);
        var user_id = currentUserInfo.currentUser.profile.user_id;

        var ws_scheme = window.location.protocol == "https:" ? "wss" : "ws";
        $scope.chatsock = null;

        Chat.getChatRooms().then(function(response) {
            $scope.rooms = response.results;
            $scope.filter();
        });

        $scope.gotoManage = function() {
            $location.path('/manage-conversation/'+$scope.currentRoom.id);
        }

        $scope.enterRoom = function(room) {
            // body.../api/company/friend/list/
            var roomID = room.id;
            if (false == room.confirmed) {
                $scope.selection = 'list';
                return;
            }
            $scope.selection = 'detail';
            $scope.currentRoom = $filter('filter')($scope.rooms, { id: roomID })[0];
            $scope.chatsock = new ReconnectingWebSocket(ws_scheme + '://' + config.chatURL + "/chat/" + roomID);
            $scope.chatsock.onmessage = function(message) {
                var data = JSON.parse(message.data);
                var chat = $("#chat");
                var hdr = {};
                var msg = {}
                var files = $('<span></span>');
                var ele = $('<li class="wrapper"></li>');
                if (data.handle.avatar) {
                    ele.append('<img class="user-avatar" src="'+data.handle.avatar+'" />');
                } else {
                    ele.append('<img class="user-avatar" src="assets/images/no-avatar.png" />');
                }


                hdr = $("<div class=chat-header></div>");
                hdr.append($('<span class="user-name"></span>').text(data.handle.first_name + " " + data.handle.last_name));
                hdr.append($('<span class="chat-time"></span>').text($scope.getFormattedTime(data.timestamp)));

                if (data.type == 'file') {
                    return;
                    var fileUrl = data.message;
                    var fileName = fileUrl.substring(fileUrl.lastIndexOf('/') + 1);
                    msg = $("<p class=chat-data></p>").html("<a href=" + fileUrl + " target=_blank>" + fileName + "</a>")
                } else {
                    msg = $("<p class=chat-data></p>").html(data.message)
                }

                angular.forEach(data.files, function(file){
                    if ($scope.is_image(file.filename)) {
                        files.append('<a href="'+file.file+'" target="_blank"><img class="chat-preview" src="'+file.file+'" title="'+file.filename+'"></a>');
                    } else {
                        files.append('<a href="'+file.file+'" target="_blank">'+file.filename+'</a>');
                    }
                });

                ele.append($('<div class="conv-wrapper"></div>').append($('<div class="chat-content"></div>').append(hdr).append(msg).append(files)));

                chat.append(ele)
            };
        }

        $scope.getFileName = function(fileUrl){
            var fileName = fileUrl.substring(fileUrl.lastIndexOf('/') + 1);
            return fileName;
        }

        $scope.getFormattedTime = function(datetime_input){
            var datetime = moment(datetime_input).fromNow();
            return datetime;
        }

        $scope.getMessagePreview = function(message) {
            if (message.length > 100) {
                return message.slice(0, 100) + '...';
            } else {
                return message;
            }
        }

        $scope.filterChat = function(type) {
            $scope.filter_type = type;
            $scope.filter();
        }

        $scope.filter = function() {
            console.log('filtering', $scope.search, $scope.filter_type);
            $scope.roomsFiltered = $scope.rooms.filter(function (room) {
                var filtered = true;
                var filter_text = $scope.search.text.toLocaleLowerCase();

                if ('confirmed' == $scope.filter_type) {
                    filtered = filtered && true == room.confirmed;
                } else if ('unconfirmed' == $scope.filter_type) {
                    filtered = filtered && false == room.confirmed;
                }

                filtered = filtered && ((room.name.toLowerCase().indexOf(filter_text) !==-1) || (room.last_message.message.toLowerCase().indexOf(filter_text)!=-1) || (room.target.name.toLowerCase().indexOf(filter_text)!=-1) || (room.target.website && room.target.website.toLowerCase()==filter_text) || (room.target.owner.email && room.target.owner.email.toLowerCase()==filter_text));

                return filtered;
            });
        }

        $scope.deleteMember = function(room_id, member_id) {
            Chat.delChatMember(room_id, member_id).then(function(resp) {
                $scope.currentRoom.members = $scope.currentRoom.members.filter(function(m) {
                    return member_id != m.id;
                });
                $scope.ctx = resp;
                ngDialog.open({
                    template: 'notificationPopup',
                    scope: $scope
                });
            });
        }

        $scope.getMemberName = function(member) {
            var result = "";

            if (member.first_name == "" && member.last_name == "") {
                result = member.email;
            } else {
                result = member.first_name + " " + member.last_name;
            }

            return result;

        }

        $scope.send = function() {
            $scope.uploaded = 0;
            $scope.show_spin = true;
            if ($scope.files.length) {
                angular.forEach($scope.files, function(file) {
                    Chat.uploadFile(file)
                        .success(function(data, status, headers, cfg) {
                            console.log('uploaded', data);
                            $scope.files_id.push(data.id);
                            $scope.$emit("fileUploaded");
                        }).error(function(data, status, headers, cfg) {
                            console.log('error: ', data);
                            $scope.$emit("fileUploaded");
                        });
                });
            } else {
                $scope.$emit("fileUploaded");
            }
        }

        $scope.$on("fileUploaded", function () {
            $scope.uploaded += 1;
            if ($scope.uploaded >= $scope.files.length) {
                $scope.real_send();
            }
        });

        $scope.real_send = function() {
            var message = {
                handle: user_id,
                type: "text",
                files: $scope.files_id,
                message: $('#message').val(),
            }
            $scope.chatsock.send(JSON.stringify(message));
            $("#message").val('').focus();
            $scope.files = [];
            $scope.msg_text = '';
            $scope.show_spin = false;
            return false;
        }

        $scope.checkFile = function() {
            var fileName = $("#file").val();
            if (fileName) {
                // Added code
                var fd = new FormData();
                var files = $('#file').prop("files");
                if (files.length) {
                    fd.append("file", files[0], files[0].name);
                }
                $http({
                    method: 'POST',
                    dataType: 'jsonp',
                    url: config.baseURL + '/api/v1/files/',
                    data: fd,
                    processData: false,
                    crossDomain: true,
                    headers: {
                        'Content-Type': undefined
                    },
                    xhrFields: {
                        withCredentials: true
                    },
                    transformRequest: angular.identity
                }).success(function(data, status, headers, cfg) {
                        var message = {
                            handle: user_id,
                            type: "file",
                            message: data.file,
                        }
                        $scope.chatsock.send(JSON.stringify(message));
                        $("#message").val('').focus();
                        $("#file").val('');
                        return false;
                }).error(function(data, status, headers, cfg) {
                    console.log('error: ', data);
                });
                // End of code

            } else {
                console.log("not selected");
            }
        }

    }])
    .controller('ConvMngController',
            ['$scope', '$route', '$http', 'config', '$rootScope', '$uibModal', '$location', 'Company', 'Chat', 'Common', 'ngDialog',
            function($scope, $route, $http, config, $rootScope, $uibModal, $location, Company, Chat, Common, ngDialog) {
        $rootScope.hasMenuBG = true;
        $rootScope.hideFooter = true;

        $scope.members = [];
        $scope.members_filtered = [];
        $scope.currentRoom = {};
        $scope.departList = [];
        $scope.filter_source = false;
        $scope.filter_target = false;
        $scope.filterSource = filterSource;
        $scope.filterTarget = filterTarget;
        $scope.allFiltered = true;
        $scope.chatSourceMembers = {};
        $scope.chatTargetMembers = {};
        $scope.dragging = false;

        var currentRoomID = $route.current.params.id;

        function filter() {
            $scope.members_filtered = $scope.members.filter(function(member) {
                var filtered = true;
                filtered = filtered && ((!$scope.filter_source || member.company != $scope.currentRoom.source.id));
                filtered = filtered && ((!$scope.filter_target || member.company != $scope.currentRoom.target.id));
                if(!$scope.allFiltered) {
                    var matched = false;
                    $scope.departList.forEach(function(depart) {
                        if (depart.filtered) {
                            matched = matched || (member.department == depart.name);
                        }
                    });
                    filtered = filtered && matched;
                }
                return filtered;
            });
        }

        $scope.selectDepartment = function(name) {
            if ('all' == name) {
                $scope.allFiltered = !$scope.allFiltered;
                $scope.departList.forEach(function(el) {
                    el.filtered = $scope.allFiltered;
                });
            } else {
                $scope.allFiltered = false;
                $scope.departList.forEach(function(el) {
                    if (el.name == name) {
                        el.filtered = !el.filtered;
                    }
                });
            }
            filter();
        }

        function filterTarget() {
            $scope.filter_target = !$scope.filter_target;
            filter();
        }

        function filterSource() {
            $scope.filter_source = !$scope.filter_source;
            filter();
        }

        Common
            .getDepartList()
            .then(function(response) {
                $scope.departList = response.map(function(el) {
                    return {'filtered':true, 'name': el.name }
                });
            });

        function loadCompanyMemebers(company_id, company_name) {
            Company
                .getCompanyMemberWithId(company_id)
                .then(function(response) {
                    response.results.forEach(function(m) {
                        m.company_name = company_name;
                    });

                    $scope.members = $scope.members.concat(response.results);
                    $scope.members_filtered = $scope.members_filtered.concat(response.results);

                    for (var i = 0; i < $scope.members.length; i++) {
                        $scope.members[i].selected = false;
                        for (var j = 0; j < $scope.currentRoom.members.length; j++) {
                            if ($scope.currentRoom.members[j].id == $scope.members[i].user_id) {
                                $scope.members[i].selected = true;
                            }
                        }
                    }
                });
        }

        function addCompanyMemeber(m) {
            var company = '';
            if (m.company == $scope.currentRoom.source.id) {
                company = $scope.chatSourceMembers;
            } else {
                company = $scope.chatTargetMembers;
            }

            if (!(m.department in company)) {
                var name = m.department;
                if (!m.department) {
                    name = 'Unknown';
                }

                company[m.department] = {'name': name, list: {}};
            }

            if (!(m.id in company[m.department]))
                company[m.department].list[m.id] = m;
        }

        function updateCompanyMembers(members) {
            $scope.chatSourceMembers = {};
            $scope.chatTargetMembers = {};
            members.forEach(function(m) {
                addCompanyMemeber(m);
            });
        }

        function syncCompanyMembers() {
            var selected_members = $scope.members.filter(function(m) {
                return m.selected;
            });
            updateCompanyMembers(selected_members);
        }

        Chat.getChatRoom(currentRoomID).then(function(response) {
            $scope.currentRoom = response;
            updateCompanyMembers(response.members);

            loadCompanyMemebers(response.source.id, response.source.name);
            loadCompanyMemebers(response.target.id, response.target.name);
        });

        $scope.dragStart = function(data, evt) {
            console.log('drag start', data, evt);
            $scope.dragging = true;
        }

        $scope.dragStop = function(data, evt) {
            console.log('drag stop', data, evt);
            $scope.dragging = false;
        }

        $scope.onDropComplete = function(data, evt) {
            console.log('drop complete', data, evt);
            if (data.type == 'depart') {
                var dep_name = data.obj;
                $scope.selectDepartment(dep_name);

                $scope.members_filtered.forEach(function(m) {
                    if (dep_name == 'all' || m.department == dep_name) {
                        m.selected = true;
                    }
                });
                syncCompanyMembers();
            } else {
                var member = $scope.members.find(function (m) {
                    return m.id === data.obj.id;
                });
                member.selected = true;

                syncCompanyMembers();
            }
        }

        $scope.getMemberName = function(member) {
            var result = "";

            if (member.first_name == "" && member.last_name == "") {
                result = member.email;
            } else {
                result = member.first_name + " " + member.last_name;
            }

            return result;
        }


        $scope.deleteConversation = function() {
            Chat.deleteChatRoom(currentRoomID).then(function(resp) {
                $scope.ctx = resp;
                ngDialog.open({
                    template: 'notificationPopup',
                    scope: $scope
                });
            });
        }

        $scope.closeManager = function() {
            $location.path('conversations');
        }

        $scope.deleteDepartmentFromChat = function(is_source, name) {
            var dep_list = "";
            if (is_source) {
                dep_list = $scope.chatSourceMembers;
            } else {
                dep_list = $scope.chatTargetMembers;
            }

            if (name == 'Unknown') {
                name = null;
            }

            angular.forEach(dep_list[name].list, (function(m) {
                m.selected = false;
            }));
            syncCompanyMembers();
        }

        $scope.toggleSelect = function(member_id) {
            var member = $scope.members.find(function (m) {
                return m.id === member_id;
            });
            member.selected = !(member.selected);
            syncCompanyMembers();
        }

        $scope.saveSettings = function() {
            var members = [];
            for (var i = 0; i < $scope.members.length; i++) {
                if ($scope.members[i].selected) {
                    members.push($scope.members[i].user_id);
                }
            }
            Chat.addChatMember(currentRoomID, members).then(function(resp) {
                $scope.ctx = resp;
                ngDialog.open({
                    template: 'notificationPopup',
                    scope: $scope
                });
            });
        }
    }]);
