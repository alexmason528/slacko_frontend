'use strict';

angular
    .module('app')
    .controller('AuthMagicLoginController',
            ['$scope', '$rootScope', '$route', '$uibModal', '$location', 'ngDialog', 'Auth', 'UserStorage', 'Profile', 'localStorageService', 'Company', 'Chat',
            function($scope, $rootScope, $route, $uibModal, $location, ngDialog, Auth, UserStorage, Profile, localStorageService, Company, Chat) {
                $scope.ctx = {};

                var key = $route.current.params.key;
                Auth.signinMagic(key)
                    .then(
                        function (response) {
                            UserStorage.setCompany(response.company);
                            UserStorage.setEmail(response.email);
                            Auth.setCredentials(UserStorage.getCompany(), UserStorage.getEmail(), response.token);

                            Profile
                                .getCompanyOwner()
                                .then(
                                    function (response) {
                                        $rootScope.globals.companyOwner = response;
                                        Profile
                                            .getProfile()
                                            .then(
                                                function (response) {
                                                    $rootScope.isOwner = $rootScope.globals.companyOwner.id == response.id;
                                                    $rootScope.isAuthorized = true;
                                                    $rootScope.globals.currentUser.profile = response;
                                                    localStorageService.set('globals', $rootScope.globals);
                                                    $location.path('/home');
                                                },
                                                function (response) {
                                                    console.log(response);
                                                }
                                            );
                                    },
                                    function (response) {
                                        console.log(response);
                                    }
                                );
                        },
                        function error(response) {
                            console.log(response);
                        }
                    );
            }
        ]
    )
    .controller('AuthVerifyController',
            ['$scope', '$rootScope', '$route', '$uibModal', '$location', 'ngDialog', 'Auth', 'UserStorage', 'Profile', 'localStorageService', 'Company', 'Chat',
            function($scope, $rootScope, $route, $uibModal, $location, ngDialog, Auth, UserStorage, Profile, localStorageService, Company, Chat) {
                $scope.ctx = {};

                var key = $route.current.params.key;
                Auth.activate(key)
                    .then(
                        function (response) {
                            ngDialog.open({
                                template: 'companyActivatedPopup',
                            });
                            $location.path('/');
                        },
                        function error(response) {
                            console.log(response);
                        }
                    );
            }
        ]
    )
    .controller('AuthPasswordResetController',
            ['$scope', '$rootScope', '$route', '$uibModal', '$location', 'ngDialog', 'Auth', 'UserStorage', 'Profile', 'localStorageService', 'Company', 'Chat',
            function($scope, $rootScope, $route, $uibModal, $location, ngDialog, Auth, UserStorage, Profile, localStorageService, Company, Chat) {
                $scope.ctx = {};

                var key = $route.current.params.key;
                var modalInstance = $uibModal.open({
                    animation: true,
                    templateUrl: 'views/modal/change-pwd.modal.tpl.html',
                    controller: 'ChangePwdModalCtrl',
                    windowClass: 'vcenter-modal',
                    resolve: {
                        key: function () {
                            return key;
                        }
                    }
                });
                modalInstance.result.then(
                    function(data) {
                    },
                    function() {
                        console.info('Modal dismissed at: ' + new Date());
                    }
                );
            }
        ]
    )
    .controller('AithInviteController',
            ['$scope', '$rootScope', '$route', '$uibModal', '$location', 'ngDialog', 'Auth', 'UserStorage', 'Profile', 'localStorageService', 'Company', 'Chat',
            function($scope, $rootScope, $route, $uibModal, $location, ngDialog, Auth, UserStorage, Profile, localStorageService, Company, Chat) {
                $scope.ctx = {};

                var key = $route.current.params.key;
                var modalInstance = $uibModal.open({
                    animation: true,
                    templateUrl: 'views/modal/register-member.modal.tpl.html',
                    controller: 'RegisterMemberModalCtrl',
                    windowClass: 'vcenter-modal'
                });
                modalInstance.result.then(
                    function(data) {
                        Company
                            .confirmInviteMember(data.firstName, data.lastName, data.pwd, key)
                            .then(
                                function (response) {
                                    if ('Success' == response.status) {
                                        UserStorage.setCompany(response.company);
                                        UserStorage.setEmail(response.email);
                                        Auth.setCredentials(UserStorage.getCompany(), UserStorage.getEmail(), response.token);

                                        Profile
                                            .getCompanyOwner()
                                            .then(
                                                function (response) {
                                                    $rootScope.globals.companyOwner = response;
                                                    Profile
                                                        .getProfile()
                                                        .then(
                                                            function (response) {
                                                                $rootScope.isOwner = $rootScope.globals.companyOwner.id == response.id;
                                                                $rootScope.isAuthorized = true;
                                                                $rootScope.globals.currentUser.profile = response;
                                                                localStorageService.set('globals', $rootScope.globals);
                                                                $location.path('/settings');
                                                            },
                                                            function (response) {
                                                                console.log(response);
                                                            }
                                                        );
                                                },
                                                function (response) {
                                                    console.log(response);
                                                }
                                            );
                                    }
                                },
                                function (response) {
                                    console.log(response);
                                    $scope.messages = response.data;
                                }
                            );
                    },
                    function() {
                        console.info('Modal dismissed at: ' + new Date());
                    }
                );
            }
        ]
    )
    .controller('CompanyInviteController',
            ['$scope', '$rootScope', '$route', '$uibModal', '$location', 'ngDialog', 'Auth', 'UserStorage', 'Profile', 'localStorageService', 'Company', 'Chat',
            function($scope, $rootScope, $route, $uibModal, $location, ngDialog, Auth, UserStorage, Profile, localStorageService, Company, Chat) {
                $scope.ctx = {};

                var key = $route.current.params.key;
                Company.confirmInviteFriend(key)
                    .then(
                        function (response) {
                            ngDialog.open({
                                template: 'notificationPopupFriendship',
                            });
                            $location.path('/company-profile/' + response.source);
                        },
                        function error(response) {
                            console.log(response);
                        }
                    );
            }
        ]
    )
    .controller('ConversationInviteController',
            ['$scope', '$rootScope', '$route', '$uibModal', '$location', 'ngDialog', 'Auth', 'UserStorage', 'Profile', 'localStorageService', 'Company', 'Chat',
            function($scope, $rootScope, $route, $uibModal, $location, ngDialog, Auth, UserStorage, Profile, localStorageService, Company, Chat) {
                $scope.ctx = {};

                var key = $route.current.params.key;
                Chat.confirmConversationInvite(key)
                    .then(
                        function (response) {
                            console.log(response.chatroom_id);
                            $location.path('conversations');
                        },
                        function error(response) {
                            console.log(response);
                        }
                    );
            }
        ]
    )
    .controller('ChatDeleteController',
            ['$scope', '$rootScope', '$route', '$uibModal', '$location', 'ngDialog', 'Auth', 'UserStorage', 'Profile', 'localStorageService', 'Company', 'Chat',
            function($scope, $rootScope, $route, $uibModal, $location, ngDialog, Auth, UserStorage, Profile, localStorageService, Company, Chat) {
                $scope.ctx = {};

                var key = $route.current.params.key;
                Chat.confirmConversationDelete(key)
                    .then(
                        function (response) {
                            console.log(response.chatroom_id);
                            $location.path('conversations');
                        },
                        function error(response) {
                            console.log(response);
                        }
                    );
            }
        ]
    )
    .controller('CompanyAffiliatedInviteController',
            ['$scope', '$rootScope', '$route', '$uibModal', '$location', 'ngDialog', 'Auth', 'UserStorage', 'Profile', 'localStorageService', 'Company', 'Chat',
            function($scope, $rootScope, $route, $uibModal, $location, ngDialog, Auth, UserStorage, Profile, localStorageService, Company, Chat) {
                $scope.ctx = {};

                var key = $route.current.params.key;
                Company.confirmAffilateInvite(key)
                    .then(
                        function (response) {
                            ngDialog.open({
                                template: 'notificationPopupAffiliated',
                            });
                            $location.path('/');
                        },
                        function error(response) {
                            console.log(response);
                        }
                    );
            }
        ]
    );
