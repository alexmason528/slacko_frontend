'use strict';

//This will prevent Dropzone to instantiate on it's own unless you are using dropzone class for styling
Dropzone.autoDiscover = false;

angular
    .module('app.config', [])
    .factory('httpInterceptor', ['$q','$location', '$rootScope', 'localStorageService', httpInterceptor])
    .config(configs)
    .run(runs);

function httpInterceptor($q, $location, $rootScope, localStorageService){
    var responseError = function (rejection) {
        if (rejection.status === 401) {
            console.log('You are unauthorised to access the requested resource (401)');
        } else if (rejection.status === 404) {
            console.log('The requested resource could not be found (404)');
        } else if (rejection.status === 500) {
            console.log('Internal server error (500)');
            // $location.path('/');
        } else if (rejection.status === 403) {
            if ('Signature has expired.' == rejection.data.detail) {
                console.log('Signature has expired. (403)');
                $rootScope.isAuthorized = false;
                $rootScope.isOwner = false;
                $rootScope.globals = {};
                localStorageService.remove('globals');
                // $http.defaults.headers.common.Authorization = 'Basic';
                $location.path('/');
            } else {
                console.log('Got unexpected 403:', rejection);
            }
        }
        return $q.reject(rejection);
    };

    return {
        responseError: responseError
    };
}

function configs($httpProvider, $resourceProvider) {
    //$httpProvider.defaults.withCredentials = true;
    $httpProvider.interceptors.push('httpInterceptor');
    $resourceProvider.defaults.stripTrailingSlashes = false;
}

function runs($rootScope, $location, $http, localStorageService, config) {
    // keep user logged in after page refresh
    $rootScope.globals = localStorageService.get('globals') || {};
    $rootScope.isOwner = false;
    $rootScope.isAuthorized = false;
    $rootScope.hideFooter = false;
    if ($rootScope.globals.currentUser) {
        $http.defaults.headers.common['Authorization'] = 'JWT ' + $rootScope.globals.currentUser.token; // jshint ignore:line
        $rootScope.isAuthorized = true;
        if ($rootScope.globals.companyOwner) {
            $rootScope.isOwner = $rootScope.globals.companyOwner.id == $rootScope.globals.currentUser.profile.id;
        }
    }

    $rootScope.$on('$locationChangeStart', function (event, next, current) {
        // redirect to login page if not logged in and trying to access a restricted page
        var restrictedPage = $.inArray($location.path(), ['/', ]) === -1;
        var loggedIn = $rootScope.globals.currentUser;
        if (restrictedPage && !loggedIn && !$location.path().startsWith('/confirm/')) {
            $location.path('/');
        } else {
            if ($rootScope.globals.currentUser) {
                $http.get(config.baseURL + '/api/v1/notifications/?user=' + $rootScope.globals.currentUser.profile.user_id).then(function(response) {
                    console.log('notifications:', response.data);
                    $rootScope.globals.notifications = response.data.results;

                    $rootScope.globals.notifications_new = 0;
                    angular.forEach(response.data.results, function(el) {
                        if (false == el.is_viewed) {
                            $rootScope.globals.notifications_new += 1;
                        }
                    });
                });
            }
        }
    });
}
