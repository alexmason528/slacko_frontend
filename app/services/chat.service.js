(function(){
'use strict';

angular
    .module('app')
    .factory('Chat', ChatService);

    ChatService.$inject = ['$http', '$rootScope', '$timeout', 'config'];

    function ChatService($http, $rootScope, $timeout, config) {
        var service = {};

        service.getNotifications = getNotifications;
        service.createChatRoom = createChatRoom;
        service.confirmConversationInvite = confirmConversationInvite;
        service.confirmConversationDelete = confirmConversationDelete;
        service.deleteChatRoom = deleteChatRoom;
        service.getChatRooms = getChatRooms;
        service.getChatRoom = getChatRoom;
        service.addChatMember = addChatMember;
        service.delChatMember = delChatMember;
        service.uploadFile = uploadFile;
        service.updateNotifications = updateNotifications;

        function uploadFile(file) {
            var fd = new FormData();
            fd.append("file", file, file.name);
            fd.append("filename", file.name);

            return $http({
                method: 'POST',
                url: config.baseURL + '/api/v1/files/',
                data: fd,
                headers: {
                    'Content-Type': undefined
                }
            });
        }

        function getChatRooms() {
            return $http.get(config.baseURL + '/api/v1/rooms/').then(function(response) {
                return response.data;
            });
        }

        function deleteChatRoom(id) {
            var request = {
                room_id: id,
            }
            return $http.post(config.baseURL + '/api/conversation/delete/', request).then(function(response) {
                return response.data;
            });
        }

        function confirmConversationDelete(key) {
            return $http.post(config.baseURL + '/api/conversation/confirm_delete/' + key + '/').then(function(response) {
                return response.data;
            });
        }

        function addChatMember(id, members) {
            var request = {
                room_id: id,
                members_id: members
            }

            return $http.post(config.baseURL + '/api/conversation/add_member/', request).then(function(response) {
                return response.data;
            });
        }

        function delChatMember(id, member) {
            var request = {
                room_id: id,
                member_id: member
            }

            return $http.post(config.baseURL + '/api/conversation/del_member/', request).then(function(response) {
                return response.data;
            });
        }

        function getChatRoom(id) {
            return $http.get(config.baseURL + '/api/v1/rooms/' + id + '/').then(function(response) {
                return response.data;
            });
        }

        /**
         * @name confirmConversationInvite
         * @desc confirm affiliated company
         */
        function confirmConversationInvite(key) {
            return $http.post(config.baseURL + '/api/conversation/confirm/' + key + '/').then(function(response) {
                return response.data;
            });
        }

        /**
         * @name getNotifications
         * @desc get notifications
         */
        function getNotifications(userId) {
            return $http.get(config.baseURL + '/api/v1/notifications/?user=' + userId).then(function(response) {
                return response.data;
            });
        }

        function updateNotifications(id, data) {
            return $http.patch(config.baseURL + '/api/v1/notifications/' + id + '/', data).then(function(response) {
                return response.data;
            });
        }


        /**
         * @name getNotifications
         * @desc create chat room
         */
        function createChatRoom(data) {
            return $http.post(config.baseURL + '/api/conversation/new_room/', data).then(function(response) {
                return response.data;
            });
        }


        return service;
    }
})();
