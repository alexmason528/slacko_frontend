(function(){
'use strict';

angular
	.module('app')
	.factory('Profile', ProfileService);

	ProfileService.$inject = ['$http', '$rootScope', '$timeout', 'config'];

	function ProfileService($http, $rootScope, $timeout, config) {
		var service = {};

		service.getProfile = getProfile;
		service.saveProfile = saveProfile;
		service.changePassword = changePassword;
		service.getCompanyOwner = getCompanyOwner;
		service.updateUploadedFile = updateUploadedFile;

		/**
	 	 * @name getProfile
	 	 * @desc get profile
	 	 */
		function getCompanyOwner() {
			return $http.get(config.baseURL + '/api/company_owner/').then(function(response) {
				return response.data;
			});
		}

		/**
	 	 * @name updateUploadedFile
	 	 * @desc update company profile by uploading file
	 	 */
		function updateUploadedFile(data) {
			var request = {
                method: 'PATCH',
                url: config.baseURL + '/api/profile/',
                headers: {
                    'Content-Type': undefined
                },
                data: data,
            };

			return $http(request).then(function(response) {
				return response.data;
			});
		}

		/**
	 	 * @name getProfile
	 	 * @desc get profile
	 	 */
		function getProfile() {
			return $http.get(config.baseURL + '/api/profile/').then(function(response) {
				return response.data;
			});
		}

		/**
	 	 * @name saveProfile
	 	 * @desc save profile
	 	 */
		function saveProfile(objData) {
            var request = {};
            angular.copy(objData, request);
			delete request['avatar'];
			return $http.put(config.baseURL + '/api/profile/', request).then(function(response) {
				return response.data;
			});
		}

		/**
	 	 * @name changePassword
	 	 * @desc Change password
	 	 */
		function changePassword(oldPwd, newPwd) {
			var objData = {
				old_password: oldPwd,
				new_password: newPwd
			};

			return $http.put(config.baseURL + '/api/profile/change-password/', objData).then(function(response) {
				return response.data;
			});
		}

	 	return service;
	}
})();
