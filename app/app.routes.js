'use strict';

angular
    .module('app.routes', ['ngRoute'])
    .config(config);

function config ($routeProvider, $locationProvider) {
    $routeProvider.
        when('/', {
            templateUrl: 'views/index/index.tpl.html',
            controller: 'IndexController'
        })
        .when('/home', {
            templateUrl: 'views/home/home.tpl.html',
            controller: 'HomeController'
        })
        .when('/companies', {
            templateUrl: 'views/companies/index.tpl.html',
            controller: 'CompanyController'
        })
        .when('/conversations', {
            templateUrl: 'views/conversation/index.tpl.html',
            controller: 'ConvController'
        })
        .when('/manage-conversation/:id', {
            templateUrl: 'views/conversation/conv-mng.tpl.html',
            controller: 'ConvMngController'
        })
        .when('/company-profile/:id', {
            templateUrl: 'views/companies/company-profile.tpl.html',
            controller: 'CompanyProfileController'
        })
        .when('/settings', {
            templateUrl: 'views/settings/index.tpl.html',
            controller: 'SettingsController'
        })
        .when('/confirm/auth/magic_login/:key', {
            templateUrl: 'views/index/index.tpl.html',
            controller: 'AuthMagicLoginController'
        })
        .when("/confirm/auth/verify/:key", {
            templateUrl: 'views/index/index.tpl.html',
            controller: 'AuthVerifyController',
        })
        .when("/confirm/auth/password_reset/:key", {
            templateUrl: 'views/index/index.tpl.html',
            controller: 'AuthPasswordResetController'
        })
        .when("/confirm/auth/invite/:key", {
            templateUrl: 'views/index/index.tpl.html',
            controller: 'AithInviteController'
        })
        .when("/confirm/company/invite/:key", {
            templateUrl: 'views/index/index.tpl.html',
            controller: 'CompanyInviteController'
        })
        .when("/confirm/conversation/invite/:key", {
            templateUrl: 'views/index/index.tpl.html',
            controller: 'ConversationInviteController'
        })
        .when("/confirm/company/affiliated_invite/:key", {
            templateUrl: 'views/index/index.tpl.html',
            controller: 'CompanyAffiliatedInviteController'
        })
        .when("/confirm/chat/delete/:key", {
            templateUrl: 'views/index/index.tpl.html',
            controller: 'ChatDeleteController'
        })
        .otherwise({
            redirectTo: '/'
        });

    // use the HTML5 History API
    // $locationProvider.html5Mode(true);
}
